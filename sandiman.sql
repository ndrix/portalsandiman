-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 14 Feb 2019 pada 02.03
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sandiman`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id` int(10) UNSIGNED NOT NULL,
  `isi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id`, `isi`, `file`, `created_at`, `updated_at`) VALUES
(1, 'Mari mencoba fitur di aplikasi ini.', 'Screen Shot 2018-12-07 at 14.37.15.png', '2019-02-05 17:00:00', '2019-02-05 20:40:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `formasi`
--

CREATE TABLE `formasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `instansi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operator` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jml_formasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jml_fungsional` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jml_pegawai_dg_formasi_fungsional` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jml_formasi_kosong` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_surat_formasi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` enum('belum diisi','sudah diisi') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `formasi`
--

INSERT INTO `formasi` (`id`, `instansi`, `provinsi`, `tahun`, `operator`, `jabatan`, `jml_formasi`, `jml_fungsional`, `jml_pegawai_dg_formasi_fungsional`, `jml_formasi_kosong`, `file_surat_formasi`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Badan Siber dan Sandi Negara', 'Jambi', '2019', 'abc@abc', 'Sandiman', '5,5,5,5,5,5,5,5', '5,5,5,5,5,5,5,5', '5,5,5,5,5,5,5,5', '5,5,5,5,5,5,5,5', '123_surat_formasi.png', 'sudah diisi', NULL, '2019-01-27 11:36:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `formulir`
--

CREATE TABLE `formulir` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `formulir`
--

INSERT INTO `formulir` (`id`, `nama`, `file`, `created_at`, `updated_at`) VALUES
(3, 'Form Komitmen', 'form_komitmen.doc', '2019-02-02 17:00:00', '2019-02-02 17:00:00'),
(4, 'Form Bersedia Mengikuti Diklat', 'form_bersedia_diklat.doc', '2019-02-02 17:00:00', '2019-02-02 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inpassing`
--

CREATE TABLE `inpassing` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('menunggu','review','direvisi','diterima') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan_yg_diajukan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gelar_depan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gelar_belakang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip_comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pangkat_gol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmt_gol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tingkat_pendidikan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instansi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_ijazah_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_ijazah_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_sk_kp_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_sk_kp_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_surat_masa_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_surat_masa_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_surat_kompeten` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_surat_kompeten` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_surat_usulan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_surat_usulan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai_realisasi_skp_2_thn_sebelumnya` double NOT NULL,
  `file_realisasi_skp_2_thn_sebelumnya` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai_realisasi_skp_1_thn_sebelumnya` double NOT NULL,
  `file_realisasi_skp_1_thn_sebelumnya` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai_orientasi_pelayanan` double NOT NULL,
  `nilai_integritas` double NOT NULL,
  `nilai_komitmen` double NOT NULL,
  `nilai_disiplin` double NOT NULL,
  `nilai_kerjasama` double NOT NULL,
  `nilai_kepemimpinan` double NOT NULL,
  `nilai_jumlah_ppk` double NOT NULL,
  `nilai_rata_rata_ppk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai_perilaku_kerja` double NOT NULL,
  `nilai_prestasi_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_ppk_thn_sebelumnya` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `komentar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_ujian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasil_ujian` enum('lulus','tidak lulus') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nilai_ujian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `inpassing`
--

INSERT INTO `inpassing` (`id`, `status`, `tahun`, `jabatan_yg_diajukan`, `nama`, `gelar_depan`, `gelar_belakang`, `nip`, `nip_comment`, `pangkat_gol`, `tmt_gol`, `tingkat_pendidikan`, `no_telepon`, `email`, `tanggal_lahir`, `unit_kerja`, `instansi`, `provinsi`, `no_ijazah_terakhir`, `file_ijazah_terakhir`, `no_sk_kp_terakhir`, `file_sk_kp_terakhir`, `no_surat_masa_kerja`, `file_surat_masa_kerja`, `no_surat_kompeten`, `file_surat_kompeten`, `no_surat_usulan`, `file_surat_usulan`, `nilai_realisasi_skp_2_thn_sebelumnya`, `file_realisasi_skp_2_thn_sebelumnya`, `nilai_realisasi_skp_1_thn_sebelumnya`, `file_realisasi_skp_1_thn_sebelumnya`, `nilai_orientasi_pelayanan`, `nilai_integritas`, `nilai_komitmen`, `nilai_disiplin`, `nilai_kerjasama`, `nilai_kepemimpinan`, `nilai_jumlah_ppk`, `nilai_rata_rata_ppk`, `nilai_perilaku_kerja`, `nilai_prestasi_kerja`, `file_ppk_thn_sebelumnya`, `komentar`, `tanggal_ujian`, `hasil_ujian`, `nilai_ujian`, `created_at`, `updated_at`) VALUES
(1, 'diterima', '2019', 'Sandiman Pelaksana', 'asd', NULL, NULL, '123', NULL, 'Juru Tk I (I/d)', '26-01-2019', 'MA', '123', 'agd@agd', '26-01-2019', 'asd', 'Badan Siber dan Sandi Negara', 'Jambi', '123', '123_ijazah_terakhir.png', '123', '123_sk_kp_terakhir.png', '123', '123_surat_masa_kerja.png', '123', '123_surat_kompeten.png', '123', '123_surat_usulan.png', 76, '123_file_realisasi_skp_2_thn_sebelumnya.png', 76, '123_file_realisasi_skp_1_thn_sebelumnya.png', 76, 76, 76, 76, 76, 76, 456, '76', 30.400000000000002, '76', '123_ppk_thn_sebelumnya.png', NULL, '31/01/2019', 'lulus', '76', '2019-01-25 18:27:12', '2019-01-26 19:06:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_16_075706_create_user_data_table', 2),
(4, '2019_01_20_102311_create_inpassing_table', 3),
(5, '2019_01_22_041824_create_formasi_table', 4),
(6, '2019_01_25_035620_create_wall_table', 4),
(7, '2019_01_26_014902_create_permission_tables', 5),
(8, '2019_02_03_152310_create_table_formulir', 6),
(9, '2019_02_06_023524_create_berita_table', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(3, 'App\\User', 3),
(4, 'App\\User', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'web', NULL, NULL),
(2, 'Admin Instansi', 'web', NULL, NULL),
(3, 'Penilai Inpassing', 'web', NULL, NULL),
(4, 'Peserta Inpassing', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','active','nonactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin', NULL, '$2y$12$wO0hpx5XqQFQfk3/wV7yDOw0s2VOKngSr.K6hMTOp00rgWrk2Huwi', 'spdVAAoXHqle6j9UvWZvinYbCgzXlz5hGveNztCph41dfTxefr8HzontbXlx', 'active', NULL, '2019-01-27 10:35:27'),
(2, 'abc', 'abc@abc', NULL, '$2y$12$39yqb5ztiicF5fL5mG25ku63HDNJHQuBkhKfihD8w/qaakaiT8KDm', 'WayXIQSog492PvZt628VgOd2DcM1cbCCZfwRkWv2vhlXXnJcCLRAlyl2Kzr3', 'active', '2019-01-18 08:45:21', '2019-01-18 20:40:33'),
(3, 'asd', 'asd@asd', NULL, '$2y$12$XCo42Ld5E4UzoRIiv/Bcs.Ky9zgUA.g9RM2UiYN6uLHLmUr8YM2Um', 'uf6qE1gnIleZFewK8htU1UzPES6f2fZrg9ba3LpvL3gE2fx7WtOERCILNie6', 'active', '2019-01-18 10:42:15', '2019-01-19 01:44:54'),
(4, 'agd', 'agd@agd', NULL, '$2y$12$J3I/65qlBA6PkXIWMJUYreCTKaLIkz/kCDmk5KXjpHPK9Q70juezS', 'x1JlDYTaCmD3w0gmz747pjggkN99vWHAMWt4W5YD3SfTHFeJg2wPsn5sX0tx', 'active', '2019-01-19 01:14:54', '2019-01-19 01:30:40'),
(6, 'qwe', 'qwe@qwe', NULL, '', NULL, 'pending', '2019-02-04 00:36:07', '2019-02-04 00:36:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_data`
--

CREATE TABLE `user_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gelar_depan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gelar_belakang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tingkat_pendidikan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan_pendidikan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_karpeg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pangkat_gol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmt_gol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `masa_kerja_thn` int(11) DEFAULT NULL,
  `masa_kerja_bln` int(11) DEFAULT NULL,
  `jabatan_yg_diajukan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan_fungsional` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_sk_pns_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_sk_pns_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_sk_awal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmt_sk_awal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gol_jabatan_awal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_sk_jabatan_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmt_jabatan_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `angka_kredit_terakhir` double DEFAULT NULL,
  `file_sk_jabatan_terakhir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_surat_tugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_surat_tugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instansi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kabupaten` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `user_data`
--

INSERT INTO `user_data` (`id`, `jenis`, `nip`, `nama`, `gelar_depan`, `gelar_belakang`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `email`, `tingkat_pendidikan`, `jurusan_pendidikan`, `no_karpeg`, `pangkat_gol`, `tmt_gol`, `masa_kerja_thn`, `masa_kerja_bln`, `jabatan_yg_diajukan`, `jabatan_fungsional`, `no_sk_pns_terakhir`, `file_sk_pns_terakhir`, `no_sk_awal`, `tmt_sk_awal`, `gol_jabatan_awal`, `no_sk_jabatan_terakhir`, `tmt_jabatan_terakhir`, `angka_kredit_terakhir`, `file_sk_jabatan_terakhir`, `no_surat_tugas`, `file_surat_tugas`, `instansi`, `provinsi`, `kabupaten`, `unit_kerja`, `created_at`, `updated_at`) VALUES
(1, 'admin_instansi', '123', 'abc', 'abc', 'abc', 'laki-laki', 'abc', '17-01-2019', 'abc', 'abc@abc', 'SMA', 'abc', 'abc', 'Juru Muda', '01-01-2019', 1, 1, 'Sandiman Pelaksana Pemula', NULL, '110', 'abc_sk_pns.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Badan Siber dan Sandi Negara', 'Sumatera Utara', 'abc', 'abc', '2019-01-18 08:45:21', '2019-01-18 08:45:21'),
(3, 'penilai_inpassing', '1130', 'asd', 'a', 'a', 'laki-laki', 'a', '01-01-2019', 'a', 'asd@asd', 'SMA', 'a', '123', 'Pengatur Muda Tk I', '02-01-2019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1125', 'asd_surat_tugas.png', 'Kementerian Koordinator Bidang Polhukam', 'Riau', 'a', 'a', '2019-01-18 10:42:15', '2019-01-18 10:42:15'),
(4, 'peserta_inpassing', '1137', 'agd', 'a', 'a', 'laki-laki', 'a', '01-01-2019', 'a', 'agd@agd', 'SMA', 'a', 'a', 'Juru Muda', '02-01-2019', 2, 1, NULL, 'Sandiman Pelaksana', '1138', NULL, '1131', '02-01-2019', 'Juru Muda', '1135', '03-01-2019', 60.75, 'agd_sk_pns.png', NULL, NULL, 'Badan Siber dan Sandi Negara', 'Jambi', 'a', 'a', '2019-01-19 01:14:54', '2019-01-19 01:14:54'),
(6, 'peserta_inpassing', '234', 'qwe', NULL, NULL, '', 'qwe', '04-02-2019', 'qwe', 'qwe@qwe', '', '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pemerintah Daerah', 'Aceh', '', '', '2019-02-04 00:36:07', '2019-02-04 00:36:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wall`
--

CREATE TABLE `wall` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `wall`
--

INSERT INTO `wall` (`id`, `email`, `message`, `created_at`, `updated_at`) VALUES
(2, 'agd@agd', 'Anda telah mengajukan permohonan Inpassing Jabatan. Silakan tunggu respon dari penilai.', '2019-01-25 18:27:12', '2019-01-25 18:27:12'),
(3, 'abc@abc', 'Terima kasih, Anda telah melengkapi formulir Formasi.', '2019-01-27 11:34:27', '2019-01-27 11:34:27'),
(4, 'all', 'Coba', '2019-02-05 23:50:29', '2019-02-06 01:00:22');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `formasi`
--
ALTER TABLE `formasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `formulir`
--
ALTER TABLE `formulir`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inpassing`
--
ALTER TABLE `inpassing`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indeks untuk tabel `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `user_data`
--
ALTER TABLE `user_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_data_nip_unique` (`nip`),
  ADD UNIQUE KEY `user_data_email_unique` (`email`);

--
-- Indeks untuk tabel `wall`
--
ALTER TABLE `wall`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `formasi`
--
ALTER TABLE `formasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `formulir`
--
ALTER TABLE `formulir`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `inpassing`
--
ALTER TABLE `inpassing`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `user_data`
--
ALTER TABLE `user_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `wall`
--
ALTER TABLE `wall`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
