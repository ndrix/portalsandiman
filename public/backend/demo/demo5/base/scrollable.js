var DatatablesBasicScrollable = {
	init: function () {
		$("#m_table_1").DataTable({
			scrollY: "50vh",
			scrollX: !0,
			scrollCollapse: !0,
		}), $("#m_table_2").DataTable({
			scrollY: "50vh",
			scrollX: !0,
			scrollCollapse: !0,
			createdRow: function (a, e, t) {
				var n = {
						1: {
							title: "Pending",
							class: "m-badge--brand"
						},
						2: {
							title: "Delivered",
							class: " m-badge--metal"
						},
						3: {
							title: "Canceled",
							class: " m-badge--primary"
						},
						4: {
							title: "Success",
							class: " m-badge--success"
						},
						5: {
							title: "Info",
							class: " m-badge--info"
						},
						6: {
							title: "Danger",
							class: " m-badge--danger"
						},
						7: {
							title: "Warning",
							class: " m-badge--warning"
						}
					},
					l = '<span class="m-badge ' + n[e[18]].class + ' m-badge--wide">' + n[e[18]].title + "</span>";
				a.getElementsByTagName("td")[18].innerHTML = l, l = '<span class="m-badge m-badge--' + (n = {
					1: {
						title: "Online",
						state: "danger"
					},
					2: {
						title: "Retail",
						state: "primary"
					},
					3: {
						title: "Direct",
						state: "accent"
					}
				})[e[19]].state + ' m-badge--dot"></span> <span class="m--font-bold m--font-' + n[e[19]].state + '">' + n[e[19]].title + "</span>", a.getElementsByTagName("td")[19].innerHTML = l
			}
		})
	}
};
jQuery(document).ready(function () {
	DatatablesBasicScrollable.init()
});