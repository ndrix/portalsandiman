<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInpassingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inpassing', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['menunggu', 'review', 'direvisi', 'diterima']);
            $table->string('tahun');
            $table->string('jabatan_yg_diajukan');
            $table->string('nama');
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang')->nullable();
            $table->string('nip');
            $table->string('pangkat_gol');
            $table->string('tmt_gol');
            $table->string('email');
            $table->string('tingkat_pendidikan');
            $table->string('no_telepon');
            $table->string('email');
            $table->string('tanggal_lahir');
            $table->string('unit_kerja');
            $table->string('instansi');
            $table->string('provinsi');
            $table->string('no_ijazah_terakhir');
            $table->string('file_ijazah_terakhir');
            $table->string('no_sk_kp_terakhir');
            $table->string('file_sk_kp_terakhir');
            $table->string('no_surat_masa_kerja');
            $table->string('file_surat_masa_kerja');
            $table->string('no_surat_kompeten');
            $table->string('file_surat_kompeten');
            $table->string('no_surat_usulan');
            $table->string('file_surat_usulan');
            $table->double('nilai_realisasi_skp_2_thn_sebelumnya');
            $table->string('file_realisasi_skp_2_thn_sebelumnya');
            $table->double('nilai_realisasi_skp_1_thn_sebelumnya');
            $table->string('file_realisasi_skp_1_thn_sebelumnya');
            $table->double('nilai_orientasi_pelayanan');
            $table->double('nilai_integritas');
            $table->double('nilai_komitmen');
            $table->double('nilai_disiplin');
            $table->double('nilai_kerjasama');
            $table->double('nilai_kepemimpinan');
            $table->double('nilai_jumlah_ppk');
            $table->double('nilai_rata_rata_ppk');
            $table->double('nilai_perilaku_kerja');
            $table->double('nilai_prestasi_kerja');
            $table->string('file_ppk_thn_sebelumnya');
            $table->string('komentar')->nullable();
            $table->string('tanggal_ujian')->nullable();
            $table->enum('hasil_ujian', ['lulus', 'tidak lulus'])->nullable();
            $table->string('nilai_ujian')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inpassing');
    }
}
