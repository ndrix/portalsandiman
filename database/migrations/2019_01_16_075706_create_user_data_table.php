<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis');
            $table->string('nip')->unique();
            $table->string('nama');
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang')->nullable();
            $table->enum('jenis_kelamin', ['laki-laki', 'perempuan']);
            $table->string('tempat_lahir');
            $table->string('tanggal_lahir');
            $table->string('alamat');
            $table->string('email')->unique();
            $table->string('tingkat_pendidikan');
            $table->string('jurusan_pendidikan');
            $table->string('no_karpeg')->nullable();
            $table->string('pangkat_gol');
            $table->string('tmt_gol');
            $table->integer('masa_kerja_thn')->nullable();
            $table->integer('masa_kerja_bln')->nullable();
            $table->string('jabatan_yg_diajukan')->nullable();
            $table->string('jabatan_fungsional')->nullable();
            $table->string('no_sk_pns_terakhir')->nullable();
            $table->string('file_sk_pns_terakhir')->nullable();
            $table->string('no_sk_awal')->nullable();
            $table->string('tmt_sk_awal')->nullable();
            $table->string('gol_jabatan_awal')->nullable();
            $table->string('no_sk_jabatan_terakhir')->nullable();
            $table->string('tmt_jabatan_terakhir')->nullable();
            $table->double('angka_kredit_terakhir')->nullable();
            $table->string('file_sk_jabatan_terakhir')->nullable();
            $table->string('no_surat_tugas')->nullable();
            $table->string('file_surat_tugas')->nullable();
            $table->string('instansi');
            $table->string('provinsi');
            $table->string('kabupaten');
            $table->string('unit_kerja');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_data');
    }
}
