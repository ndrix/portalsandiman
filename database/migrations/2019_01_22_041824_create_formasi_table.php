<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('instansi');
            $table->string('provinsi');
            $table->string('tahun');
            $table->string('operator');
            $table->string('jabatan');
            $table->string('jml_formasi');
            $table->string('jml_fungsional');
            $table->string('jml_pegawai_dg_formasi_fungsional');
            $table->string('jml_formasi_kosong');
            $table->string('file_surat_formasi')->nullable();
            $table->enum('keterangan', ['sudah diisi', 'belum diisi'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formasi');
    }
}
