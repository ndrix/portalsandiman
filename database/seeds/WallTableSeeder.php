<?php

use Illuminate\Database\Seeder;
use Carbon;

class WallTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wall')->insert([
            'email' => 'all',
            'message' => 'Selamat Datang di Portal Sandiman, silakan manfaatkan fitur yg tersedia.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
