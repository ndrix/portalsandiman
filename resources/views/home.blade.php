@extends('templatebaru')

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Home</h3>
					</div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
				</div>
            </div>

			<!-- END: Subheader -->
			<div class="m-content">
                <div class="row">
                    <div class="col-xl-8">
                        <div class="m-portlet m-portlet--skin-dark m-portlet--bordered-semi m--bg-brand">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon">
                                            <i class="flaticon-statistics"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">
                                            Selamat datang di Aplikasi Inpassing Jabatan Fungsional Sandiman
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <font size="3">Aplikasi Inpassing Jabatan Fungsional Sandiman merupakan sistem informasi yang ditujukan untuk memudahkan calon peserta
                                agar dapat secara online melakukan proses Inpassing Jabatan. Aplikasi ini dikelola oleh Badan Siber dan Sandi Negara.
                                </font>
                                <br><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="portlet">
                            <img src="{{ asset('img/hrm.png') }}" class="img-fluid">
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            Berita
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1">
                                            <div class="form-group m-form__group row align-items-center">
                                                <div class="col-md-4">
                                                    <div class="m-input-icon m-input-icon--left">
                                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                                            <span><i class="la la-search"></i></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<table class="m-datatable" id="html_table" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10%">No</th>
                                            <th>Tanggal</th>
                                            <th>Isi Berita</th>
                                            <th>File</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($berita as $i)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $i->created_at->format('d-F-Y') }}</td>
                                                <td>{{ $i->isi }}</td>
                                                <td>
                                                    @if($i->file != null)
                                                        <a href="{{ url('berita_download', ['param' =>  $i->file]) }}"><i class="flaticon-file"></i> Download
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
@endsection

@section('script')
<script src="{{ asset('backend/demo/demo5/base/html-table.js') }}" type="text/javascript"></script>
@endsection
