@extends('templatebackend')

@section('header')
    <link href="{{ asset('backend/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">Pengumuman</h3>
                    </div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
                </div>
            </div>

            <!-- END: Subheader -->
            <div class="m-content">
                @if ($message = Session::get('success'))
                    <div class="alert alert-info" role="alert">
                        <strong>Berhasil!</strong> {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> {{ $message }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Daftar Pengumuman
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#tambahmodal">
                                        <i class="flaticon-plus"></i> Tambah
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="60%">Pesan</th>
                                    <th width="20%">Tujuan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($wall as $i)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $i->message }}</td>
                                    <td>@php if($i->email != "all") { echo App\UserData::where('email', $i->email)->first()->nama; } else { echo ucwords($i->email); } @endphp</td>
                                    <td>
                                        <form method="post" action="{{ url('wall_delete', ['param' => $i->id]) }}">
                                            @csrf @method('delete')
                                            {{-- <a href="" data-toggle="modal" data-target="#m_select2_modal" class="btn btn-sm btn-info">Ubah</a> --}}
                                            <a href="{{ url('wall_show', ['param' => $i->id]) }}" class="btn btn-info btn-sm m_select2_modal">Ubah</a>
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus entri ini?');">Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="tambahmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Pengumuman</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formtambah" method="post" action="{{ url('wall_store') }}">
                        @csrf
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="220">
                            <div class="form-group m-form__group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Pesan:</label>
                                <div class="col-xl-9 col-lg-9">
                                    <textarea rows="7" name="message" class="form-control m-input" placeholder="" value=""></textarea>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m--margin-top-20">
                                <label class="col-form-label col-lg-3 col-sm-12">Tujuan</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <select class="form-control m-select2" id="tujuantambah" name="tujuan" style="width: 100%">
                                        <option value="all">All</option>
                                        @foreach(App\UserData::all() as $i)
                                            <option value="{{ $i->email }}">{{ $i->nama }} - {{ $i->nip }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" onclick="tambah()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="m_select2_modal" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Ubah Pengumuman</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="la la-remove"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="myform" method="post" action="{{ url('wall_update') }}">
                        @csrf @method('put')
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="220">
                            <input hidden type="text" class="form-control m-input" id="id" name="id" value="">
                            <div class="form-group m-form__group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Pesan:</label>
                                <div class="col-xl-9 col-lg-9">
                                    <textarea rows="6" type="text" name="message" id="message" class="form-control m-input" placeholder="" value=""></textarea>
                                </div>
                            </div>
                            <div class="form-group m-form__group row m--margin-top-20">
                                <label class="col-form-label col-lg-3 col-sm-12">Tujuan</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <select class="form-control m-select2" id="m_select2_1_modal" name="tujuan">
                                        <option value="all">All</option>
                                        @foreach(App\UserData::all() as $i)
                                            <option value="{{ $i->email }}">{{ $i->nama }} - {{ $i->nip }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" onclick="ubah()">Simpan</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{{ asset('backend/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/demo5/base/scrollable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/demo5/base/select2.js') }}" type="text/javascript"></script>
    <script>
        $('#tujuantambah').select2({
            width: 'resolve',
            theme: 'default',
            dropdownParent: $('#tambahmodal'),
        });
        function ubah() {
            document.getElementById("myform").submit();
        }
        function tambah() {
            document.getElementById("formtambah").submit();
        }
    </script>
    <script>
        $('.m_select2_modal').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#m_select2_modal").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
            })
            .done(function(response) {
                $("#id").val(response.id);
                $("#message").val(response.message);
            });

        });
    </script>
@endsection
