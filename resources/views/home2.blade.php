@extends('templatefrontend')

@section('header')

<div class="main-header main-header-fullwidth main-header-has-header-standard">

	<!-- Header Standard Landing  -->

	<div class="header--standard header--standard-landing" id="header--standard">
		<div class="container">
			<div class="header--standard-wrap">

				<a href="#" class="logo">
					<div class="img-wrap">
						<img src="{{ asset('img/logo_bssn.png') }}" style="width:30px; height:30px;">
					</div>
					<div class="title-block">
						<h6 class="logo-title">Aplikasi Inpassing Jabatan</h6>
						<div class="sub-title">Badan Siber dan Sandi Negara</div>
					</div>
				</a>

				<a href="#" class="open-responsive-menu js-open-responsive-menu">
					<svg class="olymp-menu-icon"><use xlink:href="{{ asset('img/logo_bssn.png') }}"></use></svg>
				</a>

				<div class="nav nav-pills nav1 header-menu">
					<div class="mCustomScrollbar">
						<ul>
							<li>
								<a href="{{ url('/') }}" class="nav-link">&emsp;&emsp;Home</a>
							</li>
							<li>
								<a href="{{ url('berita') }}" class="nav-link">Berita</a>
							</li>
							<li>
								<a href="{{ url('bantuan') }}" class="nav-link">Bantuan</a>
							</li>
							<li>
								<a href="{{ url('hubungikami') }}" class="nav-link">Hubungi Kami</a>
							</li>
							<li>
								<a href="{{ url('registercalonsandiman') }}" class="nav-link">Registrasi</a>
							</li>
							<li class="close-responsive-menu js-close-responsive-menu">
								<svg class="olymp-close-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- ... end Header Standard Landing  -->
	<div class="header-spacer--standard"></div>

	<div class="content-bg-wrap bg-landing"></div>

	<div class="container">
		<div class="row display-flex">
			<div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
				<div class="landing-content">
					<h1>Selamat Datang!</h1>
					<p>Bagi Anda yang berprofesi sebagai sandiman, silakan manfaatkan aplikasi ini untuk
						mendapatkan informasi terbaru seputar Inpassing Jabatan.
					</p>
					<a href="#" class="btn btn-md btn-border c-white">Daftar Sekarang!</a>
				</div>
			</div>

			<div class="col col-xl-5 ml-auto col-lg-6 col-md-12 col-sm-12 col-12">


				<!-- Login-Registration Form  -->

				<div class="registration-login-form">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#home" role="tab">
								<svg class="olymp-login-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-login-icon') }}"></use></svg>
							</a>
						</li>
						{{-- <li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#profile" role="tab">
								<svg class="olymp-register-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-register-icon') }}"></use></svg>
							</a>
						</li> --}}
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						{{-- <div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
							<div class="title h6">Register to Olympus</div>
							<form class="content">
								<div class="row">
									<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
										<div class="form-group label-floating is-empty">
											<label class="control-label">First Name</label>
											<input class="form-control" placeholder="" type="text">
										</div>
									</div>
									<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
										<div class="form-group label-floating is-empty">
											<label class="control-label">Last Name</label>
											<input class="form-control" placeholder="" type="text">
										</div>
									</div>
									<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
										<div class="form-group label-floating is-empty">
											<label class="control-label">Your Email</label>
											<input class="form-control" placeholder="" type="email">
										</div>
										<div class="form-group label-floating is-empty">
											<label class="control-label">Your Password</label>
											<input class="form-control" placeholder="" type="password">
										</div>

										<div class="form-group date-time-picker label-floating">
											<label class="control-label">Your Birthday</label>
											<input name="datetimepicker" value="10/24/1984" />
											<span class="input-group-addon">
												<svg class="olymp-calendar-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-calendar-icon') }}"></use></svg>
											</span>
										</div>

										<div class="form-group label-floating is-select">
											<label class="control-label">Your Gender</label>
											<select class="selectpicker form-control">
												<option value="MA">Male</option>
												<option value="FE">Female</option>
											</select>
										</div>

										<div class="remember">
											<div class="checkbox">
												<label>
													<input name="optionsCheckboxes" type="checkbox">
													I accept the <a href="#">Terms and Conditions</a> of the website
												</label>
											</div>
										</div>

										<a href="#" class="btn btn-purple btn-lg full-width">Complete Registration!</a>
									</div>
								</div>
							</form>
						</div> --}}

						<div class="tab-pane active" id="profile" role="tabpanel" data-mh="log-tab">
							<div class="title h6">Masuk ke Akun Anda</div>
							<form class="content" method="post" action="{{ route('login') }}">
								@csrf
								<div class="row">
									<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
										<div class="form-group label-floating is-empty">
											<label class="control-label">Email</label>
											<input class="form-control" placeholder="" type="email" name="email">
										</div>
										<div class="form-group label-floating is-empty">
											<label class="control-label">Password</label>
											<input class="form-control" placeholder="" type="password" name="password">
										</div>

										<div class="remember">

											<div class="checkbox">
												<label>
													<input name="optionsCheckboxes" type="checkbox">
													Remember Me
												</label>
											</div>
											<a href="#" class="forgot">Forgot my Password</a>
										</div>

										<button class="btn btn-lg btn-primary full-width" type="submit">Login</button>

										{{-- <div class="or"></div>

										<a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fab fa-facebook-f" aria-hidden="true"></i>Login with Facebook</a>

										<a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><i class="fab fa-twitter" aria-hidden="true"></i>Login with Twitter</a> --}}


										<p>Don’t you have an account? <a href="#">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>

				<!-- ... end Login-Registration Form  -->
			</div>
		</div>
	</div>

	<img class="img-bottom" src="{{ asset('img/group-bottom.png') }}" alt="friends">
	{{-- <img class="img-rocket" src="{{ asset('img/rocket.png') }}" alt="rocket"> --}}
</div>

@endsection

@section('content')

<section class="medium-padding120">
	<div class="container">
		<div class="row">
			<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				<img src="{{ asset('img/icon-fly.png') }}" alt="screen">
			</div>

			<div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
				<div class="crumina-module crumina-heading">
					<h2 class="heading-title">Mengapa harus bergabung di <span class="c-primary">Aplikasi Inpassing</span>?</h2>
					<p class="heading-text">Di aplikasi ini, Anda akan mendapatkan informasi-informasi penting terkait dengan
						profesi Anda. Termasuk di dalamnya adalah penilaian jabatan dan inpassing.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="medium-padding120">
	<div class="container">
		<div class="row">
			<div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
				<div class="crumina-module crumina-heading">
					<h2 class="heading-title">Bergabung bersama Sandiman <span class="c-primary">di seluruh Indonesia</span></h2>
					<p class="heading-text">Portal ini dapat diakses bagi Sandiman yang ada di seluruh wilayah Indonesia.
					</p>
				</div>
			</div>

			<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				<img src="{{ asset('img/image1.png') }}" alt="screen">
			</div>
		</div>
	</div>
</section>

<section class="medium-padding120">
	<div class="container">
		<div class="row">
			<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				<img src="{{ asset('img/image2.png') }}" alt="screen">
			</div>

			<div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
				<div class="crumina-module crumina-heading">
					<h2 class="heading-title">Menggunakan UI/UX <span class="c-primary">Dengan Fitur Menarik</span></h2>
					<p class="heading-text">Portal ini didesain supaya menarik dan memudahkan Anda untuk mendapatkan informasi
						terkait profesi Anda.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="medium-padding120">
	<div class="container">
		<div class="row">
			<div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
				<div class="crumina-module crumina-heading">
					<h2 class="heading-title">Manfaatkan fitur <span class="c-primary">Inpassing & Penilaian</span></h2>
					<p class="heading-text">Portal ini menyediakan fitur-fitur yang dapat memudahkan Anda dalam berprofesi sebagai Sandiman.
					</p>
				</div>
			</div>

			<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				<img src="{{ asset('img/image3.png') }}" alt="screen">
			</div>
		</div>
	</div>
</section>

<!-- Planer Animation -->

<section class="medium-padding120 bg-section3 background-cover planer-animation">
	<div class="container">
		<div class="row mb60">
			<div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
				<div class="crumina-module crumina-heading align-center">
					<div class="heading-sup-title">Social Network</div>
					<h2 class="h1 heading-title">Review Komunitas</h2>
					<p class="heading-text">Pendapat dari beberapa orang yang telah menggunakan aplikasi ini.</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="swiper-container pagination-bottom" data-show-items="3">
				<div class="swiper-wrapper">
					<div class="ui-block swiper-slide">


						<!-- Testimonial Item -->

						<div class="crumina-module crumina-testimonial-item">
							<div class="testimonial-header-thumb"></div>

							<div class="testimonial-item-content">

								<div class="author-thumb">
									<img src="{{ asset('img/avatar91.jpg') }}" alt="author">
								</div>

								<h3 class="testimonial-title">Desain Menarik!</h3>

								<ul class="rait-stars">
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>

									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="far fa-star star-icon"></i>
									</li>
								</ul>

								<p class="testimonial-message">Pertama kali melihat website ini, saya tertarik dengan desainnya
									yang memanfaatkan warna-warna cerah.
                                </p>

								<div class="author-content">
									<a href="#" class="h6 author-name">Fajar Hariyanto</a>
									<div class="country">Badan Siber dan Sandi Negara</div>
								</div>
							</div>
						</div>

						<!-- ... end Testimonial Item -->
					</div>

					<div class="ui-block swiper-slide">


						<!-- Testimonial Item -->

						<div class="crumina-module crumina-testimonial-item">
							<div class="testimonial-header-thumb"></div>

							<div class="testimonial-item-content">

								<div class="author-thumb">
									<img src="{{ asset('img/avatar92.jpg') }}" alt="author">
								</div>

								<h3 class="testimonial-title">Mudah Digunakan!</h3>

								<ul class="rait-stars">
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>

									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
								</ul>

								<p class="testimonial-message">Fitur yang ditawarkan dapat dengan mudah untuk digunakan oleh pengguna.
									Informasi yang dijelaskan pun dapat dipahami.
								</p>

								<div class="author-content">
									<a href="#" class="h6 author-name">S.Perdana Hasibuan</a>
									<div class="country">Badan Siber dan Sandi Negara</div>
								</div>
							</div>
						</div>

						<!-- ... end Testimonial Item -->

					</div>

					<div class="ui-block swiper-slide">


						<!-- Testimonial Item -->

						<div class="crumina-module crumina-testimonial-item">
							<div class="testimonial-header-thumb"></div>

							<div class="testimonial-item-content">

								<div class="author-thumb">
									<img src="{{ asset('img/avatar93.jpg') }}" alt="author">
								</div>

								<h3 class="testimonial-title">Fitur Lengkap!</h3>

								<ul class="rait-stars">
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>

									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="fa fa-star star-icon"></i>
									</li>
									<li>
										<i class="far fa-star star-icon"></i>
									</li>
								</ul>

								<p class="testimonial-message">Fitur yang ditawarkan telah memenuhi kebutuhan untuk
									penilaian dan inpassing jabatan Sandiman. Saya merasa dimudahkan.
								</p>

								<div class="author-content">
									<a href="#" class="h6 author-name">Rizki Barokah</a>
									<div class="country">Badan Siber dan Sandi Negara</div>
								</div>
							</div>
						</div>

						<!-- ... end Testimonial Item -->
					</div>
				</div>

				<div class="swiper-pagination"></div>
			</div>
		</div>
	</div>

	<img src="{{ asset('img/planer.png') }}" alt="planer" class="planer">
</section>

<!-- ... end Section Planer Animation -->

<!-- Section Subscribe Animation -->

<section class="medium-padding100 subscribe-animation scrollme bg-users">
	<div class="container">
		<div class="row">
			<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				<div class="crumina-module crumina-heading c-white custom-color">
					<h2 class="h1 heading-title">Segera Bergabung</h2>
					<p class="heading-text">Bergabunglah dan dapatkan informasi terkini terkait profesi Anda!
					</p>
				</div>


				<!-- Subscribe Form  -->

				{{-- <form class="form-inline subscribe-form" method="post">
					<div class="form-group label-floating is-empty">
						<label class="control-label">Enter your email</label>
						<input class="form-control bg-white" placeholder="" type="email">
					</div>

					<button class="btn btn-blue btn-lg">Send</button>
				</form> --}}

				<!-- ... end Subscribe Form  -->

			</div>
		</div>

		<img src="{{ asset('img/paper-plane.png') }}" alt="plane" class="plane">
	</div>
</section>

<!-- ... end Section Subscribe Animation -->

<section class="medium-padding120">
	<div class="container">
		<div class="row mb60">
			<div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
				<div class="crumina-module crumina-heading align-center">
					<div class="heading-sup-title">BLOG</div>
					<h2 class="h1 heading-title">Info Terbaru</h2>
					<p class="heading-text">Informasi terbaru dari pembina profesi Anda.</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


				<!-- Post -->

				<article class="hentry blog-post">

					<div class="post-thumb">
						<img src="{{ asset('img/post1.jpg') }}" alt="photo">
					</div>

					<div class="post-content">
						<a href="#" class="post-category bg-blue-light">THE COMMUNITY</a>
						<a href="#" class="h4 post-title">Here’s the Featured Urban photo of August! </a>
						<p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new catalog.</p>

						<div class="author-date">
							by
							<a class="h6 post__author-name fn" href="#">Maddy Simmons</a>
							<div class="post__date">
								<time class="published" datetime="2017-03-24T18:18">
									- 7 hours ago
								</time>
							</div>
						</div>
					</div>

				</article>

				<!-- ... end Post -->
			</div>
			<div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


				<!-- Post -->

				<article class="hentry blog-post">

					<div class="post-thumb">
						<img src="{{ asset('img/post2.jpg') }}" alt="photo">
					</div>

					<div class="post-content">
						<a href="#" class="post-category bg-primary">OLYMPUS NEWS</a>
						<a href="#" class="h4 post-title">Olympus Network added new photo filters!</a>
						<p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new catalog.</p>

						<div class="author-date">
							by
							<a class="h6 post__author-name fn" href="#">JACK SCORPIO</a>
							<div class="post__date">
								<time class="published" datetime="2017-03-24T18:18">
									- 12 hours ago
								</time>
							</div>
						</div>
					</div>

				</article>

				<!-- ... end Post -->
			</div>
			<div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


				<!-- Post -->

				<article class="hentry blog-post">

					<div class="post-thumb">
						<img src="{{ asset('img/post3.jpg') }}" alt="photo">
					</div>

					<div class="post-content">
						<a href="#" class="post-category bg-purple">INSPIRATION</a>
						<a href="#" class="h4 post-title">Take a look at these truly awesome worspaces</a>
						<p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new catalog.</p>

						<div class="author-date">
							by
							<a class="h6 post__author-name fn" href="#">Maddy Simmons</a>
							<div class="post__date">
								<time class="published" datetime="2017-03-24T18:18">
									- 2 days ago
								</time>
							</div>
						</div>
					</div>

				</article>

				<!-- ... end Post -->
			</div>
		</div>
	</div>
</section>

@endsection
