@extends('templatefrontend')


@section('header')

    <!-- Stunning header -->

    <div class="stunning-header bg-primary-opacity">

        <!-- Header Standard Landing  -->

        <div class="header--standard header--standard-landing" id="header--standard">
            <div class="container">
                <div class="header--standard-wrap">

                    <a href="#" class="logo">
                        <div class="img-wrap">
                            <img src="{{ asset('img/logo_bssn.png') }}" style="width:30px; height:30px;">
                        </div>
                        <div class="title-block">
                            <h6 class="logo-title">Aplikasi Inpassing Jabatan</h6>
                            <div class="sub-title">Badan Siber dan Sandi Negara</div>
                        </div>
                    </a>

                    <a href="#" class="open-responsive-menu js-open-responsive-menu">
                        <svg class="olymp-menu-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                    </a>

                    <a href="#" class="open-responsive-menu">Home</a>

                    <div class="nav nav-pills nav1 header-menu">
                        <div class="mCustomScrollbar">
                            <ul>
                                <li>
                                    <a href="{{ url('/') }}" class="nav-link">&emsp;&emsp;Home</a>
                                </li>
                                <li>
                                    <a href="{{ url('berita') }}" class="nav-link">Berita</a>
                                </li>
                                <li>
                                    <a href="{{ url('bantuan') }}" class="nav-link">Bantuan</a>
                                </li>
                                <li>
                                    <a href="{{ url('hubungikami') }}" class="nav-link">Hubungi Kami</a>
                                </li>
                                <li>
                                    <a href="{{ url('registercalonsandiman') }}" class="nav-link">Registrasi</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ... end Header Standard Landing  -->

        <div class="header-spacer--standard"></div>

        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Registrasi</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="#">Home</a>
                    <span class="icon breadcrumbs-custom">/</span>
                </li>
                <li class="breadcrumbs-item active">
                    <span>Akun</span>
                </li>
            </ul>
        </div>

        <div class="content-bg-wrap stunning-header-bg1"></div>
    </div>

    <!-- End Stunning header -->

@endsection


@section('content')

    <ul class="cat-list-bg-style align-center sorting-menu">
        <li class="cat-list__item active"><a href="{{ url('registercalonsandiman') }}" class="">CALON SANDIMAN</a></li>
        <li class="cat-list__item"><a href="{{ url('registersandiman') }}" class="">SANDIMAN</a></li>
        <li class="cat-list__item"><a href="{{ url('registeroperator') }}" class="">OPERATOR</a></li>
    </ul>
    @if ($message = Session::get('success') OR $message = Session::get('error') OR $errors->any())
        <div class="container">
            <div class="row">
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if ($message = Session::get('success'))
                        <div class="bg-blue" style="color: #fff; padding: 15px; display: font-size: 10px; font-weight: bold;">
                            {{ $message }}
                        </div>
                    @endif
                    @if ($message = Session::get('error'))
                        <div class="bg-grey" style="color: #fff; padding: 15px; display: font-size: 10px; font-weight: bold;">
                            {{ $message }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="bg-grey" style="color: #fff; padding: 15px; display: font-size: 10px; font-weight: bold;">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
    <form method="POST" action="{{ route('registers', ['jenis' => 'peserta inpassing']) }}" enctype="multipart/form-data">
        @csrf
        <div class="container">
            <div class="row">
                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                    <h2 class="presentation-margin">Registrasi Akun Calon Sandiman / INPASSING</h2>
                    <div class="ui-block">
                        <div class="ui-block-content">
                            <div class="row">
                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h4>Data Diri</h4>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">NIP (ditulis tanpa spasi)</label>
                                        <input class="form-control" type="text" placeholder="" name="nip" onkeyup="this.value=this.value.replace(/[^\d]/,'')" required>
                                    </div>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Nama</label>
                                        <input class="form-control" type="text" placeholder="" name="nama" required>
                                    </div>
                                    {{-- <div class="form-group label-floating is-empty">
                                        <label class="control-label">Gelar Depan</label>
                                        <input class="form-control" type="text" placeholder="" name="gelar_depan">
                                    </div>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Gelar Belakang</label>
                                        <input class="form-control" type="text" placeholder="" name="gelar_belakang">
                                    </div> --}}
                                    <fieldset class="form-group">
                                        <select class="selectpicker form-control" name="jenis_kelamin" required>
                                            <option value="">--Jenis Kelamin--</option>
                                            <option value="laki-laki">Laki Laki</option>
                                            <option value="perempuan">Perempuan</option>
                                        </select>
                                    </fieldset>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Tempat Lahir</label>
                                        <input class="form-control" type="text" placeholder="" name="tempat_lahir" required>
                                    </div>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Tanggal Lahir</label>
                                        <input class="form-control" id="datetimepicker" type="text" placeholder="" name="tanggal_lahir" required>
                                    </div>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Alamat</label>
                                        <input class="form-control" type="text" placeholder="" name="alamat" required>
                                    </div>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Email</label>
                                        <input class="form-control" type="email" placeholder="" name="email" required>
                                    </div>
                                    <fieldset class="form-group">
                                        <select class="selectpicker form-control" name="tingkat_pendidikan" required>
                                            <option value="">--Pilih Tingkat Pendidikan--</option>
                                            <option value="SMA">SMA</option>
                                            <option value="MA">MA</option>
                                            <option value="SMK">SMK</option>
                                            <option value="STM">STM</option>
                                            <option value="ST">ST</option>
                                            <option value="D1">D-I</option>
                                            <option value="D2">D-II</option>
                                            <option value="D3">D-III</option>
                                            <option value="D4">D-IV</option>
                                            <option value="S1">S-1</option>
                                            <option value="S2">S-2</option>
                                            <option value="S3">S-3</option>
                                        </select>
                                    </fieldset>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Jurusan Pendidikan</label>
                                        <input class="form-control" type="text" placeholder="" name="jurusan_pendidikan" required>
                                    </div>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">No Karpeg</label>
                                        <input class="form-control" type="text" placeholder="" name="no_karpeg">
                                    </div>
                                    <fieldset class="form-group">
                                        <select class="selectpicker form-control" name="pangkat_gol" required>
                                            <option value="">--Pilih Pangkat/Golongan--</option></option>
                                            <option value="Juru Muda (I/a)">Juru Muda (I/a)</option>
                                            <option value="Juru Muda Tk I (I/b)">Juru Muda Tk.I (I/b)</option>
                                            <option value="Juru (I/c)">Juru (I/c)</option>
                                            <option value="Juru Tk I (I/d)">Juru Tk.I (I/d)</option>
                                            <option value="Pengatur Muda (II/a)">Pengatur Muda (II/a)</option>
                                            <option value="Pengatur Muda Tk I (II/b)">Pengatur Muda Tk.I (II/b)</option></option>
                                            <option value="Pengatur (II/c)">Pengatur (II/c)</option>
                                            <option value="Pengatur Tk I (II/d)">Pengatur Tk.I (II/d)</option>
                                            <option value="Penata Muda (III/a)">Penata Muda (III/a)</option>
                                            <option value="Penata Muda Tk I (III/b)">Penata Muda Tk.I (III/b)</option></option>
                                            <option value="Penata (III/c)">Penata (III/c)</option>
                                            <option value="Penata Tk I (III/d)">Penata Tk.I (III/d)</option>
                                            <option value="Pembina (IV/a)">Pembina (IV/a)</option>
                                            <option value="Pembina Tk I (IV/b)">Pembina Tk.I (IV/b)</option>
                                            <option value="Pembina Utama Muda (IV/c)">Pembina Utama Muda (IV/c)</option>
                                            <option value="Pembina Utama Madya (IV/d)">Pembina Utama Madya (IV/d)</option>
                                        </select>
                                    </fieldset>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">TMT Golongan</label>
                                        <input class="form-control" id="datepicker2" type="text" placeholder="" name="tmt_gol" required>
                                    </div>
                                    <fieldset class="form-group">
                                        <select class="selectpicker form-control" name="masa_kerja_thn" required>
                                            <option value="">--Masa Kerja (tahun)--</option>
                                            <option value="1">1 tahun</option>
                                            <option value="2">2 tahun</option>
                                            <option value="3">3 tahun</option>
                                            <option value="4">4 tahun</option>
                                            <option value="5">5 tahun</option>
                                            <option value="6">6 tahun</option>
                                            <option value="7">7 tahun</option>
                                            <option value="8">8 tahun</option>
                                            <option value="9">9 tahun</option>
                                            <option value="10">10 tahun</option>
                                            <option value="11">11 tahun</option>
                                            <option value="12">12 tahun</option>
                                            <option value="13">13 tahun</option>
                                            <option value="14">14 tahun</option>
                                            <option value="15">15 tahun</option>
                                            <option value="16">16 tahun</option>
                                            <option value="17">17 tahun</option>
                                            <option value="18">18 tahun</option>
                                            <option value="19">19 tahun</option>
                                            <option value="20">20 tahun</option>
                                        </select>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <select class="selectpicker form-control" name="masa_kerja_bln" required>
                                            <option value="">--Masa Kerja (bulan)--</option>
                                            <option value="1">1 bulan</option>
                                            <option value="2">2 bulan</option>
                                            <option value="3">3 bulan</option>
                                            <option value="4">4 bulan</option>
                                            <option value="5">5 bulan</option>
                                            <option value="6">6 bulan</option>
                                            <option value="7">7 bulan</option>
                                            <option value="8">8 bulan</option>
                                            <option value="9">9 bulan</option>
                                            <option value="10">10 bulan</option>
                                            <option value="11">11 bulan</option>
                                        </select>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <select class="selectpicker form-control" name="jabatan_yg_diajukan" required>
                                            <option value="">--Pilih Jabatan Yg Diajukan--</option>
                                            <option value="Sandiman Pelaksana Pemula">Sandiman Pelaksana Pemula</option>
                                            <option value="Sandiman Pelaksana">Sandiman Pelaksana</option>
                                            <option value="Sandiman Pelaksana Lanjutan">Sandiman Pelaksana Lanjutan</option>
                                            <option value="Sandiman Penyelia">Sandiman Penyelia</option>
                                            <option value="Sandiman Pertama">Sandiman Pertama</option>
                                            <option value="Sandiman Muda">Sandiman Muda</option>
                                            <option value="Sandiman Madya">Sandiman Madya</option>
                                            <option value="Sandiman Utama">Sandiman Utama</option>
                                        </select>
                                    </fieldset>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">No SK PNS/KP Terakhir</label>
                                        <input class="form-control" type="text" placeholder="" name="no_sk_pns_terakhir" required>
                                    </div>
                                    <label class="control-label">File SK PNS/KP Terakhir (PDF)</label>
                                    <input class="form-control" type="file" name="file_sk_pns_terakhir" required><br/><br/>
                                    <h4>Data Instansi</h4>
                                    <fieldset class="form-group">
                                        <select class="select2 form-control" name="instansi" required>
                                            <option value="">--Pilih Instansi--</option>
                                            <option value="Pemerintah Daerah">Pemerintah Daerah (Pemda)</option>
                                            <option value="Badan Siber dan Sandi Negara">Badan Siber dan Sandi Negara (BSSN)</option>
                                            <option value="Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah">Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah (LKPP)</option>
                                            <option value="Kantor Staf Presiden RI">Kantor Staf Presiden RI</option>
                                            <option value="Palang Merah Indonesia">Palang Merah Indonesia (PMI)</option>
                                            <option value="Badan Pusat Statistik">Badan Pusat Statistik</option>
                                            <option value="Japanese International Cooperation Agency">Japanese International Coorperation Agency (JICA)</option>
                                            <option value="Kementerian Koordinator Bidang Polhukam">Kementerian Koordinator Bidang Polhukam</option>
                                            <option value="Kementerian Koordinator Bidang Kesra">Kementerian Koordinator Bidang Kesra</option>
                                            <option value="Kementerian Koordinator Bidang Perekonomian">Kementerian Koordinator Bidang Perekonomian</option>
                                            <option value="Kementerian Dalam Negeri">Kementerian Dalam Negeri</option>
                                            <option value="Kementerian Luar Negeri">Kementerian Luar Negeri</option>
                                            <option value="Kementerian Pertahanan">Kementerian Pertahanan</option>
                                            <option value="Kementerian Hukum dan HAM">Kementerian Hukum Dan Ham</option>
                                            <option value="Kementerian Keuangan">Kementerian Keuangan</option>
                                            <option value="Kementerian ESDM">Kementerian ESDM</option>
                                            <option value="Kementerian Perindustrian">Kementerian Perindustrian</option>
                                            <option value="Kementerian Perdagangan">Kementerian Perdagangan</option>
                                            <option value="Kementerian Pertanian">Kementerian Pertanian </option>
                                            <option value="Kementerian Kehutanan">Kementerian Kehutanan</option>
                                            <option value="Kementerian Perhubungan">Kementerian Perhubungan</option>
                                            <option value="Kementerian Kelautan dan Perikanan">Kementerian Kelautan dan Perikanan</option>
                                            <option value="Kementerian Tenaga Kerja dan Transmigrasi">Kementerian Tenaga Kerja dan Transmigrasi</option>
                                            <option value="Kementerian Kesehatan">Kementerian Kesehatan</option>
                                            <option value="Kementerian Pekerjaan Umum">Kementerian Pekerjaan Umum</option>
                                            <option value="Kementerian Pendidikan dan Kebudayaan">Kementerian Pendidikan dan Kebudayaan</option>
                                            <option value="Kementerian Sosial">Kementerian Sosial</option>
                                            <option value="Kementerian Agama">Kementerian Agama</option>
                                            <option value="Kementerian Pariwisata dan Ekonomi Kreeatif">Kementerian Pariwisata dan Ekonomi Kreatif</option>
                                            <option value="Kementerian Komunikasi dan Informatika">Kementerian Komunikasi dan Informatika</option>
                                            <option value="Kementerian Negara Riset dan Teknologi">Kementerian Negara Riset dan Teknologi</option>
                                            <option value="Kementerian Koperasi dan Usaha Kecil dan Menengah">Kementerian Koperasi dan Usaha Kecil dan Menengah</option>
                                            <option value="Kementerian Lingkungan Hidup">Kementerian Lingkungan Hidup</option>
                                            <option value="Kementerian Negara Pemberdayaan Perempuan dan Perlindungan Anak">Kementerian Negara Pemberdayaan Perempuan dan Perlindungan anak</option>
                                            <option value="Kementerian Perencanaan Pembangunan Nasional">Kementerian Perencanaan Pembangunan Nasional (Bappenas)</option>
                                            <option value="Kementerian PAN-RB">Kementerian PAN-RB</option>
                                            <option value="Kementerian Negara Pembangunan Daerah Tertinggal">Kementerian Negara Pembangunan Daerah Tertinggal</option>
                                            <option value="Kementerian BUMN">Kementerian BUMN</option>
                                            <option value="Kementerian Negara Perumahan Rakyat">Kementerian Negara Perumahan Rakyat</option>
                                            <option value="Kementerian Pemuda dan Olahraga">Kementerian Pemuda dan Olahraga</option>
                                            <option value="Kementerian Sekretariat Negara">Kementerian Sekretariat Negara</option>
                                            <option value="Arsip Nasional RI">Arsip Nasional RI (ANRI)</option>
                                            <option value="Lembaga Administrasi Negara">Lembaga Administrasi Negara (LAN)</option>
                                            <option value="Badan Kepegawaian Negara">Badan Kepegawaian Negara (BKN)</option>
                                            <option value="Perpustakaan Nasional">Perpustakaan Nasional (PERPUSNAS)</option>
                                            <option value="Badan Intelijen Negara">Badan Inteljen Negara (BIN)</option>
                                            <option value="Badan Kependudukan dan Keluarga Berencana Nasional">Badan Kependudukan dan Keluarga Berencana Nasional (BKKBN)</option>
                                            <option value="Lembaga Penerbangan Antariksa Nasional">Lembaga Penerbangan Antariksa Nasional (LAPAN)</option>
                                            <option value="Badan Informasi Geospasial">Badan Informasi Geospasial (BIG)</option>
                                            <option value="Badan Pengawasan Keuangan dan Pembangunan">Badan Pengawasan Keuangan dan Pembangunan (BPKP)</option>
                                            <option value="Lembaga Ilmu Pengetahuan Indonesia">Lembaga Ilmu Pengetahuan Indonesia (LIPI)</option>
                                            <option value="Badan Pengkajian dan Penerapan Teknologi">Badan Pengkajian dan Penerapan Teknologi (BPPT)</option>
                                            <option value="Badan Koordinasi Penanaman Modal">Badan Koordinasi Penanaman Modal (BKPM)</option>
                                            <option value="Badan Pertanahan Nasional">Badan Pertanahan Nasional (BPN)</option>
                                            <option value="Badan Pengawasan Obat dan Makanan">Badan Pengawasan Obat dan Makanan (BPOM)</option>
                                            <option value="Lembaga Ketahanan Nasional">Lembaga Ketahanan Nasional (LEMHANAS)</option>
                                            <option value="Badan Meteorologi, Klimatologi, dan Geofisika">Badan Meteorologi, Klimatologi, dan Geofisika (BMKG)</option>
                                            <option value="Badan Nasional Penempatan dan Perlindungan Tenaga Kerja Indonesia">Badan Nasional Penempatan & Perlindungan Tenaga Kerja Indonesia (BNP2TKI)</option>
                                            <option value="Badan Nasional Penanggulangan Bencana">Badan Nasional Penanggulangan Bencana (BNPB)</option>
                                            <option value="Badan SAR Nasional">Badan SAR Nasional</option>
                                            <option value="Badan Narkotika Nasional">Badan Narkotika Nasional (BNN)</option>
                                            <option value="Badan Standarisasi Nasional">Badan Standarisasi Nasional / BS</option>
                                            <option value="Badan Tenaga Nuklir Nasional">Badan Tenaga Nuklir Nasional</option>
                                            <option value="Badan Pengawas Tenaga Nuklir">Badan Pengawas Tenaga Nuklir / BAPETE</option>
                                            <option value="Badan Nasional Penanggulangan Terorisme">Badan Nasional Penanggulangan Terorisme RI (BNPT)</option>
                                            <option value="Kepolisian Negara RI">Kepolisian Negara RI</option>
                                            <option value="Kejaksanaan Agung RI">Kejaksaan Agung RI</option>
                                            <option value="Sekretariat Kabinet">Sekretariat Kabinet </option>
                                            <option value="Sekretariat Jendral BPK">Sekretariat Jendral BPK</option>
                                            <option value="Sekretariat Jendral MPR">Sekretariat Jendral MPR</option>
                                            <option value="Sekretariat Jendral DPR">Sekretariat Jendral DPR</option>
                                            <option value="Sekretariat Mahkamah Agung">Sekretariat Mahkamah Agung</option>
                                            <option value="Sekretariat Jendral DPD-RI">Sekretariat Jendral DPD-RI</option>
                                            <option value="Sekretariat Mahkamah Konstitusi">Sekretariat Mahkamah Kostitusi</option>
                                            <option value="Sekretariat Komisi Yudisial">Sekretariat Komisi Yudisial</option>
                                            <option value="Sekretariat Komisi Nasional HAM">Sekretariat Komisi Nasional HAM</option>
                                            <option value="Sekretariat KPU">Sekretariat KPU</option>
                                            <option value="Sekretariat Ombudsman RI">Sekretariat OMBUDSMAN RI</option>
                                            <option value="Badan Koordinasi Keamanan Laut">Badan Koordinasi Keamanan Laut (BAKORKAMLA)</option>
                                            <option value="Pusat Pelaporan dan Analisis Transaksi Keuangan">PPATK</option>
                                            <option value="Bank Indonesia">Bank Indonesia (BI)</option>
                                            <option value="Pusdiklat Badan Pusat Statistik">Pusdiklat BPS</option>
                                            <option value="Sekretariat Asean">Sekretariat Asean</option>
                                            <option value="Asian and Pasific Coconut Community">Asian and Pasific Coconut Community (APCC)</option>
                                            <option value="Komisi Pengawas Persaingan Usaha Republik Indonesia">Komisi Pengawas Persaingan Usaha Republik Indonesia</option>
                                            <option value="Kantor Sekretariat Wakil Presiden">Kantor Sekretariat Wakil Presiden</option>
                                            <option value="Badan Ekonomi Kreatif">Badan Ekonomi Kreatif Nasional</option>
                                            <option value="Badan Pengawas Pemilu">Badan Pengawas Pemilihan Umum (Bawaslu) RI</option>
                                            <option value="Komisi Aparatur Sipil Negara">Komisi Aparatur Sipil Negara (KASN)</option>
                                            <option value="Komisi Pemberantasan Korupsi">Komisi Pemberantasan Korupsi (KPK)</option>
                                            <option value="Lainnya">Lainnya</option>
                                        </select>
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <select class="select2 form-control" name="provinsi" required>
                                            <option value="">--Pilih Provinsi--</option>
                                            <option value="Aceh">Aceh</option>
                                            <option value="Sumatera Utara">Sumatera Utara</option>
                                            <option value="Sumatera Barat">Sumatera Barat</option>
                                            <option value="Riau">Riau</option>
                                            <option value="Jambi">Jambi</option>
                                            <option value="Sumatera Selatan">Sumatera Selatan</option>
                                            <option value="Bengkulu">Bengkulu</option>
                                            <option value="Lampung">Lampung</option>
                                            <option value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung</option>
                                            <option value="Kepulauan Riau">Kepulauan Riau</option>
                                            <option value="DKI Jakarta">DKI Jakarta</option>
                                            <option value="Jawa Barat">Jawa Barat</option>
                                            <option value="Jawa Tengah">Jawa Tengah</option>
                                            <option value="DI Yogyakarta">DI Yogyakarta</option>
                                            <option value="Jawa Timur">Jawa Timur</option>
                                            <option value="Banten">Banten</option>
                                            <option value="Bali">Bali</option>
                                            <option value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                            <option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                            <option value="Kalimantan Barat">Kalimantan Barat</option>
                                            <option value="Kalimantan Tengah">Kalimantan Tengah</option>
                                            <option value="Kalimantan Selatan">Kalimantan Selatan</option>
                                            <option value="Kalimantan Timur">Kalimantan Timur</option>
                                            <option value="Kalimantan Utara">Kalimantan Utara</option>
                                            <option value="Sulawesi Utara">Sulawesi Utara</option>
                                            <option value="Sulawesi Tengah">Sulawesi Tengah</option>
                                            <option value="Sulawesi Selatan">Sulawesi Selatan</option>
                                            <option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                            <option value="Gorontalo">Gorontalo</option>
                                            <option value="Sulawesi Barat">Sulawesi Barat</option>
                                            <option value="Maluku">Maluku</option>
                                            <option value="Maluku Utara">Maluku Utara</option>
                                            <option value="Papua Barat">Papua Barat</option>
                                            <option value="Papua">Papua</option>
                                        </select>
                                    </fieldset>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Kabupaten/Kota</label></label>
                                        <input class="form-control" type="text" placeholder="" name="kabupaten" required>
                                    </div>
                                    </fieldset>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Unit Kerja</label>
                                        <input class="form-control" type="text" placeholder="" name="unit_kerja" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <button class="btn btn-blue btn-md full-width" type="submit">Simpan Registrasi</button>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript">
    $(".select2").select2({
        width: '100%'
    });
</script>
<script>
    $(function() {
        $( "#datepicker1" ).datepicker({
            dateFormat : 'dd-mm-yy',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            maxDate: '-1d'
        });
        $( "#datepicker2" ).datepicker({
            dateFormat : 'dd-mm-yy',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            maxDate: '-1d'
        });
    });
</script>

@endsection
