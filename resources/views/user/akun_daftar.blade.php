@extends('templatebackend')

@section('header')
    <link href="{{ asset('backend/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">Akun</h3>
                    </div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
                </div>
            </div>

            <!-- END: Subheader -->
            <div class="m-content">
                @if ($message = Session::get('success'))
                    <div class="alert alert-info" role="alert">
                        <strong>Berhasil!</strong> {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> {{ $message }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Daftar Akun Pengguna
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Email</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user as $i)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $i->email }}</td>
                                    <td>{{ $i->name }}</td>
                                    <td>
                                        @if($i->status == "active")
                                        <span class="m-badge m-badge--success m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-success">{{ ucwords($i->status) }}</span>
                                        @elseif($i->status == "pending")
                                        <span class="m-badge m-badge--warning m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-secondary">{{ ucwords($i->status) }}</span>
                                        @elseif($i->status == "nonactive")
                                        <span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-warning">{{ ucwords($i->status) }}</span>
                                        @endif
                                    </td>
                                    <td><a href="{{ url('akun_show', ['param' => $i->id]) }}" class="btn btn-info btn-sm mymodal">Lihat</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Akun</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="myform" method="post" action="{{ url('akun_update') }}">
                        @csrf @method('put')
                    <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="200">
                        <div class="form-group m-form__group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Nama:</label>
                            <div class="col-xl-9 col-lg-9">
                                <input type="text" name="nama" id="nama" class="form-control m-input" placeholder="" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Email:</label>
                            <div class="col-xl-9 col-lg-9">
                                <input type="text" name="email" id="email" class="form-control m-input" placeholder="" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Ubah Password:</label>
                            <div class="col-xl-9 col-lg-9">
                                <input type="text" name="password" id="password" class="form-control m-input" placeholder="Diisi ketika ingin mengganti password." value="">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Status:</label>
                            <div class="col-xl-9 col-lg-9">
                                <select name="status" id="status" class="form-control m-input">
                                    <option value=""></option>
                                    <option id="stat_active" value="active">Active</option>
                                    <option value="nonactive">Nonactive</option>
                                    <option value="pending">Pending</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" onclick="myFunction()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function myFunction() {
          document.getElementById("myform").submit();
        }
    </script>
    <script>
        $('.mymodal').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#mymodal").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
            })
            .done(function(response) {
                $("#nama").val(response.nama);
                $("#email").val(response.email);
                // if(response.status == "active"){
                //     $("#status").append(
                //         '<option value="' + response.status + '" selected' + '>&nbsp;&nbsp;' + response.status + '</option>'
                //     );
                //     $("#status").append(
                //         '<option value="nonactive">&nbsp;&nbsp;nonactive</option>'
                //     );
                //     $("#status").append(
                //         '<option value="pending">&nbsp;&nbsp;pending</option>'
                //     );
                // }
                // if(response.status == "nonactive"){
                //     $("#status").append(
                //         '<option value="' + response.status + '" selected' + '>' + response.status + '</option>'
                //     );
                //     $("#status").append(
                //         '<option value="active">&nbsp;&nbsp;active</option>'
                //     );
                //     $("#status").append(
                //         '<option value="pending">&nbsp;&nbsp;pending</option>'
                //     );
                // }
                // if(response.status == "pending"){
                //     $("#status").append(
                //         '<option value="' + response.status + '" selected' + '>&nbsp;&nbsp;' + response.status + '</option>'
                //     );
                //     $("#status").append(
                //         '<option value="active">&nbsp;&nbsp;active</option>'
                //     );
                //     $("#status").append(
                //         '<option value="nonactive">&nbsp;&nbsp;nonactive</option>'
                //     );
                // }
            });

        });
    </script>
    <script src="{{ asset('backend/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/demo5/base/scrollable.js') }}" type="text/javascript"></script>
@endsection
