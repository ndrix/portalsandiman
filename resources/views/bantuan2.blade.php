@extends('templatebaru')

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Bantuan</h3>
					</div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
				</div>
            </div>

			<!-- END: Subheader -->
			<div class="m-content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet m-portlet--full-height">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            FAQ
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">

                                <!--begin::Section-->
                                <div class="m-accordion m-accordion--default m-accordion--solid" id="m_accordion_3" role="tablist">

                                    <!--begin::Item-->
                                    <div class="m-accordion__item">
                                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="    false">
                                            <span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span>
                                            <span class="m-accordion__item-title">Bagaimana cara membuat akun di aplikasi ini ?</span>
                                            <span class="m-accordion__item-mode"></span>
                                        </div>
                                        <div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3">
                                            <div class="m-accordion__item-content">
                                                <p>
                                                    Melakukan pembuatan akun di aplikasi ini cukup mudah, Anda hanya perlu mengklik tombol Register / Daftar lalu melengkapi formulir pendaftaran. Selanjutnya tunggu konfirmasi dari Administrator.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end::Item-->

                                    <!--begin::Item-->
                                    <div class="m-accordion__item">
                                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_2_head" data-toggle="collapse" href="#m_accordion_3_item_2_body" aria-expanded="    false">
                                            <span class="m-accordion__item-icon"><i class="fa  flaticon-placeholder"></i></span>
                                            <span class="m-accordion__item-title">Fitur apa saja yang tersedia di aplikasi ini?</span>
                                            <span class="m-accordion__item-mode"></span>
                                        </div>
                                        <div class="m-accordion__item-body collapse" id="m_accordion_3_item_2_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_2_head" data-parent="#m_accordion_3">
                                            <div class="m-accordion__item-content">
                                                <p>
                                                    Aplikasi ini menyediakan fitur registrasi inpassing jabatan Sandiman secara online.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end::Item-->

                                    <!--begin::Item-->
                                    <div class="m-accordion__item">
                                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_3_head" data-toggle="collapse" href="#m_accordion_3_item_3_body" aria-expanded="    false">
                                            <span class="m-accordion__item-icon"><i class="fa  flaticon-alert-2"></i></span>
                                            <span class="m-accordion__item-title">Bagaimana cara menghubungi Administrator ?</span>
                                            <span class="m-accordion__item-mode"></span>
                                        </div>
                                        <div class="m-accordion__item-body collapse" id="m_accordion_3_item_3_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_3_head" data-parent="#m_accordion_3">
                                            <div class="m-accordion__item-content">
                                                <p>
                                                    Anda dapat menghubungi Administrator melalui nomor telefon Badan Siber dan Sandi Negara lalu memilih ekstensi Direktorat.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end::Item-->
                                </div>

                                <!--end::Section-->
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script src="{{ asset('backend/demo/demo5/base/html-table.js') }}" type="text/javascript"></script>
@endsection
