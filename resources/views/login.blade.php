@extends('templatebaru')

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Login</h3>
					</div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
				</div>
            </div>
			<div class="m-content">
                @if ($message = Session::get('success'))
                    <div class="alert alert-info" role="alert">
                        <strong>Berhasil!</strong> {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> {{ $message }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> Terdapat kesalahan dalam input Anda.
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            Form Login
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <form method="post" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group m-form__group row">
                                        <div class="col-xl-3"></div>
                                        <label class="col-xl-2 col-lg-2 col-form-label">Email:</label>
                                        <div class="col-xl-4 col-lg-4">
                                            <input type="text" name="email" class="form-control m-input" placeholder="Input Email..." required>
                                            <span class="m-form__help"></span>
                                        </div>
                                        <div class="col-xl-3"></div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-xl-3"></div>
                                        <label class="col-xl-2 col-lg-2 col-form-label">Password:</label>
                                        <div class="col-xl-4 col-lg-4">
                                            <input type="password" name="password" class="form-control m-input" placeholder="Input Password..." required>
                                            <span class="m-form__help"></span>
                                        </div>
                                        <div class="col-xl-3"></div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4 m--align-center">
                                            <button type="submit" class="btn btn-primary m-btn m-btn--custom m-btn--icon">Login</button>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script src="{{ asset('backend/demo/demo5/base/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/demo/demo5/base/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script>$.fn.datepicker.defaults.format = "dd-mm-yyyy";</script>
@endsection
