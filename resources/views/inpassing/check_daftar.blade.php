@extends('templatebackend')

@section('header')
    <link href="{{ asset('backend/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">Inpassing</h3>
                    </div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                            <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                <i class="la la-angle-down"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>

            <!-- END: Subheader -->
            <div class="m-content">
                @if ($message = Session::get('success'))
                    <div class="alert alert-info" role="alert">
                        <strong>Berhasil!</strong> {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> {{ $message }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
                <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Daftar Peserta Baru
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-xl-4 col-lg-4 col-form-label">* Tahun:</label>
                                <div class="col-xl-5 col-lg-5">
                                    <input type="text" name="tahun" class="form-control" placeholder="" value="{{ date('Y') }}" readonly>
                                    <span class="m-form__help"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-xl-4 col-lg-4 col-form-label">* Instansi:</label>
                                <div class="col-xl-5 col-lg-5">
                                    <input type="text" name="instansi" class="form-control" placeholder="" value="{{ $formasi->instansi }}" readonly>
                                    <span class="m-form__help"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-xl-4 col-lg-4 col-form-label">* Provinsi:</label>
                                <div class="col-xl-5 col-lg-5">
                                    <input type="text" name="provinsi" class="form-control" placeholder="" value="{{ $formasi->provinsi }}" readonly>
                                    <span class="m-form__help"></span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-xl-4 col-lg-4 col-form-label">* Jenis Jabatan:</label>
                                <div class="col-xl-5 col-lg-5">
                                    <input type="text" name="jabatan" class="form-control" readonly="" placeholder="" value="{{ $formasi->jabatan }}">
                                    <span class="m-form__help"></span>
                                </div>
                            </div><br><br><br>
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIP</th>
                                        <th>Email</th>
                                        <th>Instansi</th>
                                        <th>Formasi</th>
                                        <th>Status</th>
                                        <th>Tanggal Ujian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($inpassing as $i)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $i->nama }}</td>
                                        <td>{{ $i->nip }}</td>
                                        <td>{{ $i->email }}</td>
                                        <td>{{ $i->instansi }}</td>
                                        <td>@if(App\Formasi::where('instansi', $i->instansi)->first()->keterangan == 'lengkap') Lengkap @else Belum Lengkap @endif</td>
                                        <td>
                                            @if($i->status == "diterima")
                                            <span class="m-badge m-badge--success m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-success">{{ ucwords($i->status) }}</span>
                                            @elseif($i->status == "menunggu")
                                            <span class="m-badge m-badge--secondary m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-secondary">{{ ucwords($i->status) }}</span>
                                            @elseif($i->status == "review")
                                            <span class="m-badge m-badge--warning m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-warning">{{ ucwords($i->status) }}</span>
                                            @elseif($i->status == "revisi")
                                            <span class="m-badge m-badge--info m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-info">{{ ucwords($i->status) }}</span>
                                            @endif
                                        </td>
                                        <td>{{ $i->tgl_ujian }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('backend/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/demo5/base/scrollable.js') }}" type="text/javascript"></script>
@endsection
