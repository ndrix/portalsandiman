@extends('templatebackend')

@section('content')
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">Inpassing</h3>
                    </div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <br><div class="alert alert-info" role="alert">
                        <strong>Berhasil!</strong> {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <br><div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> {{ $message }}
                    </div>
                @endif
                @if ($errors->any())
                    <br><div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> Terdapat kesalahan dalam input Anda.
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
            </div>

            <!-- END: Subheader -->
            <div class="m-content">
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Lihat Pengajuan Inpassing
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="{{ url('inpassing_daftar_semua_peserta') }}" data-toggle="m-tooltip" class="btn btn-sm btn-primary">
                                        <i class="flaticon-reply"></i> Kembali
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <div class="m-portlet__body">
                        <h4>Informasi Peserta</h4>
                        <form method="post" action="{{ route('inpassing_update', ['param' => Crypt::encrypt($inpassing->email)]) }}" enctype="multipart/form-data">
                            @csrf @method('put')
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Tahun:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="tahun" class="form-control" placeholder="" value="{{ $inpassing->tahun }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Jabatan yg Diajukan:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="jabatan_yg_diajukan" class="form-control" placeholder="" value="{{ $inpassing->jabatan_yg_diajukan }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Nama:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="nama" class="form-control" placeholder="" value="{{ $inpassing->nama }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Gelar Depan:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="gelar_depan" class="form-control" placeholder="" value="{{ $inpassing->gelar_depan }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Gelar Belakang:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="gelar_belakang" class="form-control" placeholder="" value="{{ $inpassing->gelar_belakang }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">NIP:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="nip" class="form-control" placeholder="" value="{{ $inpassing->nip }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Tanggal Lahir:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="tanggal_lahir" class="form-control" placeholder="" value="{{ $inpassing->tanggal_lahir }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Pangkat/Golongan:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="pangkat_gol" class="form-control" placeholder="" value="{{ $inpassing->pangkat_gol }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">TMT Golongan:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="tmt_gol" class="form-control" placeholder="" value="{{ $inpassing->tmt_gol }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Tingkat Pendidikan:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="tingkat_pendidikan" class="form-control" placeholder="" value="{{ $inpassing->tingkat_pendidikan }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">No Telepon:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="no_telepon" class="form-control" placeholder="" value="{{ $inpassing->no_telepon }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div><br>
                        <h4>Informasi Instansi</h4>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Instansi:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="instansi" class="form-control" placeholder="" value="{{ $inpassing->instansi }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Unit Kerja Sesuai SK:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="unit_kerja" class="form-control" placeholder="" value="{{ $inpassing->unit_kerja }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Provinsi:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="provinsi" class="form-control" placeholder="" value="{{ $inpassing->provinsi }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div><br>
                        <h4>Kelengkapan Dokumen</h4>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">No Ijazah yg Diakui secara Kedinasan:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="no_ijazah_terakhir" class="form-control" placeholder="" value="{{ $inpassing->no_ijazah_terakhir }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File Ijazah:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($inpassing->file_ijazah != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_ijazah', ['param' => Crypt::encrypt($inpassing->email)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">No SK Kenaikan Pangkat Terakhir:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="no_sk_kp_terakhir" class="form-control" placeholder="" value="{{ $inpassing->no_sk_kp_terakhir }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File SK Kenaikan Pangkat Terakhir:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($inpassing->file_sk_kp_terakhir != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_sk_kp', ['param' => Crypt::encrypt($inpassing->email)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" name="file_sk_kp_terakhir" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">No Surat Pernyataan Masa Kerja:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="no_surat_masa_kerja" class="form-control" placeholder="" value="{{ $inpassing->no_surat_masa_kerja }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File Surat Pernyataan Masa Kerja:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($inpassing->file_surat_masa_kerja != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_surat_masa_kerja', ['param' => Crypt::encrypt($inpassing->email)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" name="file_surat_masa_kerja" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">No Surat Pernyataan Kompeten:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="no_surat_kompeten" class="form-control" placeholder="" value="{{ $inpassing->no_surat_kompeten }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File Surat Pernyataan Kompeten:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($inpassing->file_surat_kompeten != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_surat_kompeten', ['param' => Crypt::encrypt($inpassing->email)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" name="file_surat_kompeten" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">No Surat Usulan dari Eselon II:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="no_surat_usulan" class="form-control" placeholder="" value="{{ $inpassing->no_surat_usulan }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File Surat Usulan dari Eselon II:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($inpassing->file_surat_usulan != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_surat_usulan', ['param' => Crypt::encrypt($inpassing->email)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" name="file_surat_usulan" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div><br>
                        <h4>Entri SKP dan PPK</h4>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Nilai Realisasi SKP {{ date('Y')-2 }}:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="nilai_realisasi_skp_2_thn_sebelumnya" class="form-control" placeholder="" value="{{ $inpassing->nilai_realisasi_skp_2_thn_sebelumnya }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File Realisasi SKP {{ date('Y')-2 }}:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($inpassing->file_realisasi_skp_2_thn_sebelumnya != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_skp_2_thn_sebelumnya', ['param' => Crypt::encrypt($inpassing->email)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" name="file_realisasi_skp_2_thn_sebelumnya" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Nilai Realisasi SKP {{ date('Y')-1 }}:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="nilai_realisasi_skp_1_thn_sebelumnya" class="form-control" placeholder="" value="{{ $inpassing->nilai_realisasi_skp_2_thn_sebelumnya }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File Realisasi SKP {{ date('Y')-1 }}:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($inpassing->file_realisasi_skp_1_thn_sebelumnya != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_skp_1_thn_sebelumnya', ['param' => Crypt::encrypt($inpassing->email)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" name="file_realisasi_skp_1_thn_sebelumnya" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File Penilaian Prestasi Kerja {{ date('Y')-1 }}:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($inpassing->file_ppk_thn_sebelumnya != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_ppk_thn_sebelumnya', ['param' => Crypt::encrypt($inpassing->email)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" name="file_ppk_thn_sebelumnya" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div><br>
                        <h4>Entri Nilai Penilaian Kinerja</h4>
                        <div class="form-group m-form__group row">
                            <table width="100%" class="table table-striped table-bordered">
                                <tr>
                                    <th colspan="2">Sasaran Kinerja Pegawai (SKP) = <font id="nilai_skp_1_thn_sebelumnya">{{ $inpassing->nilai_realisasi_skp_1_thn_sebelumnya }}</font> x 60%</th>
                                    <th><font id="nilai_skp_1_thn_sebelumnya_kali_persen">{{ $inpassing->nilai_realisasi_skp_1_thn_sebelumnya*0.6 }}</font></th>
                                </tr>
                                <tr>
                                    <td width="40%">1. Orientasi Pelayanan</td>
                                    <td width="40%"><input type="text" class="form-control" placeholder="0.00" id="nilai_orientasi_pelayanan" name="nilai_orientasi_pelayanan" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" value="{{ $inpassing->nilai_orientasi_pelayanan }}" readonly></td>
                                    <td width="20%"></td>
                                </tr>
                                <tr>
                                    <td>2. Integritas</td>
                                    <td><input type="text" class="form-control" placeholder="0.00" id="nilai_integritas" name="nilai_integritas" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" value="{{ $inpassing->nilai_integritas }}" required></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>3. Komitmen</td>
                                    <td><input type="text" class="form-control" placeholder="0.00" id="nilai_komitmen" name="nilai_komitmen" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" value="{{ $inpassing->nilai_komitmen }}" required></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>4. Disiplin</td>
                                    <td><input type="text" class="form-control" placeholder="0.00" id="nilai_disiplin" name="nilai_disiplin" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" value="{{ $inpassing->nilai_disiplin }}" required></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>5. Kerjasama</td>
                                    <td><input type="text" class="form-control" placeholder="0.00" id="nilai_kerjasama" name="nilai_kerjasama" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" value="{{ $inpassing->nilai_kerjasama }}" required></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>6. Kepemimpinan</td>
                                    <td><input type="text" class="form-control" placeholder="0.00" id="nilai_kepemimpinan" name="nilai_kepemimpinan" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" value="{{ $inpassing->nilai_kepemimpinan }}" required></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Jumlah</td>
                                    <td><font id="nilai_jumlah_ppk">{{ $inpassing->nilai_jumlah_ppk }}</font></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Rata-rata</td>
                                    <td><font id="nilai_rata_rata_ppk">{{ $inpassing->nilai_rata_rata_ppk }}</font></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Nilai Perilaku Kerja = <font id="nilai_perilaku_kerja">{{ $inpassing->nilai_perilaku_kerja }}</font> x 40%</td>
                                    <td><font id="nilai_perilaku_kerja_persen">{{ $inpassing->nilai_perilaku_kerja*0.4 }}</font></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Nilai Prestasi Kerja</td>
                                    <td><font id="nilai_prestasi_kerja">{{ $inpassing->nilai_prestasi_kerja }}</font></td>
                                </tr>
                            </table>
                        </div><br>
                        <h4>Penilai</h4>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Status:</label>
                            <div class="col-xl-8 col-lg-8">
                                <select name="status" class="form-control">
                                    @php
                                        $status = [
                                            'menunggu', 'review', 'direvisi', 'diterima'
                                        ];
                                        foreach($status as $i){
                                            if($i != $inpassing->status){
                                                echo '
                                                    <option value="'.$i.'">'.ucwords($i).'</option>
                                                ';
                                            } else{
                                                echo '
                                                    <option value="'.$i.'" selected>'.ucwords($i).'</option>
                                                ';
                                            }
                                        }
                                    @endphp
                                </select>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Tanggal Ujian:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="tanggal_ujian" class="form-control" id="m_datepicker_1" readonly="" value="{{ $inpassing->tanggal_ujian }}" placeholder="">
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Hasil Ujian:</label>
                            <div class="col-xl-8 col-lg-8">
                                <select name="hasil_ujian" class="form-control">
                                    <option value=""></option>
                                    @php
                                        $hasil_ujian = [
                                            'lulus', 'tidak lulus'
                                        ];
                                        foreach($hasil_ujian as $i){
                                            if($i != $inpassing->hasil_ujian){
                                                echo '
                                                    <option value="'.$i.'">'.ucwords($i).'</option>
                                                ';
                                            } else{
                                                echo '
                                                    <option value="'.$i.'" selected>'.ucwords($i).'</option>
                                                ';
                                            }
                                        }
                                    @endphp
                                </select>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Nilai Ujian:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="nilai_ujian" class="form-control" placeholder="" value="{{ $inpassing->nilai_ujian }}">
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Komentar:</label>
                            <div class="col-xl-8 col-lg-8">
                                <textarea name="komentar" class="form-control" placeholder="" value={{ $inpassing->komentar }}></textarea>
                                <span class="m-form__help"></span>
                            </div>
                        </div><br><hr>
                        <div class="form-group m-form__group row">
                            <div class="col-xl-5 col-lg-5"></div>
                            <div class="col-xl-2 col-lg-2">
                                <button type="submit" class="btn btn-info">&emsp;&emsp;&emsp;Simpan&emsp;&emsp;&emsp;</button>
                            </div>
                            <div class="col-xl-5 col-lg-5"></div>
                        </div>
                        </form>
                    </div>

                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('backend/demo/demo5/base/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script>$.fn.datepicker.defaults.format = "dd/mm/yyyy";</script>
@endsection
