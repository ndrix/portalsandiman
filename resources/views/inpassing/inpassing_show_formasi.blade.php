@extends('templatebackend')

@section('content')
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">Inpassing</h3>
                    </div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
                </div>
            </div>

            <!-- END: Subheader -->
            <div class="m-content">
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Lihat Formasi
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="{{ url('inpassing_daftar_formasi') }}" data-toggle="m-tooltip" class="btn btn-sm btn-primary">
                                        <i class="flaticon-reply"></i> Kembali
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Tahun:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="tahun" class="form-control" placeholder="" value="{{ $formasi->tahun }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Instansi:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="instansi" class="form-control" placeholder="" value="{{ $formasi->instansi }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Provinsi:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="provinsi" class="form-control" placeholder="" value="{{ $formasi->provinsi }}" readonly>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">File Surat Formasi:</label>
                            <div class="col-xl-8 col-lg-8">
                                @if($formasi->file_surat_formasi != null)
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <a href="{{ route('download_surat_formasi', ['param' => Crypt::encrypt($formasi->operator)]) }}" class="btn btn-primary"><i class="flaticon-download"></i></a>
                                    </div>
                                    <span class="m-form__help"></span>
                                </div>
                                @else
                                <input type="text" name="file_surat_formasi" class="form-control" placeholder="" value="Belum ada file diunggah." readonly>
                                <span class="m-form__help"></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-xl-4 col-lg-4 col-form-label">Jenis Jabatan:</label>
                            <div class="col-xl-8 col-lg-8">
                                <input type="text" name="jabatan" class="form-control" readonly="" placeholder="" value="{{ $formasi->jabatan }}">
                                <span class="m-form__help"></span>
                            </div>
                        </div><br><br>
                        <div class="form-group m-form__group row">
                            <table width="100%" class="table table-striped table-bordered">
                                <tr>
                                    <th>No</th>
                                    <th>Jabatan</th>
                                    <th>Formasi (sesuai e-Formasi)</th>
                                    <th>Jumlah {{ $formasi->jabatan }} (telah diangkat)</th>
                                    <th>Jumlah CPNS dan PNS <font color="red">*)</font> dengan Formasi {{ $formasi->jabatan }}</th>
                                    <th>Jumlah Formasi yg Kosong (kol 3 - kol (4+5))</th>
                                </tr>
                                <tr>
                                    <td align="center">(1)</td>
                                    <td align="center">(2)</td>
                                    <td align="center">(3)</td>
                                    <td align="center">(4)</td>
                                    <td align="center">(5)</td>
                                    <td align="center">(6)</td>
                                </tr>
                                    @php
                                        $jml_formasi = explode(",", $formasi->jml_formasi);
                                        $jml_fungsional = explode(",", $formasi->jml_fungsional);
                                        $jml_pegawai_fungsional = explode(",", $formasi->jml_pegawai_dg_formasi_fungsional);
                                        $jml_formasi_kosong = explode(",", $formasi->jml_formasi_kosong);
                                        $jabatan_sandiman = ['Sandiman Pelaksana Pemula', 'Sandiman Pelaksana',
                                                    'Sandiman Pelaksana Lanjutan', 'Sandiman Penyelia', 'Sandiman Pertama', 'Sandiman Muda',
                                                    'Sandiman Madya', 'Sandiman Utama'];
                                    @endphp
                                    @foreach($jabatan_sandiman as $j)
                                        <tr>
                                            <td align="center">{{ $loop->iteration }}</td>
                                            <td>{{ $j }}</td>
                                            <td align="center">
                                                {{ $jml_formasi[$loop->iteration - 1] }}
                                            </td>
                                            <td align="center">
                                                {{ $jml_fungsional[$loop->iteration - 1] }}
                                            </td>
                                            <td align="center">
                                                {{ $jml_pegawai_fungsional[$loop->iteration - 1] }}
                                            </td>
                                            <td align="center">
                                                {{ $jml_formasi_kosong[$loop->iteration - 1] }}
                                            </td>
                                        </tr>
                                    @endforeach
                            </table>
                        </div>
                    </div>

                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
@endsection
