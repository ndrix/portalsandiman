@extends('templatebackend')

@section('header')
    <link href="{{ asset('backend/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">Inpassing</h3>
                    </div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
                </div>
            </div>

            <!-- END: Subheader -->
            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Daftar Pengajuan
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Email</th>
                                    <th>Instansi</th>
                                    <th>Status</th>
                                    <th>Tanggal Ujian</th>
                                    <th>Hasil Ujian</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($inpassing as $i)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $i->nama }}</td>
                                    <td>{{ $i->nip }}</td>
                                    <td>{{ $i->email }}</td>
                                    <td>{{ $i->instansi }}</td>
                                    <td>
                                        @if($i->status == "diterima")
                                        <span class="m-badge m-badge--success m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-success">{{ ucwords($i->status) }}</span>
                                        @elseif($i->status == "menunggu")
                                        <span class="m-badge m-badge--secondary m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-secondary">{{ ucwords($i->status) }}</span>
                                        @elseif($i->status == "review")
                                        <span class="m-badge m-badge--warning m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-warning">{{ ucwords($i->status) }}</span>
                                        @elseif($i->status == "revisi")
                                        <span class="m-badge m-badge--info m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-info">{{ ucwords($i->status) }}</span>
                                        @endif
                                    </td>
                                    <td>{{ $i->tanggal_ujian }}</td>
                                    <td>
                                        @if($i->hasil_ujian == "lulus")
                                            <span class="m-badge m-badge--success m-badge--wide">Lulus</span>
                                        @elseif($i->hasil_ujian == "tidak lulus")
                                            <span class="m-badge m-badge--warning m-badge--wide">Tidak Lulus</span>
                                        @endif
                                    <td><a href="{{ url('inpassing_show', ['param' => Crypt::encrypt($i->email)]) }}" class="btn btn-info btn-sm">Lihat</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('backend/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/demo5/base/scrollable.js') }}" type="text/javascript"></script>
@endsection
