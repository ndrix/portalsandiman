@extends('templatebackend')

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Inpassing</h3>
					</div>
					<div>
						<span class="m-subheader__daterange" id="">
							<span class="m-subheader__daterange-label">
								Today : {{ date('d M Y') }}
							</span>
						</span>
					</div>
                </div><br>
                @if ($message = Session::get('success'))
                    <div class="alert alert-info" role="alert">
                        <strong>Berhasil!</strong> {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> {{ $message }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> Terdapat kesalahan dalam input Anda.
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
			</div>

			<!-- END: Subheader -->
			<div class="m-content">
				<div class="m-portlet m-portlet--full-height">

					<!--begin: Portlet Head-->
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Pengajuan Inpassing
								</h3>
							</div>
						</div>
					</div>

					<!--end: Portlet Head-->

					<!--begin: Form Wizard-->
					<div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard">

						<!--begin: Message container -->
						<div class="m-portlet__padding-x">

							<!-- Here you can put a message or alert -->
						</div>

						<!--end: Message container -->

						<!--begin: Form Wizard Head -->
						<div class="m-wizard__head m-portlet__padding-x">

							<!--begin: Form Wizard Progress -->
							<div class="m-wizard__progress">
								<div class="progress">
									<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>

							<!--end: Form Wizard Progress -->

							<!--begin: Form Wizard Nav -->
							<div class="m-wizard__nav">
								<div class="m-wizard__steps">
									<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
										<a href="#" class="m-wizard__step-number">
											<span><i class="fa  flaticon-placeholder"></i></span>
										</a>
										<div class="m-wizard__step-info">
											<div class="m-wizard__step-title">
												1. Pendaftaran Peserta
											</div>
										</div>
									</div>
									<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
										<a href="#" class="m-wizard__step-number">
											<span><i class="fa  flaticon-layers"></i></span>
										</a>
										<div class="m-wizard__step-info">
											<div class="m-wizard__step-title">
												2. Unggah Dokumen
											</div>
										</div>
									</div>
									<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
										<a href="#" class="m-wizard__step-number">
											<span><i class="fa  flaticon-medal"></i></span>
										</a>
										<div class="m-wizard__step-info">
											<div class="m-wizard__step-title">
												3. Entri SKP dan PPK
											</div>
										</div>
									</div>
								</div>
							</div>

							<!--end: Form Wizard Nav -->
						</div>

						<!--end: Form Wizard Head -->

						<!--begin: Form Wizard Form-->
						<div class="m-wizard__form">

							<!--
							1) Use m-form--label-align-left class to alight the form input lables to the right
							2) Use m-form--state class to highlight input control borders on form validation
							-->
							<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" method="post" action="{{ route('inpassing_store') }}" enctype="multipart/form-data">
								@csrf
								<!--begin: Form Body -->
								<div class="m-portlet__body">

									<!--begin: Form Wizard Step 1-->
									<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
										<div class="row">
											<div class="col-xl-8 offset-xl-2">
												<div class="m-form__section m-form__section--first">
													<div class="m-form__heading">
														<h3 class="m-form__heading-title">Informasi Peserta</h3>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Tahun:</label>
														<div class="col-xl-9 col-lg-9">
															<input type="text" name="tahun" class="form-control m-input" placeholder="" value="{{ date('Y') }}" readonly>
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Jabatan yg Diajukan:</label>
														<div class="col-xl-9 col-lg-9">
															<select name="jabatan_yg_diajukan" class="form-control m-input">
																<option value="">--Pilih Jabatan Yg Diajukan--</option>
																@php
																	$jabatan_yg_diajukan = ['Sandiman Pelaksana Pemula', 'Sandiman Pelaksana',
																		'Sandiman Pelaksana Lanjutan', 'Sandiman Penyelia', 'Sandiman Pertama', 'Sandiman Muda',
																		'Sandiman Madya', 'Sandiman Utama'];
																	foreach($jabatan_yg_diajukan as $i){
																		echo '
																			<option value="'.$i.'">'.$i.'</option>
																		';
																	}
																@endphp
															</select>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Nama Lengkap:</label>
														<div class="col-xl-9 col-lg-9">
															<input type="text" name="nama" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Gelar Depan:</label>
														<div class="col-xl-9 col-lg-9">
															<input type="text" name="gelar_depan" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Gelar Belakang:</label>
														<div class="col-xl-9 col-lg-9">
															<input type="text" name="gelar_belakang" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* NIP:</label>
														<div class="col-xl-9 col-lg-9">
															<input type="text" name="nip" class="form-control m-input" placeholder="" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Tanggal Lahir:</label>
														<div class="col-xl-9 col-lg-9">
															<input type="text" name="tanggal_lahir" class="form-control" id="m_datepicker_1" readonly="" placeholder="">
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Pangkat/Golongan:</label>
														<div class="col-xl-9 col-lg-9">
															<select name="pangkat_gol" class="form-control m-input">
																<option value="">--Pilih Pangkat/Golongan--</option>
																@php
																	$pangkat_gol = [
																		'Juru Muda (I/a)', 'Juru Muda Tk I (I/b)', 'Juru (I/c)', 'Juru Tk I (I/d)', 'Pengatur Muda (II/a)',
																		'Pengatur Muda Tk I (II/b)', 'Pengatur (II/c)', 'Pengatur Tk I (II/d)', 'Penata Muda (III/a)',
																		'Penata Muda Tk I (III/b)', 'Penata (III/c)', 'Penata Tk I (III/d)', 'Pembina (IV/a)', 'Pembina Tk I (IV/b)',
																		'Pembina Utama Muda (IV/c)', 'Pembina Utama Madya (IV/d)'
																	];
																	foreach($pangkat_gol as $i){
																		echo '
																			<option value="'.$i.'">'.$i.'</option>
																		';
																	}
																@endphp
															</select>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* TMT Golongan:</label>
														<div class="col-xl-9 col-lg-9">
															<input type="text" name="tmt_gol" class="form-control" id="m_datepicker_1" readonly="" placeholder="">
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Tingkat Pendidikan:</label>
														<div class="col-xl-9 col-lg-9">
															<select name="tingkat_pendidikan" class="form-control m-input">
																<option value="">--Pilih Tingkat Pendidikan--</option>
																@php
																	$tingkat_pendidikan = [
																		'SMA', 'MA', 'SMK', 'STM', 'ST', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3'
																	];
																	foreach($tingkat_pendidikan as $i){
																		echo '
																			<option value="'.$i.'">'.$i.'</option>
																		';
																	}
																@endphp
															</select>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* No Telepon</label>
														<div class="col-xl-9 col-lg-9">
															<div class="input-group">
																<div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
																<input type="text" name="no_telepon" class="form-control m-input" placeholder="" onkeyup="this.value=this.value.replace(/[^\d]/,'')">
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
												</div>
												<div class="m-separator m-separator--dashed m-separator--lg"></div>
												<div class="m-form__section">
													<div class="m-form__heading">
														<h3 class="m-form__heading-title">
															Informasi Instansi
														</h3>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Unit Kerja sesuai SK:</label>
														<div class="col-xl-9 col-lg-9">
															<input type="text" name="unit_kerja" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Instansi:</label>
														<div class="col-xl-9 col-lg-9">
															<select name="instansi" class="form-control m-select2" id="m_select2_1">
																<option value="">--Pilih Instansi--</option>
																@php
																	$instansi = [
																		'Pemerintah Daerah', 'Badan Siber dan Sandi Negara', 'Badan Pusat Statistik', 'Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah',
																		'Kantor Staf Presiden RI', 'Kementerian Koordinator Bidang Polhukam', 'Kementerian Koordinator Bidang Kesra',
																		'Kementerian Koordinator Bidang Perekonomian', 'Kementerian Dalam Negeri', 'Kementerian Luar Negeri', 'Kementerian Pertahanan',
																		'Kementerian Hukum dan HAM', 'Kementerian Keuangan', 'Kementerian ESDM', 'Kementerian Perindustrian', 'Kementerian Perdagangan',
																		'Kementerian Pertanian', 'Kementerian Kehutanan', 'Kementerian Perhubungan', 'Kementerian Kelautan dan Perikanan', 'Kementerian Tenaga Kerja dan Transmigrasi',
																		'Kementerian Kesehatan', 'Kementerian Pekerjaan Umum', 'Kementerian Pendidikan dan Kebudayaan', 'Kementerian Sosial', 'Kementerian Agama',
																		'Kementerian Pariwisata dan Ekonomi Kreeatif', 'Kementerian Komunikasi dan Informatika', 'Kementerian Negara Riset dan Teknologi', 'Kementerian Koperasi dan Usaha Kecil dan Menengah',
																		'Kementerian Lingkungan Hidup', 'Kementerian Negara Pemberdayaan Perempuan dan Perlindungan Anak', 'Kementerian Perencanaan Pembangunan Nasional',
																		'Kementerian PAN-RB', 'Kementerian Negara Pembangunan Daerah Tertinggal', 'Kementerian BUMN', 'Kementerian Negara Perumahan Rakyat',
																		'Kementerian Pemuda dan Olahraga', 'Kementerian Sekretariat Negara', 'Arsip Nasional RI', 'Lembaga Administrasi Negara', 'Badan Kepegawaian Negara',
																		'Perpustakaan Nasional', 'Badan Intelijen Negara', 'Badan Kependudukan dan Keluarga Berencana Nasional', 'Lembaga Penerbangan Antariksa Nasional',
																		'Badan Informasi Geospasial', 'Badan Pengawasan Keuangan dan Pembangunan', 'Lembaga Ilmu Pengetahuan Indonesia', 'Badan Pengkajian dan Penerapan Teknologi',
																		'Badan Koordinasi Penanaman Modal', 'Badan Pertanahan Nasional', 'Badan Pengawasan Obat dan Makanan', 'Lembaga Ketahanan Nasional', 'Badan Meteorologi, Klimatologi, dan Geofisika',
																		'Badan Nasional Penempatan dan Perlindungan Tenaga Kerja Indonesia', 'Badan Nasional Penanggulangan Bencana', 'Badan SAR Nasional', 'Badan Narkotika Nasional', 'Badan Standarisasi Nasional',
																		'Badan Tenaga Nuklir Nasional', 'Badan Pengawas Tenaga Nuklir', 'Badan Nasional Penanggulangan Terorisme', 'Kejaksanaan Agung RI', 'Sekretariat Kabinet',
																		'Sekretariat Jendral BPK', 'Sekretariat Jendral MPR', 'Sekretariat Jendral DPR', 'Sekretariat Mahkamah Agung' , 'Sekretariat Jendral DPD-RI', 'Sekretariat Mahkamah Konstitusi',
																		'Sekretariat Komisi Yudisial', 'Sekretariat Komisi Nasional HAM', 'Sekretariat KPU', 'Sekretariat Ombudsman RI', 'Badan Koordinasi Keamanan Laut',
																		'Pusat Pelaporan dan Analisis Transaksi Keuangan', 'Bank Indonesia', 'Sekretariat Asean', 'Asian and Pasific Coconut Community',
																		'Komisi Pengawas Persaingan Usaha Republik Indonesia', 'Badan Ekonomi Kreatif', 'Badan Pengawas Pemilu', 'Komisi Aparatur Sipil Negara',
																		'Komisi Pemberantasan Korupsi', 'Lainnya'
																	];
																	foreach($instansi as $i){
																		echo '
																			<option value="'.$i.'">'.$i.'</option>
																		';
																	}
																@endphp
															</select>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<label class="col-xl-3 col-lg-3 col-form-label">* Provinsi:</label>
														<div class="col-xl-9 col-lg-9">
															<select name="provinsi" class="form-control m-input">
																<option value="">--Pilih Provinsi--</option>
																@php
																	$provinsi = [
																		'Aceh', 'Sumatera Utara', 'Sumatera Barat', 'Riau', 'Jambi', 'Sumatera Selatan',
																		'Bengkulu', 'Lampung', 'Kepulauan Bangka Belitung', 'Kepulauan Riau',
																		'DKI Jakarta', 'Jawa Barat', 'Jawa Tengah', 'DI Yogyakarta' , 'Jawa Timur',
																		'Banten', 'Bali', 'Nusa Tenggara Barat', 'Nusa Tenggara Timur',
																		'Kalimantan Barat', 'Kalimantan Tengah', 'Kalimantan Selatan',
																		'Kalimantan Timur', 'Kalimantan Utara', 'Sulawesi Utara', 'Sulawesi Tengah',
																		'Sulawesi Selatan', 'Sulawesi Tenggara', 'Gorontalo', 'Sulawesi Barat',
																		'Maluku', 'Maluku Utara', 'Papua', 'Papua Barat'
																	];
																	foreach($provinsi as $i){
																		echo '
																			<option value="'.$i.'">'.$i.'</option>
																		';
																	}
																@endphp
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--end: Form Wizard Step 1-->

									<!--begin: Form Wizard Step 2-->
									<div class="m-wizard__form-step" id="m_wizard_form_step_2">
										<div class="row">
											<div class="col-xl-8 offset-xl-2">
												<div class="m-form__section m-form__section--first">
													<div class="m-form__heading">
														<h3 class="m-form__heading-title">Unggah Dokumen</h3>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* No Ijazah yg Diakui secara Kedinasan:</label>
															<input type="text" name="no_ijazah_terakhir" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* File Ijazah (PDF):</label>
															<div class="custom-file">
																<input type="file" class="custom-file-input" name="file_ijazah_terakhir" id="customFile">
																<label class="custom-file-label" for="customFile">Choose file</label>
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* No SK Kenaikan Pangkat Terakhir:</label>
															<input type="text" name="no_sk_kp_terakhir" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* File SK Kenaikan Pangkat Terakhir (PDF):</label>
															<div class="custom-file">
																<input type="file" class="custom-file-input" name="file_sk_kp_terakhir" id="customFile">
																<label class="custom-file-label" for="customFile">Choose file</label>
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* No Surat Pernyataan Masa Kerja:</label>
															<input type="text" name="no_surat_masa_kerja" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* File Surat Pernyataan Masa Kerja (PDF):</label>
															<div class="custom-file">
																<input type="file" class="custom-file-input" name="file_surat_masa_kerja" id="customFile">
																<label class="custom-file-label" for="customFile">Choose file</label>
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* No Surat Pernyataan Kompeten:</label>
															<input type="text" name="no_surat_kompeten" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* File Surat Pernyataan Kompeten (PDF):</label>
															<div class="custom-file">
																<input type="file" class="custom-file-input" name="file_surat_kompeten" id="customFile">
																<label class="custom-file-label" for="customFile">Choose file</label>
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* No Surat Usulan Eselon II:</label>
															<input type="text" name="no_surat_usulan" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* File Surat Usulan Eselon II (PDF):</label>
															<div class="custom-file">
																<input type="file" class="custom-file-input" name="file_surat_usulan" id="customFile">
																<label class="custom-file-label" for="customFile">Choose file</label>
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--end: Form Wizard Step 2-->

									<!--begin: Form Wizard Step 3-->
									<div class="m-wizard__form-step" id="m_wizard_form_step_3">
										<div class="row">
											<div class="col-xl-8 offset-xl-2">

												<!--begin::Section-->
												<div class="m-form__section m-form__section--first">
													<div class="m-form__heading">
														<h3 class="m-form__heading-title">Entri SKP</h3>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* Nilai Realisasi SKP {{ date('Y') - 2 }}:</label>
															<input type="text" name="nilai_realisasi_skp_2_thn_sebelumnya" id="nilai_realisasi_skp_2_thn_sebelumnya" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* File Realisasi SKP {{ date('Y') - 2 }} (PDF):</label>
															<div class="custom-file">
																<input type="file" class="custom-file-input" name="file_realisasi_skp_2_thn_sebelumnya" id="customFile">
																<label class="custom-file-label" for="customFile">Choose file</label>
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* Nilai Realisasi SKP {{ date('Y') - 1 }}:</label>
															<input type="text" name="nilai_realisasi_skp_1_thn_sebelumnya" id="nilai_realisasi_skp_1_thn_sebelumnya" class="form-control m-input" placeholder="">
															<span class="m-form__help"></span>
														</div>
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* File Realisasi SKP {{ date('Y') - 1 }} (PDF):</label>
															<div class="custom-file">
																<input type="file" class="custom-file-input" name="file_realisasi_skp_1_thn_sebelumnya" id="customFile">
																<label class="custom-file-label" for="customFile">Choose file</label>
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label class="form-control-label">* File Penilaian Prestasi Kerja {{ date('Y') - 1 }}:</label>
															<div class="custom-file">
																<input type="file" class="custom-file-input" name="file_ppk_thn_sebelumnya" id="customFile">
																<label class="custom-file-label" for="customFile">Choose file</label>
															</div>
															<span class="m-form__help"></span>
														</div>
													</div>
												</div>

												<!--end::Section-->

												<!--begin::Section-->
												<div class="m-separator m-separator--dashed m-separator--lg"></div>
												<div class="m-form__section m-form__section--second">
													<div class="m-form__heading">
														<h3 class="m-form__heading-title">Entri Nilai Penilaian Kinerja</h3>
													</div>
													<div class="form-group m-form__group row">
														<table width="100%" class="table table-striped table-bordered">
															<tr>
																<th colspan="2">Sasaran Kinerja Pegawai (SKP) = <font id="nilai_skp_1_thn_sebelumnya"></font> x 60%</th>
																<th><font id="nilai_skp_1_thn_sebelumnya_kali_persen"></font></th>
															</tr>
															<tr>
																<td width="40%">1. Orientasi Pelayanan</td>
																<td width="40%"><input type="text" class="form-control" placeholder="0.00" id="nilai_orientasi_pelayanan" name="nilai_orientasi_pelayanan" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" ></td>
																<td width="20%"></td>
															</tr>
															<tr>
																<td>2. Integritas</td>
																<td><input type="text" class="form-control" placeholder="0.00" id="nilai_integritas" name="nilai_integritas" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" ></td>
																<td></td>
															</tr>
															<tr>
																<td>3. Komitmen</td>
																<td><input type="text" class="form-control" placeholder="0.00" id="nilai_komitmen" name="nilai_komitmen" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" ></td>
																<td></td>
															</tr>
															<tr>
																<td>4. Disiplin</td>
																<td><input type="text" class="form-control" placeholder="0.00" id="nilai_disiplin" name="nilai_disiplin" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" ></td>
																<td></td>
															</tr>
															<tr>
																<td>5. Kerjasama</td>
																<td><input type="text" class="form-control" placeholder="0.00" id="nilai_kerjasama" name="nilai_kerjasama" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" ></td>
																<td></td>
															</tr>
															<tr>
																<td>6. Kepemimpinan</td>
																<td><input type="text" class="form-control" placeholder="0.00" id="nilai_kepemimpinan" name="nilai_kepemimpinan" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" ></td>
																<td></td>
															</tr>
															<tr>
																<td>Jumlah</td>
																<td><font id="nilai_jumlah_ppk"></font></td>
																<td></td>
															</tr>
															<tr>
																<td>Rata-rata</td>
																<td><font id="nilai_rata_rata_ppk"></font></td>
																<td></td>
															</tr>
															<tr>
																<td colspan="2">Nilai Perilaku Kerja = <font id="nilai_perilaku_kerja"></font> x 40%</td>
																<td><font id="nilai_perilaku_kerja_persen"></font></td>
															</tr>
															<tr>
																<td colspan="2">Nilai Prestasi Kerja</td>
																<td><font id="nilai_prestasi_kerja"></font></td>
															</tr>
														</table>
													</div>
												</div>
												<!--end::Section-->
												<div class="m-separator m-separator--dashed m-separator--lg"></div>
												<div class="form-group m-form__group m-form__group--sm row">
													<div class="col-xl-12">
														<div class="m-checkbox-inline">
															<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
																<input type="checkbox" name="accept" value="1">
																Dengan ini, Saya menyetujui ketentuan yang berlaku.
																<span></span>
															</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--end: Form Wizard Step 3-->
								</div>

								<!--end: Form Body -->

								<!--begin: Form Actions -->
								<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-lg-2"></div>
											<div class="col-lg-4 m--align-left">
												<a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
													<span>
														<i class="la la-arrow-left"></i>&nbsp;&nbsp;
														<span>Back</span>
													</span>
												</a>
											</div>
											<div class="col-lg-4 m--align-right">
												<button type="submit" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit"><i class="la la-check"></i>&nbsp;&nbsp;Submit</button></button>
												<a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
													<span>
														<span>Save & Continue</span>&nbsp;&nbsp;
														<i class="la la-arrow-right"></i>
													</span>
												</a>
											</div>
											<div class="col-lg-2"></div>
										</div>
									</div>
								</div>

								<!--end: Form Actions -->
							</form>
						</div>

						<!--end: Form Wizard Form-->
					</div>

					<!--end: Form Wizard-->
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script src="{{ asset('backend/demo/demo5/base/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	<script src="{{ asset('backend/demo/demo5/base/select2.js') }}" type="text/javascript"></script>
	<script src="{{ asset('backend/demo/demo5/base/wizard.js') }}" type="text/javascript"></script>
	<script>$.fn.datepicker.defaults.format = "dd-mm-yyyy";</script>
	<script>
		var nilai_skp = document.getElementById("nilai_skp_1_thn_sebelumnya");
		var nilai_skp_kali_persen = document.getElementById("nilai_skp_1_thn_sebelumnya_kali_persen");
		var nilai_jumlah_ppk = document.getElementById("nilai_jumlah_ppk");
		var nilai_rata_rata_ppk = document.getElementById("nilai_rata_rata_ppk");
		var nilai_perilaku_kerja = document.getElementById("nilai_perilaku_kerja");
		var nilai_prestasi_kerja = document.getElementById("nilai_prestasi_kerja");
		var nilai_orientasi_pelayanan = document.getElementById("nilai_orientasi_pelayanan");
		var nilai_integritas = document.getElementById("nilai_integritas");
		var nilai_komitmen = document.getElementById("nilai_komitmen");
		var nilai_disiplin = document.getElementById("nilai_disiplin");
		var nilai_kerjasama = document.getElementById("nilai_kerjasama");
		var nilai_kepemimpinan = document.getElementById("nilai_kepemimpinan");
		$("#nilai_realisasi_skp_1_thn_sebelumnya").keyup(function() {
			nilai_skp.innerHTML = this.value;
			nilai_skp_kali_persen.innerHTML = this.value*0.6;
			nilai_prestasi_kerja.innerHTML = eval(nilai_skp_kali_persen.innerHTML) + eval(nilai_perilaku_kerja_persen.innerHTML);
		});
		$("#nilai_orientasi_pelayanan").keyup(function() {
			nilai_jumlah_ppk.innerHTML = eval(nilai_orientasi_pelayanan.value) + eval(nilai_integritas.value) + eval(nilai_disiplin.value) +
				eval(nilai_kerjasama.value) + eval(nilai_kepemimpinan.value);
			nilai_rata_rata_ppk.innerHTML = nilai_jumlah_ppk.innerHTML/6;
			nilai_perilaku_kerja.innerHTML = nilai_rata_rata_ppk.innerHTML;
			nilai_perilaku_kerja_persen.innerHTML = nilai_perilaku_kerja.innerHTML*0.4;
			nilai_prestasi_kerja.innerHTML = eval(nilai_skp_kali_persen.innerHTML) + eval(nilai_perilaku_kerja_persen.innerHTML);

		});
		$("#nilai_integritas").keyup(function() {
			nilai_jumlah_ppk.innerHTML = eval(nilai_orientasi_pelayanan.value) + eval(nilai_integritas.value) + eval(nilai_disiplin.value) +
				eval(nilai_kerjasama.value) + eval(nilai_kepemimpinan.value);
			nilai_rata_rata_ppk.innerHTML = nilai_jumlah_ppk.innerHTML/6;
			nilai_perilaku_kerja.innerHTML = nilai_rata_rata_ppk.innerHTML;
			nilai_perilaku_kerja_persen.innerHTML = nilai_perilaku_kerja.innerHTML*0.4;
			nilai_prestasi_kerja.innerHTML = eval(nilai_skp_kali_persen.innerHTML) + eval(nilai_perilaku_kerja_persen.innerHTML);
		});
		$("#nilai_komitmen").keyup(function() {
			nilai_jumlah_ppk.innerHTML = eval(nilai_orientasi_pelayanan.value) + eval(nilai_integritas.value) + eval(nilai_disiplin.value) +
				eval(nilai_kerjasama.value) + eval(nilai_kepemimpinan.value);
			nilai_rata_rata_ppk.innerHTML = nilai_jumlah_ppk.innerHTML/6;
			nilai_perilaku_kerja.innerHTML = nilai_rata_rata_ppk.innerHTML;
			nilai_perilaku_kerja_persen.innerHTML = nilai_perilaku_kerja.innerHTML*0.4;
			nilai_prestasi_kerja.innerHTML = eval(nilai_skp_kali_persen.innerHTML) + eval(nilai_perilaku_kerja_persen.innerHTML);
		});
		$("#nilai_disiplin").keyup(function() {
			nilai_jumlah_ppk.innerHTML = eval(nilai_orientasi_pelayanan.value) + eval(nilai_integritas.value) + eval(nilai_disiplin.value) +
				eval(nilai_kerjasama.value) + eval(nilai_kepemimpinan.value);
			nilai_rata_rata_ppk.innerHTML = nilai_jumlah_ppk.innerHTML/6;
			nilai_perilaku_kerja.innerHTML = nilai_rata_rata_ppk.innerHTML;
			nilai_perilaku_kerja_persen.innerHTML = nilai_perilaku_kerja.innerHTML*0.4;
			nilai_prestasi_kerja.innerHTML = eval(nilai_skp_kali_persen.innerHTML) + eval(nilai_perilaku_kerja_persen.innerHTML);
		});
		$("#nilai_kerjasama").keyup(function() {
			nilai_jumlah_ppk.innerHTML = eval(nilai_orientasi_pelayanan.value) + eval(nilai_integritas.value) + eval(nilai_disiplin.value) +
				eval(nilai_kerjasama.value) + eval(nilai_kepemimpinan.value);
			nilai_rata_rata_ppk.innerHTML = nilai_jumlah_ppk.innerHTML/6;
			nilai_perilaku_kerja.innerHTML = nilai_rata_rata_ppk.innerHTML;
			nilai_perilaku_kerja_persen.innerHTML = nilai_perilaku_kerja.innerHTML*0.4;
			nilai_prestasi_kerja.innerHTML = eval(nilai_skp_kali_persen.innerHTML) + eval(nilai_perilaku_kerja_persen.innerHTML);
		});
		$("#nilai_kepemimpinan").keyup(function() {
			nilai_jumlah_ppk.innerHTML = eval(nilai_orientasi_pelayanan.value) + eval(nilai_integritas.value) + eval(nilai_disiplin.value) +
				eval(nilai_kerjasama.value) + eval(nilai_kepemimpinan.value);
			nilai_rata_rata_ppk.innerHTML = nilai_jumlah_ppk.innerHTML/6;
			nilai_perilaku_kerja.innerHTML = nilai_rata_rata_ppk.innerHTML;
			nilai_perilaku_kerja_persen.innerHTML = nilai_perilaku_kerja.innerHTML*0.4;
			nilai_prestasi_kerja.innerHTML = eval(nilai_skp_kali_persen.innerHTML) + eval(nilai_perilaku_kerja_persen.innerHTML);
		});
	</script>
@endsection
