@extends('templatebackend')

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Inpassing</h3>
					</div>
					<div>
						<span class="m-subheader__daterange" id="">
							<span class="m-subheader__daterange-label">
								Today : {{ date('d M Y') }}
							</span>
						</span>
					</div>
				</div>
			</div>

			<!-- END: Subheader -->
			<div class="m-content">
				<div class="m-portlet m-portlet--full-height">

					<!--begin: Portlet Head-->
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Pengajuan Inpassing
								</h3>
							</div>
						</div>
					</div>

					<!--end: Portlet Head-->

					<div class="m-portlet__body">
						Anda telah mengajukan Inpassing. Silakan tunggu respon dari Operator.<br>
						Status :
						@if($status_inpassing != null)
							@if($status_inpassing == "menunggu")
								<span class="m-badge m-badge--brand m-badge--wide">Menunggu</span>
							@elseif($status_inpassing == "revisi")
								<span class="m-badge m-badge--warning m-badge--wide">Revisi</span>
							@elseif($status_inpassing == "diterima")
								<span class="m-badge m-badge--success m-badge--wide">Diterima</span>
							@endif
						@endif
						<br>
						Komentar :
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection
