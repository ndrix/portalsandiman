@extends('templatefrontend')

@section('header')

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">

	<!-- Header Standard Landing  -->

	<div class="header--standard header--standard-landing" id="header--standard">
		<div class="container">
			<div class="header--standard-wrap">

				<a href="#" class="logo">
					<div class="img-wrap">
						<img src="{{ asset('img/logo_bssn.png') }}" style="width:30px; height:30px;">
					</div>
					<div class="title-block">
						<h6 class="logo-title">Aplikasi Inpassing Jabatan</h6>
						<div class="sub-title">Badan Siber dan Sandi Negara</div>
					</div>
				</a>

				<a href="#" class="open-responsive-menu js-open-responsive-menu">
					<svg class="olymp-menu-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                </a>

				<div class="nav nav-pills nav1 header-menu">
					<div class="mCustomScrollbar">
						<ul>
                            <li>
                                <a href="{{ url('/') }}" class="nav-link">&emsp;&emsp;Home</a>
                            </li>
                            <li>
                                <a href="{{ url('berita') }}" class="nav-link">Berita</a>
                            </li>
                            <li>
                                <a href="{{ url('bantuan') }}" class="nav-link">Bantuan</a>
                            </li>
                            <li>
                                <a href="{{ url('hubungikami') }}" class="nav-link">Hubungi Kami</a>
                            </li>
                            <li>
                                <a href="{{ url('registercalonsandiman') }}" class="nav-link">Registrasi</a>
                            </li>
							<li class="close-responsive-menu js-close-responsive-menu">
								<svg class="olymp-close-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- ... end Header Standard Landing  -->

	<div class="header-spacer--standard"></div>

	<div class="stunning-header-content">
		<h1 class="stunning-header-title">Bantuan</h1>
		<ul class="breadcrumbs">
			<li class="breadcrumbs-item">
				<a href="#">Home</a>
				<span class="icon breadcrumbs-custom">/</span>
			</li>
			<li class="breadcrumbs-item active">
				<span>Bantuan</span>
			</li>
		</ul>
	</div>

	<div class="content-bg-wrap stunning-header-bg1"></div>
</div>

<!-- End Stunning header -->

@endsection

@section('content')

<section class="mb60">
	<div class="container">
		<div class="row">
			<div class="col col-xl-8 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
				<div id="accordion" role="tablist" aria-multiselectable="true" class="accordion-faqs">
					<div class="card">
						<div class="card-header" role="tab" id="headingOne">
							<h3 class="mb-0">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									Bagaimana cara membuat akun di aplikasi ini ?
									<span class="icons-wrap">
										<svg class="olymp-plus-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-plus-icon"></use></svg>
										<svg class="olymp-accordion-close-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-accordion-close-icon"></use></svg>
									</span>
								</a>
							</h3>
						</div>

						<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
							<p>
                                Melakukan pembuatan akun di aplikasi ini cukup mudah, Anda hanya perlu mengklik tombol Register / Daftar
                                lalu melengkapi formulir pendaftaran. Selanjutnya tunggu konfirmasi dari Administrator.
							</p>
						</div>

						<div class="card-header" role="tab" id="headingOne-1">
							<h3 class="mb-0">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
									Fitur apa saja yang tersedia di aplikasi ini ?
									<span class="icons-wrap">
                                        <svg class="olymp-plus-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-plus-icon"></use></svg>
                                        <svg class="olymp-accordion-close-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-accordion-close-icon"></use></svg>
                                    </span>
								</a>
							</h3>
						</div>

						<div id="collapseOne-1" class="collapse" role="tabpanel" aria-labelledby="headingOne-1">
							<p>
								Dalam portal ini, Anda dapat melakukan penilaian profesi (DUPAK) dan pengajuan INPASSING jabatan. Selain
                                itu terdapat fitur Forum untuk berkomunikasi atau bertanya dengan anggota lain.
							</p>
						</div>

						<div class="card-header" role="tab" id="headingOne-2">
							<h3 class="mb-0">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne-2" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
									Bagaimana cara menghubungi Administrator ?
									<span class="icons-wrap">
                                        <svg class="olymp-plus-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-plus-icon"></use></svg>
                                        <svg class="olymp-accordion-close-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-accordion-close-icon"></use></svg>
                                    </span>
								</a>
							</h3>
						</div>

						<div id="collapseOne-2" class="collapse" role="tabpanel" aria-labelledby="headingOne-2">
							<p>
								Anda dapat menghubungi Administrator melalui nomor telefon Badan Siber dan Sandi Negara lalu memilih ekstensi Direktorat.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
