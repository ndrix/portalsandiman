@extends('templatebackend')

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Dashboard</h3>
					</div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
				</div>
            </div>
			<div class="m-content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet ">
                            <div class="m-portlet__body  m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="col-md-12 col-lg-6 col-xl-3">
                                        <div class="m-widget24">
                                            <div class="m-widget24__item">
                                                <h4 class="m-widget24__title">
                                                    Total Inpassing
                                                </h4><br>
                                                <span class="m-widget24__desc">
                                                    Jumlah Keseluruhan
                                                </span>
                                                <span class="m-widget24__stats m--font-brand">
                                                    {{ $jml_inpassing }}
                                                </span>
                                                <div class="m--space-10"></div>
                                                <div class="progress m-progress--sm">
                                                    <div class="progress-bar m--bg-brand" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <span class="m-widget24__change">
                                                </span>
                                                <span class="m-widget24__number">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6 col-xl-3">
                                        <div class="m-widget24">
                                            <div class="m-widget24__item">
                                                <h4 class="m-widget24__title">
                                                    Jumlah Inpassing Diterima
                                                </h4><br>
                                                <span class="m-widget24__desc">
                                                    Inpassing Diterima
                                                </span>
                                                <span class="m-widget24__stats m--font-info">
                                                    {{ $jml_inpassing_diterima }}
                                                </span>
                                                <div class="m--space-10"></div>
                                                <div class="progress m-progress--sm">
                                                    <div class="progress-bar m--bg-info" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <span class="m-widget24__change">
                                                </span>
                                                <span class="m-widget24__number">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6 col-xl-3">
                                        <div class="m-widget24">
                                            <div class="m-widget24__item">
                                                <h4 class="m-widget24__title">
                                                    Jumlah Instansi
                                                </h4><br>
                                                <span class="m-widget24__desc">
                                                    Instansi Pendaftar Inpassing
                                                </span>
                                                <span class="m-widget24__stats m--font-danger">
                                                    {{ $jml_instansi }}
                                                </span>
                                                <div class="m--space-10"></div>
                                                <div class="progress m-progress--sm">
                                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <span class="m-widget24__change">
                                                </span>
                                                <span class="m-widget24__number">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6 col-xl-3">
                                        <div class="m-widget24">
                                            <div class="m-widget24__item">
                                                <h4 class="m-widget24__title">
                                                    Jumlah Pengguna
                                                </h4><br>
                                                <span class="m-widget24__desc">
                                                    Pengguna Sistem
                                                </span>
                                                <span class="m-widget24__stats m--font-success">
                                                    {{ $jml_user - 1 }}
                                                </span>
                                                <div class="m--space-10"></div>
                                                <div class="progress m-progress--sm">
                                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <span class="m-widget24__change">
                                                </span>
                                                <span class="m-widget24__number">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row">
					<div class="col-xl-12">
						<div class="m-portlet m-portlet--full-height ">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Pemberitahuan
										</h3>
									</div>
								</div>
							</div>
							<div class="m-portlet__body">
								@foreach($wall as $w)
								<div class="m-widget3">
									<div class="m-widget3__item">
										<div class="m-widget3__header">
											<div class="m-widget3__user-img">
												<img class="m-widget3__img" src="{{ asset('backend/app/media/img/users/user1.jpg') }}" alt="">
											</div>
											<div class="m-widget3__info">
												<span class="m-widget3__username">
													Administrator
												</span><br>
												<span class="m-widget3__time">
													{{ $w->created_at->diffforHumans() }}
												</span>
											</div>
										</div>
										<div class="m-widget3__body">
											<p class="m-widget3__text">
												{{ $w->message }}
											</p>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
