@extends('templatebackend')

@section('header')
    <link href="{{ asset('backend/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Formulir</h3>
					</div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
				</div>
            </div>

			<!-- END: Subheader -->
			<div class="m-content">
                @if ($message = Session::get('success'))
                    <div class="alert alert-info" role="alert">
                        <strong>Berhasil!</strong> {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> {{ $message }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> Terdapat kesalahan dalam input Anda.
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            Berkas Formulir
                                        </h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#msmodal">
                                                <i class="flaticon-plus"></i> Tambah
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>Judul</th>
                                            <th>File</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($formulir as $i)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $i->created_at->format('d-F-Y') }}</td>
                                            <td>{{ ucwords($i->nama) }}</td>
                                            <td><a href="{{ url('formulir_download', ['param' =>  $i->file]) }}"><i class="flaticon-file"></i> Download</td>
                                            <form method="post" action="{{ url('formulir_delete', ['param' => $i->id]) }}">
                                                @csrf @method('delete')
                                                <td>
                                                    <a href="{{ url('formulir_show', ['param' => $i->id]) }}" class="btn btn-info btn-sm mymodal">Ubah</a>
                                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus entri ini?');">Hapus</button>
                                                </td>
                                            </form>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
    <div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Berkas Formulir</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="myform" method="post" action="{{ url('formulir_update') }}" enctype="multipart/form-data">
                        @csrf @method('put')
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="200">
                            <input type="text" name="id" id="id" class="form-control m-input" placeholder="" value="" readonly hidden>
                            <div class="form-group m-form__group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Judul:</label>
                                <div class="col-xl-9 col-lg-9">
                                    <input type="text" name="nama" id="nama" class="form-control m-input" placeholder="" value="">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">File:</label>
                                <div class="col-xl-9 col-lg-9">
                                    <div class="input-group-append">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="customFile" name="file_formulir" id="file_formulir">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>
                                    <span class="m-form__help">Untuk mengubah file, silakan unggah file melalui kolom ini. Kosongkan jika tidak perlu.</span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" onclick="ubah()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="msmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Berkas Formulir</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="msform" method="post" action="{{ url('formulir_store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="200">
                            <input type="text" name="id" id="id" class="form-control m-input" placeholder="" value="" readonly hidden>
                            <div class="form-group m-form__group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Judul:</label>
                                <div class="col-xl-9 col-lg-9">
                                    <input type="text" name="nama" id="nama" class="form-control m-input" placeholder="" value="">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">File:</label>
                                <div class="col-xl-9 col-lg-9">
                                    <div class="input-group-append">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="customFile" name="file_formulir" id="file_formulir">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" onclick="simpan()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function ubah() {
            document.getElementById("myform").submit();
        }
        function simpan() {
            document.getElementById("msform").submit();
        }
    </script>
    <script>
        $('.mymodal').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#mymodal").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
            })
            .done(function(response) {
                $("#id").val(response.id);
                $("#nama").val(response.nama);
            });

        });
    </script>
    <script src="{{ asset('backend/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/demo5/base/scrollable.js') }}" type="text/javascript"></script>
@endsection
