<!DOCTYPE html>
<html lang="en">
<head>

	<title>Portal Sandiman</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/dist/css/bootstrap-reboot.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/dist/css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/dist/css/bootstrap-grid.css') }}">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.min.css') }}">
	<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="shortcut icon" href="{{ asset('img/logo_bssn.png') }}" />

	<!-- Main Font -->
	<script src="{{ asset('js/webfontloader.min.js') }}"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

</head>
<body class="body-bg-white">


@yield('header')

@yield('content')

<!-- Clients Block -->

{{-- <section class="crumina-module crumina-clients">
	<div class="container">
		<div class="row">
			<div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
				<a class="clients-item" href="#">
					<img src="{{ asset('img/client1.png') }}" class="" alt="logo">
				</a>
			</div>
			<div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
				<a class="clients-item" href="#">
					<img src="{{ asset('img/client2.png') }}" class="" alt="logo">
				</a>
			</div>
			<div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
				<a class="clients-item" href="#">
					<img src="{{ asset('img/client3.png') }}" class="" alt="logo">
				</a>
			</div>
			<div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
				<a class="clients-item" href="#">
					<img src="{{ asset('img/client4.png') }}" class="" alt="logo">
				</a>
			</div>
			<div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
				<a class="clients-item" href="#">
					<img src="{{ asset('img/client5.png') }}" class="" alt="logo">
				</a>
			</div>
		</div>
	</div>
</section> --}}

<!-- ... end Clients Block -->


<!-- Section Img Scale Animation -->

{{-- <section class="align-center pt80 section-move-bg-top img-scale-animation scrollme">
	<div class="container">
		<div class="row">
			<div class="col col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
				<img class="main-img" src="{{ asset('img/scale1.png') }}" alt="screen">
			</div>
		</div>

		<img class="first-img1" alt="img" src="{{ asset('img/scale2.png') }}">
		<img class="second-img1" alt="img" src="{{ asset('img/scale3.png') }}">
		<img class="third-img1" alt="img" src="{{ asset('img/scale4.png') }}">
	</div>
	<div class="content-bg-wrap bg-section2"></div>
</section> --}}

<!-- ... end Section Img Scale Animation -->


<!-- Section Call To Action Animation -->

<section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme">
	<div class="container">
		<div class="row">
			<div class="col col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
				<a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#registration-login-form-popup">Bergabung Sekarang!</a>
			</div>
		</div>
	</div>
	<img class="first-img" alt="guy" src="{{ asset('img/guy.png') }}">
	<img class="second-img" alt="rocket" src="{{ asset('img/rocket1.png') }}">
	<div class="content-bg-wrap bg-section1"></div>
</section>

<!-- ... end Section Call To Action Animation -->


{{-- <div class="modal fade" id="registration-login-form-popup" tabindex="-1" role="dialog" aria-labelledby="registration-login-form-popup" aria-hidden="true">
	<div class="modal-dialog window-popup registration-login-form-popup" role="document">
		<div class="modal-content">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
			</a>
			<div class="modal-body">
				<div class="registration-login-form">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#home1" role="tab">
								<svg class="olymp-login-icon">
									<use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-login-icon') }}"></use>
								</svg>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#profile1" role="tab">
								<svg class="olymp-register-icon">
									<use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-register-icon') }}"></use>
								</svg>
							</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home1" role="tabpanel" data-mh="log-tab">
							<div class="title h6">Register to Olympus</div>
							<form class="content">
								<div class="row">
									<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
										<div class="form-group label-floating is-empty">
											<label class="control-label">First Name</label>
											<input class="form-control" placeholder="" type="text">
										</div>
									</div>
									<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
										<div class="form-group label-floating is-empty">
											<label class="control-label">Last Name</label>
											<input class="form-control" placeholder="" type="text">
										</div>
									</div>
									<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
										<div class="form-group label-floating is-empty">
											<label class="control-label">Your Email</label>
											<input class="form-control" placeholder="" type="email">
										</div>
										<div class="form-group label-floating is-empty">
											<label class="control-label">Your Password</label>
											<input class="form-control" placeholder="" type="password">
										</div>

										<div class="form-group date-time-picker label-floating">
											<label class="control-label">Your Birthday</label>
											<input name="datetimepicker" value="10/24/1984" />
											<span class="input-group-addon">
											<svg class="olymp-calendar-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-calendar-icon') }}"></use></svg>
										</span>
										</div>

										<div class="form-group label-floating is-select">
											<label class="control-label">Your Gender</label>
											<select class="selectpicker form-control">
												<option value="MA">Male</option>
												<option value="FE">Female</option>
											</select>
										</div>

										<div class="remember">
											<div class="checkbox">
												<label>
													<input name="optionsCheckboxes" type="checkbox">
													I accept the <a href="#">Terms and Conditions</a> of the website
												</label>
											</div>
										</div>

										<a href="#" class="btn btn-purple btn-lg full-width">Complete Registration!</a>
									</div>
								</div>
							</form>
						</div>

						<div class="tab-pane" id="profile1" role="tabpanel" data-mh="log-tab">
							<div class="title h6">Login to your Account</div>
							<form class="content">
								<div class="row">
									<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
										<div class="form-group label-floating is-empty">
											<label class="control-label">Your Email</label>
											<input class="form-control" placeholder="" type="email">
										</div>
										<div class="form-group label-floating is-empty">
											<label class="control-label">Your Password</label>
											<input class="form-control" placeholder="" type="password">
										</div>

										<div class="remember">

											<div class="checkbox">
												<label>
													<input name="optionsCheckboxes" type="checkbox">
													Remember Me
												</label>
											</div>
											<a href="#" class="forgot">Forgot my Password</a>
										</div>

										<a href="#" class="btn btn-lg btn-primary full-width">Login</a>

										<div class="or"></div>

										<a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fab fa-facebook-f" aria-hidden="true"></i>Login with Facebook</a>

										<a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><i class="fab fa-twitter" aria-hidden="true"></i>Login with Twitter</a>


										<p>Don’t you have an account?
											<a href="#">Register Now!</a> it’s really simple and you can start enjoing all the benefits!
										</p>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> --}}


<!-- Footer Full Width -->

<div class="footer footer-full-width" id="footer">
	<div class="container">
		<div class="row">
			<div class="col col-lg-4 col-md-4 col-sm-6 col-6">


				<!-- Widget About -->

				<div class="widget w-about">

					<a href="02-ProfilePage.html" class="logo">
						<div class="img-wrap">
							<img src="{{ asset('img/logo-colored.png') }}" alt="Olympus">
						</div>
						<div class="title-block">
							<h6 class="logo-title">Portal Sandiman</h6>
							<div class="sub-title">Badan Siber dan Sandi Negara</div>
						</div>
					</a>
					<p>Portal profesi yang dibuat untuk memudahkan Anda.</p>
					<ul class="socials">
						<li>
							<a href="#">
								<i class="fab fa-facebook-square" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fab fa-twitter" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fab fa-youtube" aria-hidden="true"></i>
							</a>
						</li>
						{{-- <li>
							<a href="#">
								<i class="fab fa-google-plus-g" aria-hidden="true"></i>
							</a>
						</li> --}}
						<li>
							<a href="#">
								<i class="fab fa-instagram" aria-hidden="true"></i>
							</a>
						</li>
					</ul>
				</div>

				<!-- ... end Widget About -->

			</div>

			<div class="col col-lg-2 col-md-4 col-sm-6 col-6">


				<!-- Widget List -->

				<div class="widget w-list">
					<h6 class="title">Main Links</h6>
					<ul>
						<li>
							<a href="#">Landing</a>
						</li>
						<li>
							<a href="#">Home</a>
						</li>
						<li>
							<a href="#">About</a>
						</li>
						<li>
							<a href="#">Events</a>
						</li>
					</ul>
				</div>

				<!-- ... end Widget List -->

			</div>
			<div class="col col-lg-2 col-md-4 col-sm-6 col-6">


				<div class="widget w-list">
					<h6 class="title">Your Profile</h6>
					<ul>
						<li>
							<a href="#">Main Page</a>
						</li>
						<li>
							<a href="#">About</a>
						</li>
						<li>
							<a href="#">Friends</a>
						</li>
						<li>
							<a href="#">Photos</a>
						</li>
					</ul>
				</div>

			</div>
			<div class="col col-lg-2 col-md-4 col-sm-6 col-6">


				<div class="widget w-list">
					<h6 class="title">Features</h6>
					<ul>
						<li>
							<a href="#">Newsfeed</a>
						</li>
						<li>
							<a href="#">Post Versions</a>
						</li>
						<li>
							<a href="#">Messages</a>
						</li>
						<li>
							<a href="#">Friend Groups</a>
						</li>
					</ul>
				</div>

			</div>
			<div class="col col-lg-2 col-md-4 col-sm-6 col-6">


				<div class="widget w-list">
					<h6 class="title">Olympus</h6>
					<ul>
						<li>
							<a href="#">Privacy</a>
						</li>
						<li>
							<a href="#">Terms & Conditions</a>
						</li>
						<li>
							<a href="#">Forums</a>
						</li>
						<li>
							<a href="#">Statistics</a>
						</li>
					</ul>
				</div>

			</div>

			<div class="col col-lg-12 col-md-12 col-sm-12 col-12">


				<!-- SUB Footer -->

				<div class="sub-footer-copyright">
					<span>
						Copyright <a href="index.html">Olympus Buddypress + WP</a> All Rights Reserved 2017
					</span>
				</div>

				<!-- ... end SUB Footer -->

			</div>
		</div>
	</div>
</div>

<!-- ... end Footer Full Width -->




<!-- Window-popup-CHAT for responsive min-width: 768px -->

{{-- <div class="ui-block popup-chat popup-chat-responsive" tabindex="-1" role="dialog" aria-labelledby="update-header-photo" aria-hidden="true">

	<div class="modal-content">
		<div class="modal-header">
			<span class="icon-status online"></span>
			<h6 class="title" >Chat</h6>
			<div class="more">
				<svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg>
				<svg class="olymp-little-delete js-chat-open"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-little-delete') }}"></use></svg>
			</div>
		</div>
		<div class="modal-body">
			<div class="mCustomScrollbar">
				<ul class="notification-list chat-message chat-message-field">
					<li>
						<div class="author-thumb">
							<img src="{{ asset('img/avatar14-sm.jpg') }}" alt="author" class="mCS_img_loaded">
						</div>
						<div class="notification-event">
							<span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
						</div>
					</li>

					<li>
						<div class="author-thumb">
							<img src="{{ asset('img/author-page.jpg') }}" alt="author" class="mCS_img_loaded">
						</div>
						<div class="notification-event">
							<span class="chat-message-item">Don’t worry Mathilda!</span>
							<span class="chat-message-item">I already bought everything</span>
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:29pm</time></span>
						</div>
					</li>

					<li>
						<div class="author-thumb">
							<img src="{{ asset('img/avatar14-sm.jpg') }}" alt="author" class="mCS_img_loaded">
						</div>
						<div class="notification-event">
							<span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
						</div>
					</li>
				</ul>
			</div>

			<form class="need-validation">

		<div class="form-group label-floating is-empty">
			<label class="control-label">Press enter to post...</label>
			<textarea class="form-control" placeholder=""></textarea>
			<div class="add-options-message">
				<a href="#" class="options-message">
					<svg class="olymp-computer-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-computer-icon') }}"></use></svg>
				</a>
				<div class="options-message smile-block">

					<svg class="olymp-happy-sticker-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-sticker-icon') }}"></use></svg>

					<ul class="more-dropdown more-with-triangle triangle-bottom-right">
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat1.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat2.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat3.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat4.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat5.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat6.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat7.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat8.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat9.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat10.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat11.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat12.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat13.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat14.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat15.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat16.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat17.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat18.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat19.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat20.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat21.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat22.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat23.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat24.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat25.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat26.png') }}" alt="icon">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('img/icon-chat27.png') }}" alt="icon">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</form>
		</div>
	</div>

</div> --}}

<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->



<a class="back-to-top" href="#">
	<img src="{{ asset('svg-icons/back-to-top.svg') }}" alt="arrow" class="back-icon">
</a>



<!-- JS Scripts -->
<script src="{{ asset('js/jquery-3.2.1.js') }}"></script>
<script src="{{ asset('js/jquery.appear.js') }}"></script>
<script src="{{ asset('js/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('js/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
<script src="{{ asset('js/svgxuse.js') }}"></script>
<script src="{{ asset('js/imagesloaded.pkgd.js') }}"></script>
<script src="{{ asset('js/Headroom.js') }}"></script>
<script src="{{ asset('js/velocity.js') }}"></script>
<script src="{{ asset('js/ScrollMagic.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.js') }}"></script>
<script src="{{ asset('js/jquery.countTo.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/material.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>
<script src="{{ asset('js/smooth-scroll.js') }}"></script>
<script src="{{ asset('js/selectize.js') }}"></script>
<script src="{{ asset('js/swiper.jquery.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/daterangepicker.js') }}"></script>
<script src="{{ asset('js/simplecalendar.js') }}"></script>
<script src="{{ asset('js/fullcalendar.js') }}"></script>
<script src="{{ asset('js/isotope.pkgd.js') }}"></script>
<script src="{{ asset('js/ajax-pagination.js') }}"></script>
<script src="{{ asset('js/Chart.js') }}"></script>
<script src="{{ asset('js/chartjs-plugin-deferred.js') }}"></script>
<script src="{{ asset('js/circle-progress.js') }}"></script>
<script src="{{ asset('js/loader.js') }}"></script>
<script src="{{ asset('js/run-chart.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('js/jquery.gifplayer.js') }}"></script>
<script src="{{ asset('js/mediaelement-and-player.js') }}"></script>
<script src="{{ asset('js/mediaelement-playlist-plugin.min.js') }}"></script>

@yield('script')

<script src="{{ asset('js/base-init.js') }}"></script>
<script defer src="{{ asset('fonts/fontawesome-all.js') }}"></script>

<script src="{{ asset('bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

</body>
</html>
