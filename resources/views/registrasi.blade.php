@extends('templatebaru')

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Registrasi</h3>
					</div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
				</div>
            </div>
			<div class="m-content">
                @if ($message = Session::get('success'))
                    <div class="alert alert-info" role="alert">
                        <strong>Berhasil!</strong> {{ $message }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> {{ $message }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <strong>Maaf!</strong> Terdapat kesalahan dalam input Anda.
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            Form Registrasi
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <form method="post" action="{{ url('registers') }}">
                                    @csrf
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* Nama Lengkap:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <input type="text" name="nama" class="form-control m-input" placeholder="Input Nama..." required>
                                            <span class="m-form__help"></span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* NIP:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <input type="text" name="nip" class="form-control m-input" onkeyup="this.value=this.value.replace(/[^\d]/,'')" placeholder="Input NIP..." required>
                                            <span class="m-form__help"></span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* Jenis Kelamin:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <select name="jenis_kelamin" class="form-control m-input" required>
                                                <option value="">&nbsp;&nbsp;Pilih Jenis Kelamin...</option>
                                                <option value="laki_laki">&nbsp;&nbsp;Laki-Laki</option>
                                                <option value="perempuan">&nbsp;&nbsp;Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* Tempat Lahir:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <input type="text" name="tempat_lahir" class="form-control m-input" placeholder="Input Tempat Lahir..." required>
                                            <span class="m-form__help"></span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* Tanggal Lahir:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <input type="text" name="tanggal_lahir" class="form-control m-input" id="m_datepicker_1" placeholder="Input Tanggal Lahir..." required readonly>
                                            <span class="m-form__help"></span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* Alamat:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <input type="text" name="alamat" class="form-control m-input" placeholder="Input Alamat..." required>
                                            <span class="m-form__help"></span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* Email:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <input type="text" name="email" class="form-control m-input" placeholder="Input Email..." required>
                                            <span class="m-form__help"></span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* Instansi:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <select name="instansi" class="form-control m-select2" id="m_select2_1">
                                                <option value=""></option>
                                                @php
                                                    $instansi = [
                                                        'Pemerintah Daerah', 'Badan Siber dan Sandi Negara', 'Badan Pusat Statistik', 'Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah',
                                                        'Kantor Staf Presiden RI', 'Kementerian Koordinator Bidang Polhukam', 'Kementerian Koordinator Bidang Kesra',
                                                        'Kementerian Koordinator Bidang Perekonomian', 'Kementerian Dalam Negeri', 'Kementerian Luar Negeri', 'Kementerian Pertahanan',
                                                        'Kementerian Hukum dan HAM', 'Kementerian Keuangan', 'Kementerian ESDM', 'Kementerian Perindustrian', 'Kementerian Perdagangan',
                                                        'Kementerian Pertanian', 'Kementerian Kehutanan', 'Kementerian Perhubungan', 'Kementerian Kelautan dan Perikanan', 'Kementerian Tenaga Kerja dan Transmigrasi',
                                                        'Kementerian Kesehatan', 'Kementerian Pekerjaan Umum', 'Kementerian Pendidikan dan Kebudayaan', 'Kementerian Sosial', 'Kementerian Agama',
                                                        'Kementerian Pariwisata dan Ekonomi Kreeatif', 'Kementerian Komunikasi dan Informatika', 'Kementerian Negara Riset dan Teknologi', 'Kementerian Koperasi dan Usaha Kecil dan Menengah',
                                                        'Kementerian Lingkungan Hidup', 'Kementerian Negara Pemberdayaan Perempuan dan Perlindungan Anak', 'Kementerian Perencanaan Pembangunan Nasional',
                                                        'Kementerian PAN-RB', 'Kementerian Negara Pembangunan Daerah Tertinggal', 'Kementerian BUMN', 'Kementerian Negara Perumahan Rakyat',
                                                        'Kementerian Pemuda dan Olahraga', 'Kementerian Sekretariat Negara', 'Arsip Nasional RI', 'Lembaga Administrasi Negara', 'Badan Kepegawaian Negara',
                                                        'Perpustakaan Nasional', 'Badan Intelijen Negara', 'Badan Kependudukan dan Keluarga Berencana Nasional', 'Lembaga Penerbangan Antariksa Nasional',
                                                        'Badan Informasi Geospasial', 'Badan Pengawasan Keuangan dan Pembangunan', 'Lembaga Ilmu Pengetahuan Indonesia', 'Badan Pengkajian dan Penerapan Teknologi',
                                                        'Badan Koordinasi Penanaman Modal', 'Badan Pertanahan Nasional', 'Badan Pengawasan Obat dan Makanan', 'Lembaga Ketahanan Nasional', 'Badan Meteorologi, Klimatologi, dan Geofisika',
                                                        'Badan Nasional Penempatan dan Perlindungan Tenaga Kerja Indonesia', 'Badan Nasional Penanggulangan Bencana', 'Badan SAR Nasional', 'Badan Narkotika Nasional', 'Badan Standarisasi Nasional',
                                                        'Badan Tenaga Nuklir Nasional', 'Badan Pengawas Tenaga Nuklir', 'Badan Nasional Penanggulangan Terorisme', 'Kejaksanaan Agung RI', 'Sekretariat Kabinet',
                                                        'Sekretariat Jendral BPK', 'Sekretariat Jendral MPR', 'Sekretariat Jendral DPR', 'Sekretariat Mahkamah Agung' , 'Sekretariat Jendral DPD-RI', 'Sekretariat Mahkamah Konstitusi',
                                                        'Sekretariat Komisi Yudisial', 'Sekretariat Komisi Nasional HAM', 'Sekretariat KPU', 'Sekretariat Ombudsman RI', 'Badan Koordinasi Keamanan Laut',
                                                        'Pusat Pelaporan dan Analisis Transaksi Keuangan', 'Bank Indonesia', 'Sekretariat Asean', 'Asian and Pasific Coconut Community',
                                                        'Komisi Pengawas Persaingan Usaha Republik Indonesia', 'Badan Ekonomi Kreatif', 'Badan Pengawas Pemilu', 'Komisi Aparatur Sipil Negara',
                                                        'Komisi Pemberantasan Korupsi', 'Lainnya'
                                                    ];
                                                    foreach($instansi as $i){
                                                        echo '
                                                            <option value="'.$i.'">'.$i.'</option>
                                                        ';
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">* Provinsi:</label>
                                        <div class="col-xl-9 col-lg-9">
                                            <select name="provinsi" class="form-control m-select2" id="m_select2_2">
                                                <option value=""></option>
                                                @php
                                                    $provinsi = [
                                                        'Aceh', 'Sumatera Utara', 'Sumatera Barat', 'Riau', 'Jambi', 'Sumatera Selatan',
                                                        'Bengkulu', 'Lampung', 'Kepulauan Bangka Belitung', 'Kepulauan Riau',
                                                        'DKI Jakarta', 'Jawa Barat', 'Jawa Tengah', 'DI Yogyakarta' , 'Jawa Timur',
                                                        'Banten', 'Bali', 'Nusa Tenggara Barat', 'Nusa Tenggara Timur',
                                                        'Kalimantan Barat', 'Kalimantan Tengah', 'Kalimantan Selatan',
                                                        'Kalimantan Timur', 'Kalimantan Utara', 'Sulawesi Utara', 'Sulawesi Tengah',
                                                        'Sulawesi Selatan', 'Sulawesi Tenggara', 'Gorontalo', 'Sulawesi Barat',
                                                        'Maluku', 'Maluku Utara', 'Papua', 'Papua Barat'
                                                    ];
                                                    foreach($provinsi as $i){
                                                        echo '
                                                            <option value="'.$i.'">'.$i.'</option>
                                                        ';
                                                    }
                                                @endphp
                                            </select>
                                        </div>
                                    </div><br><hr>
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4 m--align-center">
                                            <button type="submit" class="btn btn-primary m-btn m-btn--custom m-btn--icon">Submit</button>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script src="{{ asset('backend/demo/demo5/base/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/demo/demo5/base/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script>$.fn.datepicker.defaults.format = "dd-mm-yyyy";</script>
@endsection
