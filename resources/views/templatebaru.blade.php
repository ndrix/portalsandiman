<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Aplikasi Inpassing Sandiman</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin::Global Theme Styles -->
		<link href="{{ asset('backend/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
		<link href="{{ asset('backend/demo/demo5/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/demo/demo5/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->

		<!--begin::Page Vendors Styles -->
		<link href="{{ asset('backend/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />

        <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

        @yield('header')

		<!--end::Page Vendors Styles -->
		<link rel="shortcut icon" href="{{ asset('img/logo_bssn.png') }}" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- begin::Header -->
			<header id="m_header" class="m-grid__item		m-header " m-minimize="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-header__top">
					<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
						<div class="m-stack m-stack--ver m-stack--desktop">

							<!-- begin::Brand -->
							<div class="m-stack__item m-brand">
								<div class="m-stack m-stack--ver m-stack--general m-stack--inline">
									<div class="m-stack__item m-stack__item--middle m-brand__logo">
										<a href="index.html" class="m-brand__logo-wrapper">
											<img src="{{ asset('img/logo_bssn.png') }}" style="width:40px;height:40px;">
										</a>
									</div>
									<div class="m-stack__item m-stack__item--middle m-brand__tools">
										<b>&emsp;Aplikasi Inpassing</b>

										<!-- begin::Responsive Header Menu Toggler-->
										<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
											<span></span>
										</a>

										<!-- end::Responsive Header Menu Toggler-->

										<!-- begin::Topbar Toggler-->
										<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
											<i class="flaticon-more"></i>
										</a>

										<!--end::Topbar Toggler-->
									</div>
								</div>
							</div>
							<!-- end::Brand -->

							<!-- begin::Topbar -->
							<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
								<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
									<div class="m-stack__item m-topbar__nav-wrapper">
										<ul class="m-topbar__nav m-nav m-nav--inline">
										</ul>
									</div>
								</div>
							</div>
							<!-- end::Topbar -->
						</div>
					</div>
				</div>
				<div class="m-header__bottom">
					<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
						<div class="m-stack m-stack--ver m-stack--desktop">
							@php
								$route_name = \Request::route()->getName();
							@endphp
							<!-- begin::Horizontal Menu -->
							<div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
								<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
									<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
										<li class="m-menu__item
											@if(strpos($route_name,"home") !== false)
											m-menu__item--active
											@endif
											" aria-haspopup="true"><a href="{{ url('/') }}" class="m-menu__link "><span class="m-menu__item-here"></span><span class="m-menu__link-text">Home</span></a>
                                        </li>
                                        <li class="m-menu__item
                                            @if(strpos($route_name,"formulir") !== false)
                                            m-menu__item--active
                                            @endif
                                            " aria-haspopup="true"><a href="{{ url('formulir') }}" class="m-menu__link "><span class="m-menu__item-here"></span><span class="m-menu__link-text">Formulir</span></a>
                                        </li>
                                        <li class="m-menu__item
                                            @if(strpos($route_name,"bantuan") !== false)
                                            m-menu__item--active
                                            @endif
                                            " aria-haspopup="true"><a href="{{ url('bantuan2') }}" class="m-menu__link "><span class="m-menu__item-here"></span><span class="m-menu__link-text">Bantuan</span></a>
                                        </li>
                                        <li class="m-menu__item
                                            @if(strpos($route_name,"registrasi") !== false)
                                            m-menu__item--active
                                            @endif
                                            " aria-haspopup="true"><a href="{{ url('registrasi') }}" class="m-menu__link "><span class="m-menu__item-here"></span><span class="m-menu__link-text">Registrasi</span></a>
										</li>
										<li class="m-menu__item
											@if(strpos($route_name,"logins") !== false)
											m-menu__item--active
											@endif
											" aria-haspopup="true"><a href="{{ url('logins') }}" class="m-menu__link "><span class="m-menu__item-here"></span><span class="m-menu__link-text">Login</span></a>
										</li>
									</ul>
								</div>
							</div>
							<!-- end::Horizontal Menu -->

						</div>
					</div>
				</div>
			</header>

			<!-- end::Header -->

			<!-- begin::Body -->

            @yield('content')

			<!-- end::Body -->

			<!-- begin::Footer -->
			<footer class="m-grid__item m-footer ">
				<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
					<div class="m-footer__wrapper">
						<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
							<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
								<span class="m-footer__copyright">
									2019 &copy; BSSN
								</span>
							</div>
						</div>
					</div>
				</div>
			</footer>

			<!-- end::Footer -->
		</div>

		<!-- end:: Page -->

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<!-- end::Scroll Top -->

		<!--begin::Global Theme Bundle -->
		<script src="{{ asset('backend/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('backend/demo/demo5/base/scripts.bundle.js') }}" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="{{ asset('backend/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="{{ asset('backend/app/js/dashboard.js') }}" type="text/javascript"></script>

        <!--end::Page Scripts -->

        @yield('script')

	</body>

	<!-- end::Body -->
</html>
