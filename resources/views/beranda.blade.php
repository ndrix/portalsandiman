@extends('templatebackend')

@section('content')
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">Dashboard</h3>
					</div>
                    <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                            <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                        </span>
                    </div>
				</div>
            </div>

			<!-- END: Subheader -->
			<div class="m-content">
				<div class="row">
					<div class="col-xl-12">

						<!--begin:: Widgets/Support Tickets -->
						<div class="m-portlet m-portlet--full-height ">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Pemberitahuan
										</h3>
									</div>
								</div>
							</div>
							<div class="m-portlet__body">
								@foreach($wall as $w)
								<div class="m-widget3">
									<div class="m-widget3__item">
										<div class="m-widget3__header">
											<div class="m-widget3__user-img">
												<img class="m-widget3__img" src="{{ asset('backend/app/media/img/users/user1.jpg') }}" alt="">
											</div>
											<div class="m-widget3__info">
												<span class="m-widget3__username">
													Administrator
												</span><br>
												<span class="m-widget3__time">
													{{ $w->created_at->diffforHumans() }}
												</span>
											</div>
										</div>
										<div class="m-widget3__body">
											<p class="m-widget3__text">
												{{ $w->message }}
											</p>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>

						<!--end:: Widgets/Support Tickets -->
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
