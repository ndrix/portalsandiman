@extends('templatefrontend')

@section('header')

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">

	<!-- Header Standard Landing  -->

	<div class="header--standard header--standard-landing" id="header--standard">
		<div class="container">
			<div class="header--standard-wrap">

				<a href="#" class="logo">
					<div class="img-wrap">
						<img src="{{ asset('img/logo_bssn.png') }}" style="width:30px; height:30px;">
					</div>
					<div class="title-block">
						<h6 class="logo-title">Aplikasi Inpassing Jabatan</h6>
						<div class="sub-title">Badan Siber dan Sandi Negara</div>
					</div>
				</a>

				<a href="#" class="open-responsive-menu js-open-responsive-menu">
					<svg class="olymp-menu-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
				</a>

				<div class="nav nav-pills nav1 header-menu">
					<div class="mCustomScrollbar">
						<ul>
                            <li>
                                <a href="{{ url('/') }}" class="nav-link">&emsp;&emsp;Home</a>
                            </li>
                            <li>
                                <a href="{{ url('berita') }}" class="nav-link">Berita</a>
                            </li>
                            <li>
                                <a href="{{ url('bantuan') }}" class="nav-link">Bantuan</a>
                            </li>
                            <li>
                                <a href="{{ url('hubungikami') }}" class="nav-link">Hubungi Kami</a>
                            </li>
                            <li>
                                <a href="{{ url('registercalonsandiman') }}" class="nav-link">Registrasi</a>
                            </li>
							<li class="close-responsive-menu js-close-responsive-menu">
								<svg class="olymp-close-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- ... end Header Standard Landing  -->

	<div class="header-spacer--standard"></div>

	<div class="stunning-header-content">
		<h1 class="stunning-header-title">Hubungi Kami</h1>
		<ul class="breadcrumbs">
			<li class="breadcrumbs-item">
				<a href="#">Home</a>
				<span class="icon breadcrumbs-custom">/</span>
			</li>
			<li class="breadcrumbs-item active">
				<span>Hubungi Kami</span>
			</li>
		</ul>
	</div>

	<div class="content-bg-wrap stunning-header-bg1"></div>
</div>

<!-- End Stunning header -->

@endsection

@section('content')

<section class="mt-0">
	<div class="section">
		<div id="map" style="height: 480px"></div>
		<script>
			var map;

			function initMap() {

				var myLatLng = {lat: -38.483, lng: 146.25};

				map = new google.maps.Map(document.getElementById('map'), {
					center: myLatLng,
					zoom: 10,

					styles: [
						{ "elementType": "geometry", "stylers": [ { "color": "#f5f5f5" } ] },
						{ "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] },
						{ "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] },
						{ "elementType": "labels.text.stroke", "stylers": [ { "color": "#f5f5f5" } ] },
						{ "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [ { "color": "#bdbdbd" } ] },
						{ "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#eeeeee" } ] },
						{ "featureType": "poi", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] },
						{ "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#e5e5e5" } ] },
						{ "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] },
						{ "featureType": "road", "elementType": "geometry", "stylers": [ { "color": "#ffffff" } ] },
						{ "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] },
						{ "featureType": "road.highway", "elementType": "geometry", "stylers": [ { "color": "#dadada" } ] },
						{ "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] },
						{ "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] },
						{ "featureType": "transit.line", "elementType": "geometry", "stylers": [ { "color": "#e5e5e5" } ] },
						{ "featureType": "transit.station", "elementType": "geometry", "stylers": [ { "color": "#eeeeee" } ] },
						{ "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#c9c9c9" } ] },
						{ "featureType": "water", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] }
					],

					scrollwheel: false//set to true to enable mouse scrolling while inside the map area
				});

				var marker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					icon: {
						url: "img/map-marker.png",
						scaledSize: new google.maps.Size(36, 54)
					}

				});

			}


		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBESxStZOWN9aMvTdR3Nov66v6TXxpRZMM&callback=initMap"
				async defer>
		</script>
	</div>
</section>

<section class="medium-padding120">
	<div class="container">
		<div class="row">
			<div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">


				<!-- Contact Item -->

				<div class="contact-item-wrap">
					<h3 class="contact-title">BSSN Ragunan</h3>
					<div class="contact-item">
						<a href="#">Jl. Harsono RM No.70 Ragunan, Jaksel</a>
					</div>
					<div class="contact-item">
						<h6 class="sub-title">Email:</h6>
						<a href="mailto:">humas@bssn.go.id</a>
					</div>
				</div>

				<!-- ... end Contact Item -->
			</div>

			<div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">


				<!-- Contact Item -->

				<div class="contact-item-wrap">
					<h3 class="contact-title">Direktorat A</h3>
					<div class="contact-item">
						<a href="#">Jl. Harsono RM No.70 Ragunan, Jaksel</a>
					</div>
					<div class="contact-item">
						<h6 class="sub-title">Email:</h6>
						<a href="#">direktorat@bssn.go.id</a>
					</div>
				</div>

				<!-- ... end Contact Item -->

			</div>
			<div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">


				<!-- Contact Item -->

				<div class="contact-item-wrap">
					<h3 class="contact-title">Direktorat B</h3>
					<div class="contact-item">
						<a href="#">Jl. Harsono RM No.70 Ragunan, Jaksel</a>
					</div>
					<div class="contact-item">
						<h6 class="sub-title">Email:</h6>
						<a href="#">direktorat@bssn.go.id</a>
					</div>
				</div>

				<!-- ... end Contact Item -->

			</div>
			<div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">


				<!-- Contact Item -->

				<div class="contact-item-wrap">
					<h3 class="contact-title">Direktorat C</h3>
					<div class="contact-item">
						<a href="#">Jl. Harsono RM No.70 Ragunan, Jaksel</a>
					</div>
					<div class="contact-item">
						<h6 class="sub-title">Email:</h6>
						<a href="#">direktorat@bssn.go.id</a></a>
					</div>
				</div>

				<!-- ... end Contact Item -->

			</div>
		</div>
	</div>
</section>

<section class="medium-padding120 bg-body contact-form-animation scrollme">
	<div class="container">
		<div class="row">
			<div class="col col-xl-10 col-lg-10 col-md-12 col-sm-12  m-auto">


				<!-- Contacts Form -->

				<div class="contact-form-wrap">
					<div class="contact-form-thumb">
						<h2 class="title">SEND US <span>A RAVEN</span></h2>
						<p>Punya pertanyaan mengenai Portal Sandiman? Masukkan disini, kami akan segera membalasnya!</p>
						<img src="img/crew.png" alt="crew" class="crew">
					</div>
					<form class="contact-form">
						<div class="row">
							<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
								<div class="form-group label-floating">
									<label class="control-label">Nama Depan</label>
									<input class="form-control" placeholder="" type="text" value="James">
								</div>
							</div>
							<div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
								<div class="form-group label-floating">
									<label class="control-label">Nama Belakang</label>
									<input class="form-control" placeholder="" type="text" value="Spiegel">
								</div>
							</div>
							<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
								<div class="form-group label-floating">
									<label class="control-label">Email</label>
									<input class="form-control" placeholder="" type="email" value="james-spiegel@yourmail.com">
								</div>

								<div class="form-group label-floating is-empty">
									<label class="control-label">Pesan Anda</label>
									<textarea class="form-control" placeholder=""></textarea>
								</div>

								<button class="btn btn-purple btn-lg full-width">Kirim Pesan</button>
							</div>
						</div>
					</form>
				</div>

				<!-- ... end Contacts Form -->

			</div>
		</div>
	</div>

	<div class="half-height-bg bg-white"></div>
</section>

@endsection
