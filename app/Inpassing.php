<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inpassing extends Model
{
    protected $table = 'inpassing';

    protected $fillable = [
        'status', 'tahun', 'jabatan_yg_diajukan', 'jabatan_yg_diajukan_comment', 'nama',
        'nama_comment', 'gelar_depan', 'gelar_depan_comment', 'gelar_belakang', 'gelar_belakang_comment',
        'nip', 'nip_comment', 'pangkat_gol', 'pangkat_gol_comment', 'tmt_gol', 'tmt_gol_comment',
        'tingkat_pendidikan', 'tingkat_pendidikan_comment', 'no_telepon', 'no_telepon_comment', 'email', 'tanggal_lahir',
        'tanggal_lahir_comment', 'unit_kerja', 'unit_kerja_comment', 'instansi', 'instansi_comment',
        'provinsi', 'provinsi_comment', 'no_ijazah_terakhir', 'no_ijazah_terakhir_comment', 'file_ijazah_terakhir',
        'file_ijazah_terakhir_comment', 'no_sk_kp_terakhir', 'no_sk_kp_terakhir_comment', 'file_sk_kp_terakhir',
        'file_sk_kp_terakhir_comment', 'no_surat_masa_kerja', 'no_surat_masa_kerja_comment', 'file_surat_masa_kerja',
        'file_surat_masa_kerja_comment', 'no_surat_kompeten', 'no_surat_kompeten_comment', 'file_surat_kompeten',
        'file_surat_kompeten_comment', 'no_surat_usulan', 'no_surat_usulan_comment', 'file_surat_usulan', 'file_surat_usulan_comment',
        'nilai_realisasi_skp_2_thn_sebelumnya', 'nilai_realisasi_skp_2_thn_sebelumnya_comment', 'file_realisasi_skp_2_thn_sebelumnya',
        'file_realisasi_skp_2_thn_sebelumnya_comment', 'nilai_realisasi_skp_1_thn_sebelumnya', 'nilai_realisasi_skp_1_thn_sebelumnya_comment',
        'file_realisasi_skp_1_thn_sebelumnya', 'file_realisasi_skp_1_thn_sebelumnya_comment',
        'nilai_orientasi_pelayanan', 'nilai_orientasi_pelayanan_comment', 'nilai_integritas', 'nilai_integritas_comment', 'nilai_komitmen',
        'nilai_komitmen_comment', 'nilai_disiplin', 'nilai_disiplin_comment', 'nilai_kerjasama', 'nilai_kerjasama_comment',
        'nilai_kepemimpinan', 'nilai_kepemimpinan_comment', 'nilai_jumlah_ppk', 'nilai_rata_rata_ppk', 'nilai_perilaku_kerja',
        'nilai_prestasi_kerja', 'file_ppk_thn_sebelumnya', 'file_ppk_thn_sebelumnya', 'file_ppk_thn_sebelumnya_comment', 'komentar',
        'tanggal_ujian', 'hasil_ujian', 'nilai_ujian'
    ];
}
