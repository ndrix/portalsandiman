<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $table = 'user_data';

    protected $fillable = [
        'jenis', 'nip', 'nama', 'gelar_depan', 'gelar_belakang', 'jenis_kelamin', 'tempat_lahir',
        'tanggal_lahir', 'alamat', 'email', 'tingkat_pendidikan', 'jurusan_pendidikan', 'no_karpeg',
        'pangkat_gol', 'tmt_gol', 'masa_kerja_thn', 'masa_kerja_bln', 'jabatan_yg_diajukan', 'jabatan_fungsional',
        'no_sk_pns_terakhir', 'file_sk_pns_terakhir', 'no_sk_awal', 'tmt_sk_awal', 'gol_jabatan_awal', 'no_sk_jabatan_terakhir', 'tmt_jabatan_terakhir',
        'angka_kredit_terakhir', 'file_sk_jabatan_terakhir', 'no_surat_tugas', 'file_surat_tugas', 'instansi', 'provinsi',
        'kabupaten', 'unit_kerja'
    ];
}
