<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\UserData;
use App\Formasi;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class RegisterController extends Controller
{

    public function create(RegisterRequest $request)
    {
        if(User::where('email', $request->email)->first() != null){
            return back()->with('error', 'Email anda telah terdaftar!');
        }
        elseif(UserData::where('nip', $request->nip)->first() != null){
            return back()->with('error', 'NIP anda telah terdaftar!');
        }
        else {
            User::create([
                'name'                              => $request->nama,
                'email'                             => $request->email,
                'password'                          => "",
                'status'                            => "pending",
            ]);
            UserData::create([
                'jenis'                             => "peserta_inpassing",
                'nip'                               => $request->nip,
                'name'                              => $request->nama,
                'jenis_kelamin'                     => $request->jenis_kelamin,
                'tempat_lahir'                      => $request->tempat_lahir,
                'tanggal_lahir'                     => $request->tanggal_lahir,
                'alamat'                            => $request->alamat,
                'email'                             => $request->email,
                'instansi'                          => $request->instansi,
                'provinsi'                          => $request->provinsi,
            ]);
            return back()->with('success', 'Pendaftaran berhasil. Silakan tunggu email aktivasi dari Administrator.');
        }
    }

    // public function create(RegisterRequest $request)
    // {
    //     if($request->jenis == "peserta inpassing"){
    //         UserData::create([
    //             'jenis'                     => $request->jenis,
    //             'nip'                       => $request->nip,
    //             'nama'                      => $request->nama,
    //             'gelar_depan'               => $request->gelar_depan,
    //             'gelar_belakang'            => $request->gelar_belakang,
    //             'jenis_kelamin'             => $request->jenis_kelamin,
    //             'tempat_lahir'              => $request->tempat_lahir,
    //             'tanggal_lahir'             => $request->tanggal_lahir,
    //             'alamat'                    => $request->alamat,
    //             'email'                     => $request->email,
    //             'tingkat_pendidikan'        => $request->tingkat_pendidikan,
    //             'jurusan_pendidikan'        => $request->jurusan_pendidikan,
    //             'no_karpeg'                 => $request->no_karpeg,
    //             'pangkat_gol'               => $request->pangkat_gol,
    //             'tmt_gol'                   => $request->tmt_gol,
    //             'masa_kerja_thn'            => $request->masa_kerja_thn,
    //             'masa_kerja_bln'            => $request->masa_kerja_bln,
    //             'jabatan_yg_diajukan'       => $request->jabatan_yg_diajukan,
    //             'no_sk_pns_terakhir'        => $request->no_sk_pns_terakhir,
    //             'file_sk_pns_terakhir'      => str_replace(' ', '', strtolower($request->nama))."_sk_pns.".$request->file_sk_pns_terakhir->getClientOriginalExtension(),
    //             'instansi'                  => $request->instansi,
    //             'provinsi'                  => $request->provinsi,
    //             'kabupaten'                 => $request->kabupaten,
    //             'unit_kerja'                => $request->unit_kerja,
    //         ]);
    //         Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nama))."_sk_pns.".$request->file_sk_pns_terakhir->getClientOriginalExtension(), File::get($request->file_sk_pns_terakhir));
    //         User::create(['name' => $request->nip, 'email' => $request->email, 'password' => '']);
    //         return back()->with('success', 'Berhasil registrasi, silakan tunggu email dari Administrator.');
    //     }
    //     elseif($request->jenis == "sandiman")
    //     {
    //         UserData::create([
    //             'jenis'                     => $request->jenis,
    //             'nip'                       => $request->nip,
    //             'nama'                      => $request->nama,
    //             'gelar_depan'               => $request->gelar_depan,
    //             'gelar_belakang'            => $request->gelar_belakang,
    //             'jenis_kelamin'             => $request->jenis_kelamin,
    //             'tempat_lahir'              => $request->tempat_lahir,
    //             'tanggal_lahir'             => $request->tanggal_lahir,
    //             'alamat'                    => $request->alamat,
    //             'email'                     => $request->email,
    //             'tingkat_pendidikan'        => $request->tingkat_pendidikan,
    //             'jurusan_pendidikan'        => $request->jurusan_pendidikan,
    //             'no_karpeg'                 => $request->no_karpeg,
    //             'pangkat_gol'               => $request->pangkat_gol,
    //             'tmt_gol'                   => $request->tmt_gol,
    //             'masa_kerja_thn'            => $request->masa_kerja_thn,
    //             'masa_kerja_bln'            => $request->masa_kerja_bln,
    //             'jabatan_fungsional'        => $request->jabatan_fungsional,
    //             'no_sk_pns_terakhir'        => $request->no_sk_pns_terakhir,
    //             'no_sk_awal'                => $request->no_sk_awal,
    //             'tmt_sk_awal'               => $request->tmt_sk_awal,
    //             'gol_jabatan_awal'          => $request->gol_jabatan_awal,
    //             'no_sk_jabatan_terakhir'    => $request->no_sk_jabatan_terakhir,
    //             'tmt_sk_jabatan_terakhir'   => $request->tmt_sk_jabatan_terakhir,
    //             'angka_kredit_terakhir'     => $request->angka_kredit_terakhir,
    //             'file_sk_jabatan_terakhir'  => str_replace(' ', '', strtolower($request->nama))."_sk_pns.".$request->file_sk_jabatan_terakhir->getClientOriginalExtension(),
    //             'instansi'                  => $request->instansi,
    //             'provinsi'                  => $request->provinsi,
    //             'kabupaten'                 => $request->kabupaten,
    //             'unit_kerja'                => $request->unit_kerja,
    //         ]);
    //         Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_sk_pns.".$request->file_sk_jabatan_terakhir->getClientOriginalExtension(), File::get($request->file_sk_jabatan_terakhir));
    //         User::create(['name' => $request->nama, 'email' => $request->email, 'password' => '']);
    //         return back()->with('success', 'Berhasil registrasi, silakan tunggu email dari Administrator.');
    //     }
    //     else
    //     {
    //         UserData::create([
    //             'jenis'                     => $request->jenis_akun,
    //             'nip'                       => $request->nip,
    //             'nama'                      => $request->nama,
    //             'gelar_depan'               => $request->gelar_depan,
    //             'gelar_belakang'            => $request->gelar_belakang,
    //             'jenis_kelamin'             => $request->jenis_kelamin,
    //             'tempat_lahir'              => $request->tempat_lahir,
    //             'tanggal_lahir'             => $request->tanggal_lahir,
    //             'alamat'                    => $request->alamat,
    //             'email'                     => $request->email,
    //             'tingkat_pendidikan'        => $request->tingkat_pendidikan,
    //             'jurusan_pendidikan'        => $request->jurusan_pendidikan,
    //             'no_karpeg'                 => $request->no_karpeg,
    //             'pangkat_gol'               => $request->pangkat_gol,
    //             'tmt_gol'                   => $request->tmt_gol,
    //             'no_surat_tugas'            => $request->no_surat_tugas,
    //             'file_surat_tugas'          => str_replace(' ', '', strtolower($request->nama))."_surat_tugas.".$request->file_surat_tugas->getClientOriginalExtension(),
    //             'instansi'                  => $request->instansi,
    //             'provinsi'                  => $request->provinsi,
    //             'kabupaten'                 => $request->kabupaten,
    //             'unit_kerja'                => $request->unit_kerja,
    //         ]);
    //         if($request->jenis_akun == 'admin instansi'){
    //             Formasi::create([
    //                 'instansi'              => $request->instansi,
    //                 'provinsi'              => $request->provinsi,
    //                 'tahun'                 => $request->tahun,
    //                 'operator'              => $request->email,
    //                 'jabatan'               => 'Sandiman',
    //                 'jml_formasi'           => '0,0,0,0,0,0,0,0',
    //                 'jml_fungsional'        => '0,0,0,0,0,0,0,0',
    //                 'jml_pegawai_dg_formasi_fungsional' => '0,0,0,0,0,0,0,0',
    //                 'jml_formasi_kosong'    => '0,0,0,0,0,0,0,0',
    //                 'file_surat_formasi'    => null,
    //                 'keterangan'            => 'belum diisi'
    //             ]);
    //         }
    //         Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_surat_tugas.".$request->file_surat_tugas->getClientOriginalExtension(), File::get($request->file_surat_tugas));
    //         User::create(['name' => $request->nama, 'email' => $request->email, 'password' => '']);
    //         User::where('email', $request->email)->first()->assignRole(ucwords($request->jenis_akun));
    //         return back()->with('success', 'Berhasil registrasi, silakan tunggu email dari Administrator.');
    //     }
    // }
}
