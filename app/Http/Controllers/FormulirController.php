<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\FormulirRequest;
use App\Formulir;

class FormulirController extends Controller
{
    public function index()
    {
        $formulir           = Formulir::all();
        return view('formulir', ['formulir' => $formulir]);
    }

    public function store(FormulirRequest $request)
    {
        Formulir::create([
            'nama'                      => $request->nama,
            'file'                      => $request->file_formulir->getClientOriginalName()
        ]);
        Storage::disk('local')->put($request->file_formulir->getClientOriginalName(), File::get($request->file_formulir));
        return back()->with('success', 'Berhasil menyimpan berkas formulir.');
    }

    public function show($param)
    {
        $formulir           = Formulir::where('id', $param)->first();
        return response()->json([
            'id'                        => $formulir->id,
            'nama'                      => $formulir->nama,
        ]);
    }

    public function update(Request $request)
    {
        $formulir           = Formulir::where('id', $request->id)->first();
        $formulir['nama']   = $request->nama;
        if($request->file_formulir != null)
        {
            Storage::disk('local')->delete($formulir->file);
            $formulir['file'] = $request->file_formulir->getClientOriginalName();
            Storage::disk('local')->put($formulir['file'], File::get($request->file_formulir));
        }
        $formulir->save();
        return back()->with('success', 'Berhasil mengubah berkas formulir.');
    }

    public function delete($param)
    {
        $formulir           = Formulir::where('id', $param)->first();
        $formulir->delete();
        return back()->with('success', 'Berhasil menghapus berkas formulir');
    }

    public function backend()
    {
        $formulir           = Formulir::all();
        return view('formulir_akun', ['formulir' => $formulir]);
    }

    public function download($param)
    {
        $file               = $param;
        return Storage::disk('local')->download($file);
    }
}
