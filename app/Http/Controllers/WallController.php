<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wall;
use App\Http\Requests\WallRequest;

class WallController extends Controller
{
    public function index()
    {
        $wall                               = Wall::orderByDesc('created_at')->get();
        return view('wall.wall_index', ['wall' => $wall]);
    }

    public function show($param)
    {
        $wall                               = Wall::where('id', $param)->first();
        return response()->json([
            'id'                => $wall->id,
            'message'           => $wall->message,
        ]);
    }

    public function update(WallRequest $request)
    {
        Wall::where('id', $request->id)->update([
            'message'           => $request->message,
            'email'             => $request->tujuan,
        ]);
        return back()->with('success', 'Berhasil mengubah pengumuman');
    }

    public function store(WallRequest $request)
    {
        Wall::create([
            'email'             => $request->tujuan,
            'message'           => $request->message,
        ]);
        return back()->with('success', 'Berhasil menambah pengumuman');
    }

    public function delete($param)
    {
        Wall::where('id', $param)->delete();
        return back()->with('success', 'Berhasil menghapus pengumuman');
    }
}
