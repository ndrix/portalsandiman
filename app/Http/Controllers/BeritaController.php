<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Http\Requests\BeritaRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BeritaController extends Controller
{
    public function index()
    {
        return view('berita');
    }

    public function backend()
    {
        $berita                 = Berita::all();
        return view('berita_akun', ['berita' => $berita]);
    }

    public function store(BeritaRequest $request)
    {
        if($request->file != null){
            Berita::create([
                'isi'                       => $request->isi,
                'file'                      => $request->file_berita->getClientOriginalName()
            ]);
        } else {
            Berita::create([
                'isi'                       => $request->isi,
                'file'                      => $request->file_berita->getClientOriginalName()
            ]);
            Storage::disk('local')->put($request->file_berita->getClientOriginalName(), File::get($request->file_berita));
        }
        return back()->with('success', 'Berhasil menyimpan berita.');
    }

    public function show($param)
    {
        $berita                 = Berita::where('id', $param)->first();
        return response()->json([
            'id'                        => $berita->id,
            'isi'                       => $berita->isi,
        ]);
    }

    public function download($param)
    {
        $file               = $param;
        return Storage::disk('local')->download($file);
    }

    public function update(BeritaRequest $request)
    {
        $berita             = Berita::where('id', $request->id)->first();
        $berita['isi']      = $request->isi;
        if($request->file_berita != null)
        {
            if($berita['file'] != null){
                Storage::disk('local')->delete($berita->file);
            }
            $berita['file'] = $request->file_berita->getClientOriginalName();
            Storage::disk('local')->put($berita['file'], File::get($request->file_berita));
        }
        $berita->save();
        return back()->with('success', 'Berhasil mengubah berita.');
    }

    public function delete($param)
    {
        $berita             = Berita::where('id', $param)->first();
        $berita->delete();
        if($berita->file != null){
            Storage::disk('local')->delete($berita->file);
        }
        return back()->with('success', 'Berhasil menghapus berita.');
    }
}
