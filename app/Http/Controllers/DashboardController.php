<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Wall;
use App\Inpassing;
use App\User;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        if(Auth::user()->hasRole('Administrator'))
        {
            $jml_inpassing                      = Inpassing::count();
            $jml_inpassing_diterima             = Inpassing::where('status', 'diterima')->count();
            $jml_instansi                       = Inpassing::groupby('instansi')->distinct()->count();
            $jml_user                           = User::count();
            $wall                               = Wall::where('email', 'all')->orderByDesc('created_at')->get();
            return view('beranda_admin', ['wall' => $wall, 'jml_inpassing' => $jml_inpassing, 'jml_inpassing_diterima' => $jml_inpassing_diterima, 'jml_instansi' => $jml_instansi, 'jml_user' => $jml_user]);
        }
        else {
            $email                              = Auth::user()->email;
            $wall                               = Wall::where('email', 'all')->orWhere('email', $email)->orderByDesc('created_at')->get();
            return view('beranda', ['wall' => $wall]);
        }
    }
}
