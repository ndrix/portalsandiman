<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use App\Formasi;
use App\UserData;
use App\Wall;
use Auth;

class FormasiController extends Controller
{
    public function index()
    {
        $user_data                          = UserData::where('email', Auth::user()->email)->first();
        $formasi                            = Formasi::where('operator', Auth::user()->email)->first();
        return view('inpassing.inpassing_formasi', ['formasi' => $formasi, 'user_data' => $user_data]);
    }

    public function list()
    {
        $formasi                            = Formasi::all();
        return view('inpassing.inpassing_daftar_formasi', ['formasi' => $formasi]);
    }

    public function show($param)
    {
        $instansi                           = Crypt::decrypt($param);
        $formasi                            = Formasi::where('instansi', $instansi)->first();
        $user_data                          = UserData::where('email', $formasi->operator)->first();
        return view('inpassing.inpassing_show_formasi', ['formasi' => $formasi, 'user_data' => $user_data]);
    }

    public function update($param, Request $request)
    {
        $email                              = Crypt::decrypt($param);
        $formasi                            = Formasi::where('operator', $email)->first();
        Formasi::where('operator', $email)->update([
            'jml_formasi'                   => implode(",", $request->jml_formasi),
            'jml_fungsional'                => implode(",", $request->jml_fungsional),
            'jml_pegawai_dg_formasi_fungsional' => implode(",", $request->jml_pegawai_fungsional),
            'jml_formasi_kosong'            => implode(",", $request->jml_formasi_kosong),
        ]);
        if($formasi->keterangan == 'belum diisi' && $formasi->file_surat_formasi == null && $request->file_surat_formasi !== null){
            Formasi::where('operator', $email)->update([
                'keterangan'                    => 'sudah diisi',
            ]);
            Wall::create([
                'email'                     => $formasi->operator,
                'message'                   => 'Terima kasih, Anda telah melengkapi formulir Formasi.'
            ]);
        }
        if($request->file_surat_formasi != null){
            Formasi::where('operator', $email)->update([
                'file_surat_formasi'        => str_replace(' ', '', UserData::where('email', $email)->first()->nip)."_surat_formasi.".$request->file_surat_formasi->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', UserData::where('email', $email)->first()->nip)."_surat_formasi.".$request->file_surat_formasi->getClientOriginalExtension(), File::get($request->file_surat_formasi));
        }
        return back()->with('success', 'Perubahan Anda telah disimpan.');
    }

    public function downloadSuratFormasi($param)
    {
        $email                              = Crypt::decrypt($param);
        $file                               = Formasi::where('operator', $email)->first()->file_surat_formasi;
        return Storage::disk('local')->download($file);
    }
}
