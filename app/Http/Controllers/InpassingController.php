<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use App\Http\Requests\InpassingRequest;
use App\Inpassing;
use App\UserData;
use App\Formasi;
use App\Wall;
use Auth;

class InpassingController extends Controller
{

    public function index()
    {
        $email                              = Auth::user()->email;
        $count                              = Inpassing::where('email', Auth::user()->email)->count();
        if($count != 0){
            $status_inpassing                   = Inpassing::where('email', $email)->first()->status;
            if($status_inpassing == "menunggu" OR $status_inpassing == "revisi"){
                return view('inpassing.inpassing_status', ['status_inpassing' => $status_inpassing]);
            }
            else {
                $inpassing                  = Inpassing::where('email', $email)->first();
                return view('inpassing.inpassing_revisi', ['inpassing' => $inpassing]);
            }
        }
        else {
            return view('inpassing.inpassing_create');
        }
    }

    public function all()
    {
        $inpassing                          = Inpassing::all();
        return view('inpassing.inpassing_daftar_semua_peserta', ['inpassing' => $inpassing]);
    }

    public function daftarPesertaBaru($param)
    {
        $filter                             = explode(",", Crypt::decrypt($param));
        $inpassing                          = Inpassing::where('instansi', $filter)->where('status', '!=', 'diterima')->get();
        $formasi                            = Formasi::where('instansi', $filter)->first();
        return view('inpassing.inpassing_daftar_peserta_baru', ['formasi' => $formasi, 'inpassing' => $inpassing]);
    }

    public function daftarPesertaUjian($param)
    {
        $filter                             = explode(",", Crypt::decrypt($param));
        $inpassing                          = Inpassing::where('instansi', $filter)->where('status', '==', 'diterima')->get();
        $formasi                            = Formasi::where('instansi', $filter)->first();
        return view('inpassing.inpassing_daftar_peserta_ujian', ['formasi' => $formasi, 'inpassing' => $inpassing]);
    }

    public function create()
    {
        $inpassing                          = Inpassing::where('email', Auth::user()->email)->first();
        if($inpassing != null){
            if($inpassing->status == 'menunggu' OR $inpassing->status == "direvisi" OR $inpassing->status == "diterima")
            {
                $inpassing                      = Inpassing::where('email', Auth::user()->email)->first();
                return view('inpassing.inpassing.status', ['inpassing' => $inpassing]);
            }
            elseif($inpassing->status == 'review')
            {
                $inpassing                      = Inpassing::where('email', Auth::user()->email)->first();
                return view('inpassing.inpassing.revisi', ['inpassing' => $inpassing]);
            }
        } else {
            return view('inpassing.inpassing_create');
        }
    }

    public function store(InpassingRequest $request)
    {
        Inpassing::create([
            'status'                        => 'menunggu',
            'tahun'                         => date('Y'),
            'jabatan_yg_diajukan'           => $request->jabatan_yg_diajukan,
            'nama'                          => $request->nama,
            'gelar_depan'                   => $request->gelar_depan,
            'gelar_belakang'                => $request->gelar_belakang,
            'nip'                           => $request->nip,
            'pangkat_gol'                   => $request->pangkat_gol,
            'tmt_gol'                       => $request->tmt_gol,
            'tingkat_pendidikan'            => $request->tingkat_pendidikan,
            'no_telepon'                    => $request->no_telepon,
            'email'                         => Auth::user()->email,
            'tanggal_lahir'                 => $request->tanggal_lahir,
            'unit_kerja'                    => $request->unit_kerja,
            'instansi'                      => $request->instansi,
            'provinsi'                      => $request->provinsi,
            'no_ijazah_terakhir'            => $request->no_ijazah_terakhir,
            'file_ijazah_terakhir'          => str_replace(' ', '', strtolower($request->nip))."_ijazah_terakhir.".$request->file_ijazah_terakhir->getClientOriginalExtension(),
            'no_sk_kp_terakhir'             => $request->no_sk_kp_terakhir,
            'file_sk_kp_terakhir'           => str_replace(' ', '', strtolower($request->nip))."_sk_kp_terakhir.".$request->file_sk_kp_terakhir->getClientOriginalExtension(),
            'no_surat_masa_kerja'           => $request->no_surat_masa_kerja,
            'file_surat_masa_kerja'         => str_replace(' ', '', strtolower($request->nip))."_surat_masa_kerja.".$request->file_surat_masa_kerja->getClientOriginalExtension(),
            'no_surat_kompeten'             => $request->no_surat_kompeten,
            'file_surat_kompeten'           => str_replace(' ', '', strtolower($request->nip))."_surat_kompeten.".$request->file_surat_kompeten->getClientOriginalExtension(),
            'no_surat_usulan'               => $request->no_surat_usulan,
            'file_surat_usulan'             => str_replace(' ', '', strtolower($request->nip))."_surat_usulan.".$request->file_surat_usulan->getClientOriginalExtension(),
            'nilai_realisasi_skp_2_thn_sebelumnya'  => $request->nilai_realisasi_skp_2_thn_sebelumnya,
            'file_realisasi_skp_2_thn_sebelumnya'  => str_replace(' ', '', strtolower($request->nip))."_file_realisasi_skp_2_thn_sebelumnya.".$request->file_realisasi_skp_2_thn_sebelumnya->getClientOriginalExtension(),
            'nilai_realisasi_skp_1_thn_sebelumnya'  => $request->nilai_realisasi_skp_1_thn_sebelumnya,
            'file_realisasi_skp_1_thn_sebelumnya'  => str_replace(' ', '', strtolower($request->nip))."_file_realisasi_skp_1_thn_sebelumnya.".$request->file_realisasi_skp_1_thn_sebelumnya->getClientOriginalExtension(),
            'nilai_orientasi_pelayanan'     => $request->nilai_orientasi_pelayanan,
            'nilai_integritas'              => $request->nilai_integritas,
            'nilai_komitmen'                => $request->nilai_komitmen,
            'nilai_disiplin'                => $request->nilai_disiplin,
            'nilai_kerjasama'               => $request->nilai_kerjasama,
            'nilai_kepemimpinan'            => $request->nilai_kepemimpinan,
            'nilai_jumlah_ppk'              => $request->nilai_orientasi_pelayanan + $request->nilai_integritas + $request->nilai_komitmen + $request->nilai_disiplin + $request->nilai_kerjasama + $request->nilai_kepemimpinan,
            'nilai_rata_rata_ppk'           => ($request->nilai_orientasi_pelayanan + $request->nilai_integritas + $request->nilai_komitmen + $request->nilai_disiplin + $request->nilai_kerjasama + $request->nilai_kepemimpinan)/6,
            'nilai_perilaku_kerja'          => (($request->nilai_orientasi_pelayanan + $request->nilai_integritas + $request->nilai_komitmen + $request->nilai_disiplin + $request->nilai_kerjasama + $request->nilai_kepemimpinan)/6)*0.4,
            'nilai_prestasi_kerja'          => ($request->nilai_realisasi_skp_1_thn_sebelumnya*0.6)+((($request->nilai_orientasi_pelayanan + $request->nilai_integritas + $request->nilai_komitmen + $request->nilai_disiplin + $request->nilai_kerjasama + $request->nilai_kepemimpinan)/6)*0.4),
            'file_ppk_thn_sebelumnya'       => str_replace(' ', '', strtolower($request->nip))."_ppk_thn_sebelumnya.".$request->file_ppk_thn_sebelumnya->getClientOriginalExtension(),
        ]);
        //simpan file
        Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_ijazah_terakhir.".$request->file_ijazah_terakhir->getClientOriginalExtension(), File::get($request->file_ijazah_terakhir));
        Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_sk_kp_terakhir.".$request->file_sk_kp_terakhir->getClientOriginalExtension(), File::get($request->file_sk_kp_terakhir));
        Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_surat_masa_kerja.".$request->file_surat_masa_kerja->getClientOriginalExtension(), File::get($request->file_surat_masa_kerja));
        Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_surat_usulan.".$request->file_surat_usulan->getClientOriginalExtension(), File::get($request->file_surat_usulan));
        Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_surat_kompeten.".$request->file_surat_kompeten->getClientOriginalExtension(), File::get($request->file_surat_kompeten));
        Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_realisasi_skp_2_thn_sebelumnya.".$request->file_realisasi_skp_2_thn_sebelumnya->getClientOriginalExtension(), File::get($request->file_realisasi_skp_2_thn_sebelumnya));
        Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_realisasi_skp_1_thn_sebelumnya.".$request->file_realisasi_skp_1_thn_sebelumnya->getClientOriginalExtension(), File::get($request->file_realisasi_skp_1_thn_sebelumnya));
        Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_ppk_thn_sebelumnya.".$request->file_ppk_thn_sebelumnya->getClientOriginalExtension(), File::get($request->file_ppk_thn_sebelumnya));
        //tulis di wall
        Wall::create([
            'email'                         => Auth::user()->email,
            'message'                       => 'Anda telah mengajukan permohonan Inpassing Jabatan. Silakan tunggu respon dari penilai.',
        ]);
        return back()->with('success', 'Berhasil menyimpan pengajuan.');
    }

    public function show($param)
    {
        $email                              = Crypt::decrypt($param);
        $inpassing                          = Inpassing::where('email', $email)->first();
        return view('inpassing.inpassing_show', ['inpassing' => $inpassing]);
    }

    public function update($param, Request $request)
    {
        $email                              = Crypt::decrypt($param);
        Inpassing::where('email', $email)->update([
            'status'                        => $request->status,
            'tahun'                         => date('Y'),
            'jabatan_yg_diajukan'           => $request->jabatan_yg_diajukan,
            'nama'                          => $request->nama,
            'gelar_depan'                   => $request->gelar_depan,
            'gelar_belakang'                => $request->gelar_belakang,
            'nip'                           => $request->nip,
            'pangkat_gol'                   => $request->pangkat_gol,
            'tmt_gol'                       => $request->tmt_gol,
            'tingkat_pendidikan'            => $request->tingkat_pendidikan,
            'no_telepon'                    => $request->no_telepon,
            'email'                         => Auth::user()->email,
            'tanggal_lahir'                 => $request->tanggal_lahir,
            'unit_kerja'                    => $request->unit_kerja,
            'instansi'                      => $request->instansi,
            'provinsi'                      => $request->provinsi,
            'no_ijazah_terakhir'            => $request->no_ijazah_terakhir,
            'no_sk_kp_terakhir'             => $request->no_sk_kp_terakhir,
            'no_surat_masa_kerja'           => $request->no_surat_masa_kerja,
            'no_surat_kompeten'             => $request->no_surat_kompeten,
            'no_surat_usulan'               => $request->no_surat_usulan,
            'nilai_realisasi_skp_2_thn_sebelumnya'  => $request->nilai_realisasi_skp_2_thn_sebelumnya,
            'nilai_realisasi_skp_1_thn_sebelumnya'  => $request->nilai_realisasi_skp_1_thn_sebelumnya,
            'nilai_orientasi_pelayanan'     => $request->nilai_orientasi_pelayanan,
            'nilai_integritas'              => $request->nilai_integritas,
            'nilai_komitmen'                => $request->nilai_komitmen,
            'nilai_disiplin'                => $request->nilai_disiplin,
            'nilai_kerjasama'               => $request->nilai_kerjasama,
            'nilai_kepemimpinan'            => $request->nilai_kepemimpinan,
            'nilai_jumlah_ppk'              => $request->nilai_orientasi_pelayanan + $request->nilai_integritas + $request->nilai_komitmen + $request->nilai_disiplin + $request->nilai_kerjasama + $request->nilai_kepemimpinan,
            'nilai_rata_rata_ppk'           => ($request->nilai_orientasi_pelayanan + $request->nilai_integritas + $request->nilai_komitmen + $request->nilai_disiplin + $request->nilai_kerjasama + $request->nilai_kepemimpinan)/6,
            'nilai_perilaku_kerja'          => (($request->nilai_orientasi_pelayanan + $request->nilai_integritas + $request->nilai_komitmen + $request->nilai_disiplin + $request->nilai_kerjasama + $request->nilai_kepemimpinan)/6)*0.4,
            'nilai_prestasi_kerja'          => ($request->nilai_realisasi_skp_1_thn_sebelumnya*0.6)+((($request->nilai_orientasi_pelayanan + $request->nilai_integritas + $request->nilai_komitmen + $request->nilai_disiplin + $request->nilai_kerjasama + $request->nilai_kepemimpinan)/6)*0.4),
            'komentar'                      => $request->komentar,
            'tanggal_ujian'                 => $request->tanggal_ujian,
            'hasil_ujian'                   => $request->hasil_ujian,
            'nilai_ujian'                   => $request->nilai_ujian,
        ]);
        if($request->file_ijazah_terakhir != null){
            Inpassing::where('email', $email)->update([
                'file_ijazah_terakhir'          => str_replace(' ', '', strtolower($request->nip))."_ijazah_terakhir.".$request->file_ijazah_terakhir->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_ijazah_terakhir.".$request->file_ijazah_terakhir->getClientOriginalExtension(), File::get($request->file_ijazah_terakhir));
        }
        if($request->file_skp_kp_terakhir != null){
            Inpassing::where('email', $email)->update([
                'file_sk_kp_terakhir'           => str_replace(' ', '', strtolower($request->nip))."_sk_kp_terakhir.".$request->file_sk_kp_terakhir->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_sk_kp_terakhir.".$request->file_sk_kp_terakhir->getClientOriginalExtension(), File::get($request->file_sk_kp_terakhir));
        }
        if($request->file_surat_masa_kerja != null){
            Inpassing::where('email', $email)->update([
                'file_surat_masa_kerja'         => str_replace(' ', '', strtolower($request->nip))."_surat_masa_kerja.".$request->file_surat_masa_kerja->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_surat_masa_kerja.".$request->file_surat_masa_kerja->getClientOriginalExtension(), File::get($request->file_surat_masa_kerja));
        }
        if($request->file_surat_usulan != null){
            Inpassing::where('email', $email)->update([
                'file_surat_usulan'             => str_replace(' ', '', strtolower($request->nip))."_surat_usulan.".$request->file_surat_usulan->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_surat_usulan.".$request->file_surat_usulan->getClientOriginalExtension(), File::get($request->file_surat_usulan));
        }
        if($request->file_surat_kompeten != null){
            Inpassing::where('email', $email)->update([
                'file_surat_kompeten'           => str_replace(' ', '', strtolower($request->nip))."_surat_kompeten.".$request->file_surat_kompeten->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_surat_kompeten.".$request->file_surat_kompeten->getClientOriginalExtension(), File::get($request->file_surat_kompeten));
        }
        if($request->file_realisasi_skp_2_thn_sebelumnya != null){
            Inpassing::where('email', $email)->update([
                'file_realisasi_skp_2_thn_sebelumnya'  => str_replace(' ', '', strtolower($request->nip))."_file_realisasi_skp_2_thn_sebelumnya.".$request->file_realisasi_skp_2_thn_sebelumnya->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_realisasi_skp_2_thn_sebelumnya.".$request->file_realisasi_skp_2_thn_sebelumnya->getClientOriginalExtension(), File::get($request->file_realisasi_skp_2_thn_sebelumnya));
        }
        if($request->file_realisasi_skp_1_thn_sebelumnya != null){
            Inpassing::where('email', $email)->update([
                'file_realisasi_skp_1_thn_sebelumnya'  => str_replace(' ', '', strtolower($request->nip))."_file_realisasi_skp_1_thn_sebelumnya.".$request->file_realisasi_skp_1_thn_sebelumnya->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_realisasi_skp_1_thn_sebelumnya.".$request->file_realisasi_skp_1_thn_sebelumnya->getClientOriginalExtension(), File::get($request->file_realisasi_skp_1_thn_sebelumnya));
        }
        if($request->file_ppk_thn_sebelumnya != null){
            Inpassing::where('email', $email)->update([
                'file_ppk_thn_sebelumnya'       => str_replace(' ', '', strtolower($request->nip))."_ppk_thn_sebelumnya.".$request->file_ppk_thn_sebelumnya->getClientOriginalExtension(),
            ]);
            Storage::disk('local')->put(str_replace(' ', '', strtolower($request->nip))."_ppk_thn_sebelumnya.".$request->file_ppk_thn_sebelumnya->getClientOriginalExtension(), File::get($request->file_ppk_thn_sebelumnya));
        }
        return back()->with('success', 'Berhasil menyimpan perubahan.');
    }

    public function checkDaftar()
    {
        $inpassing                          = Inpassing::get();
        return view('inpassing.check_daftar', ['inpassing' => $inpassing]);
    }

    public function downloadIjazah($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = Inpassing::where('email', $email)->first()->file_ijazah_terakhir;
        return Storage::disk('local')->download($file);
    }

    public function downloadSk($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = Inpassing::where('email', $email)->first()->file_sk_kp_terakhir;
        return Storage::disk('local')->download($file);
    }

    public function downloadSuratMasaKerja($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = Inpassing::where('email', $email)->first()->file_surat_masa_kerja;
        return Storage::disk('local')->download($file);
    }

    public function downloadSuratKompeten($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = Inpassing::where('email', $email)->first()->file_surat_kompeten;
        return Storage::disk('local')->download($file);
    }

    public function downloadSuratUsulan($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = Inpassing::where('email', $email)->first()->file_surat_usulan;
        return Storage::disk('local')->download($file);
    }

    public function downloadSkp2($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = Inpassing::where('email', $email)->first()->file_relisasi_skp_2_thn_sebelumnya;
        return Storage::disk('local')->download($file);
    }

    public function downloadSkp1($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = Inpassing::where('email', $email)->first()->file_realisasi_skp_1_thn_sebelumnya;
        return Storage::disk('local')->download($file);
    }

    public function downloadPpk($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = Inpassing::where('email', $email)->first()->file_ppk_thn_sebelumnya;
        return Storage::disk('local')->download($file);
    }
}
