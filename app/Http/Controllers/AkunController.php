<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\UserData;
use Mail;

class AkunController extends Controller
{
    public function index()
    {
        $user                               = User::all();
        return view('user.akun_daftar', ['user' => $user]);
    }

    public function show($param)
    {
        $user_data                          = User::where('id', $param)->first();
        return response()->json([
            'nama'                          => $user_data->name,
            'email'                         => $user_data->email,
            'status'                        => $user_data->status,
        ]);
    }

    public function update(Request $request)
    {
        $user                               = User::where('email', $request->email)->first();
        if($request->status != null){
            $user->status                   = $request->status;
        }
        if($request->password != null){
            $user->password                 = bcrypt($request->password); 
        }
        if($user->password == null){
            $password                       = str_random(15);
            $user->password                 = bcrypt($password);
            $akun                           = UserData::where('email', $request->email)->first();
            Mail::send('email', ['akun' => $akun, 'password' => $password, 'status' => 'setuju'], function ($message) use($akun, $password)
            {
                $message->subject('Aktivasi Akun');
                $message->from(env('MAIL_USERNAME'), 'Portal Sandiman');
                $message->to($request->email);
            });
        }
        $user->save();
        return back()->with('success', 'Berhasil mengubah akun');
    }

    public function downloadSkPns($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = UserData::where('email', $email)->first()->file_sk_pns_terakhir;
        return Storage::disk('local')->download($file);
    }

    public function downloadSkJabatanTerakhir($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = UserData::where('email', $email)->first()->file_sk_jabatan_terakhir;
        return Storage::disk('local')->download($file);
    }

    public function downloadSuratTugas($param)
    {
        $email                          = Crypt::decrypt($param);
        $file                           = UserData::where('email', $email)->first()->file_surat_tugas;
        return Storage::disk('local')->download($file);
    }

    public function setuju_akun($param)
    {
        $email                          = Crypt::decrypt($param);
        $auth                           = User::where('email', $email)->first();
        $password                       = str_random(15);
        $auth->password                 = bcrypt($password);
        $auth->status                   = 'active';
        $auth->save();
        $akun                           = UserData::where('email', $email)->first();
        Mail::send('email', ['akun' => $akun, 'password' => $password, 'status' => 'setuju'], function ($message) use($akun, $password)
        {
            $message->subject('Aktivasi Akun');
            $message->from(env('MAIL_USERNAME'), 'Portal Sandiman');
            $message->to('maulana.hendrikk@gmail.com');
        });
        return redirect()->route('permintaan_akun')->with('success', 'Berhasil menyetujui permintaan akun.');
    }

    public function tolak_akun($param)
    {
        $email                          = Crypt::decrypt($param);
        $auth                           = User::where('email', $email)->first();
        $akun                           = UserData::where('email', $email)->first();
        Mail::send('email', ['akun' => $akun, 'status' => 'tolak'], function ($message) use($akun)
        {
            $message->subject('Aktivasi Akun');
            $message->from(env('MAIL_USERNAME'), 'Portal Sandiman');
            $message->to('maulana.hendrikk@gmail.com');
        });
        $akun->delete();
        $auth->delete();
        return redirect()->route('permintaan_akun')->with('success', 'Berhasil menolak permintaan akun.');
    }
}
