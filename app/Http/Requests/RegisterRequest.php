<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //if(Request::get('jenis') == "peserta inpassing"){
            return [
                'nip'                   => 'required|string',
                'nama'                  => 'required|string',
                'jenis_kelamin'         => 'required|string',
                'tempat_lahir'          => 'required|string',
                'tanggal_lahir'         => 'required|date_format:d-m-Y',
                'alamat'                => 'required|string',
                'email'                 => 'required|string',
                'instansi'              => 'required|string',
                'provinsi'              => 'required|string',
            ];
        //}
        // if(Request::get('jenis') == "sandiman")
        // {
        //     return [
        //         'nip'                   => 'required|string',
        //         'nama'                  => 'required|string',
        //         'gelar_depan'           => 'required|string',
        //         'gelar_belakang'        => 'required|string',
        //         'jenis_kelamin'         => 'required|string',
        //         'tempat_lahir'          => 'required|string',
        //         'tanggal_lahir'         => 'required|date_format:d-m-Y',
        //         'alamat'                => 'required|string',
        //         'email'                 => 'required|string',
        //         'tingkat_pendidikan'    => 'required|string',
        //         'jurusan_pendidikan'    => 'required|string',
        //         'no_karpeg'             => 'string',
        //         'pangkat_gol'           => 'required|string',
        //         'tmt_gol'               => 'required|date_format:d-m-Y',
        //         'masa_kerja_thn'        => 'required|string',
        //         'masa_kerja_bln'        => 'required|string',
        //         'jabatan_fungsional'    => 'required|string',
        //         'no_sk_pns_terakhir'    => 'required|string',
        //         'no_sk_awal'            => 'required|string',
        //         'gol_jabatan_awal'      => 'required|string',
        //         'no_sk_jabatan_terakhir'=> 'required|string',
        //         'tmt_sk_jabatan_terakhir'=> 'required|date_format:d-m-Y',
        //         'angka_kredit_terakhir' => 'required|string',
        //         'file_sk_jabatan_terakhir'=> 'required|mimes:pdf,png,jpg|max:2048',
        //         'instansi'              => 'required|string',
        //         'provinsi'              => 'required|string',
        //         'kabupaten'             => 'required|string',
        //         'unit_kerja'            => 'required|string',
        //     ];
        // }
        // if(Request::get('jenis') == "operator")
        // {
        //     return [
        //         'nip'                   => 'required|string',
        //         'nama'                  => 'required|string',
        //         'gelar_depan'           => 'required|string',
        //         'gelar_belakang'        => 'required|string',
        //         'jenis_kelamin'         => 'required|string',
        //         'tempat_lahir'          => 'required|string',
        //         'tanggal_lahir'         => 'required|date_format:d-m-Y',
        //         'alamat'                => 'required|string',
        //         'email'                 => 'required|string',
        //         'tingkat_pendidikan'    => 'required|string',
        //         'jurusan_pendidikan'    => 'required|string',
        //         'no_karpeg'             => 'string',
        //         'pangkat_gol'           => 'required',
        //         'tmt_gol'               => 'required|date_format:d-m-Y',
        //         'no_surat_tugas'        => 'required|string',
        //         'file_surat_tugas'      => 'required|mimes:pdf,png,jpg|max:2048',
        //         'instansi'              => 'required|string',
        //         'provinsi'              => 'required|string',
        //         'kabupaten'             => 'required|string',
        //         'unit_kerja'            => 'required|string',
        //     ];
        // }
    }

    public function messages()
    {
        return [
            'required'      => 'Input :attribute tidak sesuai.',
            'date_format'   => 'Format waktu salah.',
            'after'         => 'Input :attribute tidak sesuai.',
            'string'        => 'Input :attribute tidak sesuai.',
            'integer'       => 'Input :attribute tidak sesuai',
            'mimes'         => 'Format file tidak sesuai.',
            'max'           => 'File melebihi batas maksimal (2 MB).',
        ];
    }
}
