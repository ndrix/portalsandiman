<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InpassingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jabatan_yg_diajukan'           => 'required|string',
            'nama'                          => 'required|string',
            'nip'                           => 'required|string',
            'pangkat_gol'                   => 'required|string',
            'tmt_gol'                       => 'required|string',
            'tingkat_pendidikan'            => 'required|string',
            'no_telepon'                    => 'required|string',
            'tanggal_lahir'                 => 'required|string',
            'unit_kerja'                    => 'required|string',
            'instansi'                      => 'required|string',
            'provinsi'                      => 'required|string',
            'no_ijazah_terakhir'            => 'required|string',
            'file_ijazah_terakhir'          => 'required|mimes:pdf,png,jpg|max:2048',
            'no_sk_kp_terakhir'             => 'required|string',
            'file_sk_kp_terakhir'           => 'required|mimes:pdf,png,jpg|max:2048',
            'no_surat_masa_kerja'           => 'required|string',
            'file_surat_masa_kerja'         => 'required|mimes:pdf,png,jpg|max:2048',
            'no_surat_kompeten'             => 'required|string',
            'file_surat_kompeten'           => 'required|mimes:pdf,png,jpg|max:2048',
            'no_surat_usulan'               => 'required|string',
            'file_surat_usulan'             => 'required|mimes:pdf,png,jpg|max:2048',
            'nilai_realisasi_skp_2_thn_sebelumnya' => 'required|string',
            'file_realisasi_skp_2_thn_sebelumnya' => 'required|mimes:pdf,png,jpg|max:2048',
            'nilai_realisasi_skp_1_thn_sebelumnya' => 'required|string',
            'file_realisasi_skp_2_thn_sebelumnya' => 'required|mimes:pdf,png,jpg|max:2048',
            'nilai_orientasi_pelayanan'     => 'required|string',
            'nilai_integritas'              => 'required|string',
            'nilai_komitmen'                => 'required|string',
            'nilai_disiplin'                => 'required|string',
            'nilai_kerjasama'               => 'required|string',
            'nilai_kepemimpinan'            => 'required|string',
            'file_ppk_thn_sebelumnya'       => 'required|mimes:pdf,png,jpg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'required'      => 'Input :attribute tidak sesuai.',
            'date_format'   => 'Format waktu salah.',
            'after'         => 'Input :attribute tidak sesuai.',
            'string'        => 'Input :attribute tidak sesuai.',
            'integer'       => 'Input :attribute tidak sesuai',
            'mimes'         => 'Format file tidak sesuai.',
            'max'           => 'File melebihi batas maksimal (2 MB).',
        ];
    }
}
