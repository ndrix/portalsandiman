<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WallRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message'           => 'required',
            'tujuan'            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required'      => 'Input :attribute tidak sesuai.',
            'date_format'   => 'Format waktu salah.',
            'after'         => 'Input :attribute tidak sesuai.',
            'string'        => 'Input :attribute tidak sesuai.',
            'integer'       => 'Input :attribute tidak sesuai',
            'mimes'         => 'Format file tidak sesuai.',
            'max'           => 'File melebihi batas maksimal (5 MB).',
        ];
    }
}
