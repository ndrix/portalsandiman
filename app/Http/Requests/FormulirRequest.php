<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormulirRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'                  => 'required',
            'file_formulir'         => 'required|mimes:pdf,jpg,png|max:5046',
        ];
    }

    public function messages()
    {
        return [
            'required'      => 'Input :attribute tidak sesuai.',
            'date_format'   => 'Format waktu salah.',
            'after'         => 'Input :attribute tidak sesuai.',
            'string'        => 'Input :attribute tidak sesuai.',
            'integer'       => 'Input :attribute tidak sesuai',
            'mimes'         => 'Format file tidak sesuai.',
            'max'           => 'File melebihi batas maksimal (5 MB).',
        ];
    }
}
