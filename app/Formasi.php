<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formasi extends Model
{
    protected $table = "formasi";

    protected $fillable = [
        'instansi', 'operator', 'jabatan', 'formasi', 'jml_fungsional',
        'jml_pegawai_dg_formasi_fungsional', 'jml_formasi_kosong', 'file_surat_formasi', 'keterangan'
    ];
}
