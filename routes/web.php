<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// Formulir
Route::get('formulir', 'FormulirController@index')->name('formulir_index');
Route::get('formulir_show/{param}', 'FormulirController@show')->name('formulir_show');
Route::get('formulir_download/{param}', 'FormulirController@download')->name('formulir_download');
Route::post('formulir_store', 'FormulirController@store')->name('formulir_store');
Route::put('formulir_update', 'FormulirController@update')->name('formulir_update');
Route::delete('formulir_delete/{param}', 'FormulirController@delete')->name('formulir_delete');
Route::get('formulir_akun', 'FormulirController@backend')->name('formulir_back');

// Berita
Route::get('berita', 'BeritaController@index')->name('berita_index');
Route::get('berita_akun', 'BeritaController@backend')->name('berita_back');
Route::get('berita_show/{param}', 'BeritaController@show')->name('berita_show');
Route::get('berita_download/{param}', 'BeritaController@download')->name('berita_download');
Route::post('berita_store', 'BeritaController@store')->name('berita_store');
Route::put('berita_update', 'BeritaController@update')->name('berita_update');
Route::delete('berita_delete/{param}', 'BeritaController@delete')->name('berita_delete');

Route::get('bantuan2', function () {
    return view('bantuan2');
})->name('bantuan2');
Route::get('registrasi', function () {
    return view('registrasi');
})->name('registrasi');
Route::get('bantuan', function () {
    return view('bantuan');
});
Route::get('hubungikami', function () {
    return view('hubungikami');
});
Route::get('beranda', 'DashboardController@index')->name('beranda');
Route::get('registercalonsandiman', function () {
    return view('registercalonsandiman');
});
Route::get('registersandiman', function () {
    return view('registersandiman');
});
Route::get('registeroperator', function () {
    return view('registeroperator');
});
Route::get('download_sk_pns_terakhir/{param}', 'AkunController@downloadSkPns')->name('download_sk_pns_terakhir');
Route::get('download_sk_jabatan_terakhir/{param}', 'AkunController@downloadSkJabatanTerakhir')->name('download_sk_jabatan_terakhir');
Route::get('download_surat_tugas/{param}', 'AkunController@downloadSuratTugas')->name('download_surat_tugas');
Route::get('akun_daftar', 'AkunController@index')->name('akun_daftar');
Route::get('akun_show/{param}', 'AkunController@show')->name('akun_show');
Route::put('akun_update', 'AkunController@update')->name('update_akun');
Route::get('create_inpassing', 'InpassingController@create')->name('inpassing.create');
Route::post('store_inpassing', 'InpassingController@store')->name('inpassing_store');
Route::put('update_inpassing/{param}', 'InpassingController@update')->name('inpassing_update');
Route::get('check_inpassing/{param}', 'InpassingController@check')->name('inpassing_check');
Route::get('download_ijazah/{param}', 'InpassingController@downloadIjazah')->name('download_ijazah');
Route::get('download_sk_kp/{param}', 'InpassingController@downloadSk')->name('download_sk_kp');
Route::get('download_surat_masa_kerja/{param}', 'InpassingController@downloadSuratMasaKerja')->name('download_surat_masa_kerja');
Route::get('download_surat_kompeten/{param}', 'InpassingController@downloadSuratKompeten')->name('download_surat_kompeten');
Route::get('download_surat_usulan/{param}', 'InpassingController@downloadSuratUsulan')->name('download_surat_usulan');
Route::get('download_skp_2/{param}', 'InpassingController@downloadSkp2')->name('download_skp_2_thn_sebelumnya');
Route::get('download_skp_1/{param}', 'InpassingController@downloadSkp1')->name('download_skp_1_thn_sebelumnya');
Route::get('download_ppk/{param}', 'InpassingController@downloadPpk')->name('download_ppk_thn_sebelumnya');
Route::put('inpassing_update_formasi/{param}', 'FormasiController@update')->name('inpassing_update_formasi');
Route::get('download_surat_formasi/{param}', 'FormasiController@downloadSuratFormasi')->name('download_surat_formasi');
Route::get('inpassing', 'InpassingController@index')->name('inpassing');
Route::get('inpassing_show/{param}', 'InpassingController@show')->name('inpassing_show');
Route::get('inpassing_formasi', 'FormasiController@index')->name('inpassing_formasi');
Route::get('inpassing_daftar_formasi', 'FormasiController@list')->name('inpassing_daftar_formasi');
Route::get('inpassing_show_formasi/{param}', 'FormasiController@show')->name('inpassing_show_formasi');
Route::get('inpassing_daftar_semua_peserta', 'InpassingController@all')->name('inpassing_daftar_semua_peserta');
Route::get('inpassing_daftar_peserta_baru/{param}', 'InpassingController@daftarPesertaBaru')->name('inpassing_daftar_peserta_baru');
Route::get('inpassing_daftar_peserta_ujian/{param}', 'InpassingController@daftarPesertaUjian')->name('inpassing_daftar_peserta_ujian');
Route::get('inpassing_check_daftar', 'InpassingController@checkDaftar')->name('inpassing_check_daftar');
Route::get('wall', 'WallController@index')->name('wall');
Route::get('wall_show/{param}', 'WallController@show')->name('wall_show');
Route::put('wall_update', 'WallController@update')->name('wall_update');
Route::post('wall_store', 'WallController@store')->name('wall_store');
Route::delete('wall_delete/{param}', 'WallController@delete')->name('wall_delete');
Route::post('registers', 'RegisterController@create')->name('registers');
Route::get('logins', function(){ return view('login'); })->name('logins');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
